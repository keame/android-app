package com.kashmir.keame.interfaces;

import android.view.View;

/**
 * Created by Lorena Soledad on 16/10/2015.
 */
public interface OnRecyclerItemClickListener {
    void onItemClick(View v, int position);
}
