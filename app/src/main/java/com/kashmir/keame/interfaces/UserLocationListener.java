package com.kashmir.keame.interfaces;

import android.location.Location;

/**
 * Created by Kashmir on 6/11/2015.
 */
public interface UserLocationListener {
    void onLocationReceived(Location userLocation);
}
