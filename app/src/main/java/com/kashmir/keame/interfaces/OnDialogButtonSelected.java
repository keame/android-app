package com.kashmir.keame.interfaces;

/**
 * Created by Lorena Soledad on 16/10/2015.
 */
public interface OnDialogButtonSelected {
    void onPositiveButtonClick(int position);
    void onNegativeButtonClick(int position);
    void onNeutralButtonClick();
}
