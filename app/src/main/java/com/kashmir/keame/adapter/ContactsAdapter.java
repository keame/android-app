package com.kashmir.keame.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Contact;

import java.util.ArrayList;

/**
 * Created by Lorena Soledad on 12/08/2015.
 */
public class ContactsAdapter extends BaseAdapter {

    private View.OnClickListener mListener;
    private ArrayList<Contact> mData;
    private Context mContext;

    public ContactsAdapter(ArrayList<Contact>data, Context context, View.OnClickListener listener){
        this.mData = data;
        this.mContext = context;
        this.mListener = listener;
    }

    static class ViewHolder{
        TextView name;
        ImageView menu;
    }


    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolderItem;

        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.contact_row, parent, false);

            viewHolderItem = new ViewHolder();
            viewHolderItem.name = (TextView) convertView.findViewById(R.id.list_contact_name);
            viewHolderItem.menu = (ImageView) convertView.findViewById(R.id.popup_menu);

            convertView.setTag(viewHolderItem);
        } else{
            viewHolderItem = (ViewHolder) convertView.getTag();
        }

        // object item based on the position
        Contact objectItem = mData.get(position);

        if(objectItem != null) {
            viewHolderItem.name.setText(objectItem.getFullName());
            viewHolderItem.menu.setTag(position);
            viewHolderItem.menu.setOnClickListener(mListener);
        }

        return convertView;

    }


    public void updateList(ArrayList<Contact> newList){
        mData.clear();
        mData.addAll(newList);
        notifyDataSetChanged();
    }
}
