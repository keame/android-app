package com.kashmir.keame.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Route;

import java.util.ArrayList;

/**
 * Created by Lorena Soledad on 20/09/2015.
 */
public class RoutesAdapter extends BaseAdapter {

    private ArrayList<Route> data;
    private Context mContext;

    public RoutesAdapter(ArrayList<Route> data, Context context){
        this.data = data;
        this.mContext = context;
    }

    static class ViewHolder {
        TextView titleName;
        //TextView subtitle;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolderItem;

        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.routes_row, parent, false);

            // well set up the ViewHolder
            viewHolderItem = new ViewHolder();
            viewHolderItem.titleName = (TextView) convertView.findViewById(R.id.route_name);
            //viewHolderItem.subtitle = (TextView) convertView.findViewById(R.id.list_item_subtitle);

            // store the holder with the view.
            convertView.setTag(viewHolderItem);
        } else{
            // we've just avoided calling findViewById() on resource everytime
            // just use the viewHolder
            viewHolderItem = (ViewHolder) convertView.getTag();
        }

        // object item based on the position
        Route objectItem = data.get(position);

        // assign values if the object is not null
        if(objectItem != null) {
            // get the TextView from the ViewHolder and then set the text (item name) and tag (item ID) values
            //viewHolderItem.titleName.setText("Titulo del destino"); //TODO: basado en object item
            viewHolderItem.titleName.setText(objectItem.getName());
        }

        return convertView;
    }


    public void updateList(ArrayList<Route> newData){
        data.clear();
        data.addAll(newData);
        notifyDataSetChanged();
    }
}
