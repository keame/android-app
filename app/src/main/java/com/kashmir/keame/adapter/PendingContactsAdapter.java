package com.kashmir.keame.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.controller.PendingContactsFragment;
import com.kashmir.keame.dialog.AddNewContactDialog;
import com.kashmir.keame.interfaces.OnRecyclerItemClickListener;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Contact;

import java.util.ArrayList;

/**
 * Created by Lorena Soledad on 16/10/2015.
 */
public class PendingContactsAdapter extends
        RecyclerView.Adapter<PendingContactsAdapter.ContactViewHolder> {

    private static final String TAG = PendingContactsAdapter.class.getSimpleName();

    private ArrayList<Contact> mData;
    private PendingContactsFragment mFragment;




    /* CLASE VIEW HOLDER ------------------------------------------------------------------------ */

    public static class ContactViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener{

        public TextView mName;
        private OnRecyclerItemClickListener mListener;

        public ContactViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mName = (TextView) itemView.findViewById(R.id.pending_contact_name);
        }

        public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener listener){
            mListener = listener;
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(v, getAdapterPosition());
        }
    }


    



    public PendingContactsAdapter(ArrayList<Contact> contacts, PendingContactsFragment fragment){
        mData = contacts;
        mFragment = fragment;
    }

    @Override
    public ContactViewHolder
    onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pending_contact_item, parent, false);

        ContactViewHolder holder = new ContactViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(PendingContactsAdapter.ContactViewHolder holder, int position) {

        Contact item = mData.get(position);
        holder.mName.setText(item.getFullName());
        holder.setOnRecyclerItemClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                AddNewContactDialog dialog = AddNewContactDialog.newInstance(position);
                dialog.setTargetFragment(mFragment, Constants.ApiRequestCode.ACCEPT_FRIEND);
                dialog.show(mFragment.getActivity().getSupportFragmentManager(),
                        "DIALOG_ADD_CONTACT");

            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
