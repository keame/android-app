package com.kashmir.keame.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Group;

import java.util.ArrayList;

/**
 * Created by Lorena Soledad on 17/08/2015.
 */
public class GroupsAdapter extends BaseAdapter {

    private static final String TAG = GroupsAdapter.class.getSimpleName();

    private View.OnClickListener mListener;
    private Context mContext;
    private ArrayList<Group> mData;


    public GroupsAdapter(ArrayList<Group> data, Context context, View.OnClickListener listener){
        this.mContext = context;
        this.mData = data;
        this.mListener = listener;
    }


    static class ViewHolder {
        TextView name;
        ImageView menu;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolderItem;

        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.groups_row, parent, false);

            viewHolderItem = new ViewHolder();
            viewHolderItem.name = (TextView) convertView.findViewById(R.id.list_group_name);
            viewHolderItem.menu = (ImageView) convertView.findViewById(R.id.popup_menu);

            convertView.setTag(viewHolderItem);
        } else{
            viewHolderItem = (ViewHolder) convertView.getTag();
        }

        // object item based on the position
        Group objectItem = mData.get(position);

        if(objectItem != null) {
            viewHolderItem.name.setText(objectItem.getName());
            viewHolderItem.menu.setTag(position);
            viewHolderItem.menu.setOnClickListener(mListener);
            // Retrieve the popup button from the inflated view
            // Set the item as the button's tag so it can be retrieved later
            //popupButton.setTag(getItem(position));
            // Set the fragment instance as the OnClickListener
            //popupButton.setOnClickListener(mListener);

        }

        return convertView;
    }

    public void updateList(ArrayList<Group> newList){
        mData.clear();
        mData.addAll(newList);
        notifyDataSetChanged();
    }

}
