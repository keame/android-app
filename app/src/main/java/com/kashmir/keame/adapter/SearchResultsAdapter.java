package com.kashmir.keame.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Contact;

import java.util.ArrayList;

/**
 * Created by Lorena Soledad on 08/10/2015.
 */
public class SearchResultsAdapter extends
        RecyclerView.Adapter<SearchResultsAdapter.ViewHolder> {

    private static final String TAG = SearchResultsAdapter.class.getSimpleName();

    private ArrayList<Contact> mData;
    private OnItemListClickListener mListItemListener;

    public interface OnClickListener {
        void onItemClick(View view, int position, boolean isLongClick);
    }

    public interface OnItemListClickListener {
        void onListItemClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener, View.OnLongClickListener {

        public TextView mName;
        public TextView mSomeData;
        private OnClickListener mOnClickListener;


        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            mName = (TextView) v.findViewById(R.id.textview_name);
            mSomeData = (TextView) v.findViewById(R.id.textview_some_data);
        }

        public void setClickListener(OnClickListener onClickListener) {
            mOnClickListener = onClickListener;
        }

        @Override
        public void onClick(View v) {
            mOnClickListener.onItemClick(v, getAdapterPosition(), true);
        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    public SearchResultsAdapter(ArrayList<Contact> data, OnItemListClickListener listener){
        mData = data;
        mListItemListener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_result_item, parent, false);

        ViewHolder holder = new ViewHolder(v);

        return holder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mName.setText(mData.get(position).getFullName());
        holder.setClickListener(new OnClickListener() {
            @Override
            public void onItemClick(View view, int position, boolean isLongClick) {
                mListItemListener.onListItemClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

}