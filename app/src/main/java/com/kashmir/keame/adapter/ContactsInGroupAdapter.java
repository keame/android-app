package com.kashmir.keame.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Contact;

import java.util.ArrayList;


/**
 * Created by Kashmir on 17/10/2015.
 */
public class ContactsInGroupAdapter extends
        RecyclerView.Adapter <ContactsInGroupAdapter.ViewHolder> {
    
    private ArrayList<Contact> mContacts;


    /* CLASE VIEW HOLDER ------------------------------------------------------------------------ */

    public static class ViewHolder extends RecyclerView.ViewHolder  {

        public TextView mContactName;
        public CheckBox mIsInGroup;

        public ViewHolder(View itemView) {
            super(itemView);
            mContactName = (TextView) itemView.findViewById(R.id.contact_name);
            mIsInGroup = (CheckBox) itemView.findViewById(R.id.contact_checkbox);
        }
    }





    public ContactsInGroupAdapter(ArrayList<Contact> contacts){
        mContacts = contacts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.groups_member_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mContactName.setText(mContacts.get(position).getFullName());
        holder.mIsInGroup.setChecked(mContacts.get(position).getStatusInGroup());
        holder.mIsInGroup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mContacts.get(position).setStatusInGroup(isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }


}
