package com.kashmir.keame.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Contact;
import com.kashmir.keame.model.Destination;

import java.util.ArrayList;


/**
 * Created by Kashmir on 17/10/2015.
 */
public class DestinationsInGroupAdapter extends
        RecyclerView.Adapter <DestinationsInGroupAdapter.ViewHolder> {

    private ArrayList<Destination> mDestinations;


    /* CLASE VIEW HOLDER ------------------------------------------------------------------------ */

    public static class ViewHolder extends RecyclerView.ViewHolder  {

        public TextView mDestinationName;
        public CheckBox mIsInGroup;

        public ViewHolder(View itemView) {
            super(itemView);
            mDestinationName = (TextView) itemView.findViewById(R.id.destination_name);
            mIsInGroup = (CheckBox) itemView.findViewById(R.id.destination_checkbox);
        }

    }





    public DestinationsInGroupAdapter(ArrayList<Destination> destinations){
        mDestinations = destinations;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.groups_destination_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mDestinationName.setText(mDestinations.get(position).getName());
        holder.mIsInGroup.setChecked(mDestinations.get(position).getStatusInGroup());
        holder.mIsInGroup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mDestinations.get(position).setStatusInGroup(isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDestinations.size();
    }

}
