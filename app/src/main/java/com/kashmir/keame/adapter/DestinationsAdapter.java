package com.kashmir.keame.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Destination;

import java.util.ArrayList;

public class DestinationsAdapter extends BaseAdapter {

    private ArrayList<Destination> data;
    private Context mContext;

    public DestinationsAdapter(ArrayList<Destination> data, Context context){
        this.data = data;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolderItem;

        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.destination_row, parent, false);

            // well set up the ViewHolder
            viewHolderItem = new ViewHolder();
            viewHolderItem.titleName = (TextView) convertView.findViewById(R.id.list_item_title);
            viewHolderItem.subtitle = (TextView) convertView.findViewById(R.id.list_item_subtitle);

            // store the holder with the view.
            convertView.setTag(viewHolderItem);
        } else{
            // we've just avoided calling findViewById() on resource everytime
            // just use the viewHolder
            viewHolderItem = (ViewHolder) convertView.getTag();
        }

        // object item based on the position
        Destination objectItem = data.get(position);

        // assign values if the object is not null
        if(objectItem != null) {
            // get the TextView from the ViewHolder and then set the text (item name) and tag (item ID) values
            //viewHolderItem.titleName.setText("Titulo del destino"); //TODO: basado en object item
            viewHolderItem.titleName.setText(objectItem.getName());
        }

        return convertView;
    }

    static class ViewHolder {
        TextView titleName;
        TextView subtitle;
    }


    public void updateList(ArrayList<Destination> newData){
        data.clear();
        data.addAll(newData);
        notifyDataSetChanged();
    }
}
