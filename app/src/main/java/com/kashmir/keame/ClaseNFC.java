package com.kashmir.keame;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.widget.Toast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static android.nfc.NdefRecord.createMime;

/**
 * Created by Jonatan on 26/11/15.
 */
public class ClaseNFC {
    private NfcAdapter adapter;
    private Context context;
    private Tag tag;

    /** Contructor por parametros */
    public ClaseNFC (NfcAdapter n, Context c, Tag t){
        adapter = n;
        context = c;
        tag = t;
    }

    public String handleIntent(Intent intent) {
        String action = intent.getAction();
        String error = "0";
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {
            tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            //Toast.makeText(context, "UID Tag NFC", Toast.LENGTH_LONG).show();
            return (bytesToHexString(tag.getId()));
        }
        return error;
    }

    //Pasamos los bytes a String

    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }
        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }
        return stringBuilder.toString();
    }

    //Ligamos el Tag NFC a la aplicacion de KeaME

    public void ligarTag (){
        try {
            if (tag == null) {
                Toast.makeText(context, "Debe acercar el Tag NFC", Toast.LENGTH_SHORT).show();
            } else {
                escribir_en_tag();
                Toast.makeText(context, "Su tag NFC se a ligado a KeaMe", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException | FormatException e) {
            Toast.makeText(context, "Intentelo nuevamente", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void escribir_en_tag() throws IOException, FormatException{
        NdefMessage message = new NdefMessage(
                new NdefRecord[] { createMime(
                        "application/vnd.com.kashmir.keame", null)
                        ,NdefRecord.createApplicationRecord("com.kashmir.keame")
                        //"application/vnd.com.app.jonatanggarcia.nfcfinalclass", null)
                        //,NdefRecord.createApplicationRecord("com.app.jonatanggarcia.nfcfinalclass")
                });
        Ndef ndef = Ndef.get(tag); //Obtenemos una instancia de Ndef del Tag
        ndef.connect();
        ndef.writeNdefMessage(message);
        ndef.close();
    }

    public void desligarTag (){
        try {
            if (tag == null) {
                Toast.makeText(context, "Debe acercar el Tag NFC", Toast.LENGTH_SHORT).show();
            } else {
                vaciar_tag();
                Toast.makeText(context, "Su tag NFC se a desligado de KeaMe", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException | FormatException e) {
            Toast.makeText(context, "Intentelo nuevamente", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void vaciar_tag() throws IOException, FormatException{
        NdefRecord[] records = {createRecord(" ")};
        NdefMessage message = new NdefMessage(records);
        Ndef ndef = Ndef.get(tag);
        ndef.connect();
        ndef.writeNdefMessage(message);
        ndef.close();
    }

    private NdefRecord createRecord(String text) throws UnsupportedEncodingException {
        String lang = "us";
        byte[] textBytes = text.getBytes();
        byte[] langBytes = lang.getBytes("US-ASCII");
        int langLength = langBytes.length;
        int textLength = textBytes.length;
        byte[] payLoad = new byte[1 + langLength + textLength];
        payLoad[0] = (byte) langLength;

        System.arraycopy(langBytes, 0, payLoad, 1, langLength);
        System.arraycopy(textBytes, 0, payLoad, 1 + langLength, textLength);

        NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payLoad);
        return recordNFC;
    }

    //Foreground Dispatch

    public void setupForegroundDispatch(final Activity activity) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); //Reutiliza la activity si ya esta en la pila.

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter writeTagFilters[];
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[]{tagDetected};
        if(adapter != null && adapter.isEnabled()){
            adapter.enableForegroundDispatch(activity, pendingIntent, writeTagFilters, null);
        }
    }

    public void stopForegroundDispatch(final Activity activity) {
        if(adapter != null && adapter.isEnabled()){
            adapter.disableForegroundDispatch(activity);
        }
    }
}
