package com.kashmir.keame.activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kashmir.keame.R;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.dialog.AddRouteDialog;
import com.kashmir.keame.model.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DisplayRouteActivity extends AppCompatActivity implements OnMapReadyCallback,
        Data.OnQueryListener, AddRouteDialog.OnDialogButtonSelected {

    private static final String TAG = DisplayRouteActivity.class.getSimpleName();

    private static final int LINE_WIDTH  = 2;
    private static final int ZOOM_LEVEL = 15;

    private int mCurrentUserId;

    private String mNewRouteName;
    private ArrayList<LatLng> mPoints;
    private ArrayList<Polyline> mPolylines;
    private ArrayList<Marker> mMarkers;

    SupportMapFragment mMapFragment;
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_route);

        mPoints = getIntent().getParcelableArrayListExtra(Constants.Routes.EXTRA_ROUTE);

        if(mPoints != null){
            Log.d(TAG, "No soy nulo");
        }

        mCurrentUserId = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        setUpToolbar();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "Mapa listo.");

        displayRouteOnMap();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_route, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_save:
                AddRouteDialog dialog = AddRouteDialog.newInstance();
                dialog.show(getSupportFragmentManager(), Constants.Routes.DIALOG_NEW_ROUTE);
                return true;
            case R.id.action_cancel:
                cancelRoute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpToolbar(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.activity_display_route);
    }

    public void displayRouteOnMap(){

        //Muevo la camara para centrarme en eldestino
        mMapFragment.getMap().moveCamera(CameraUpdateFactory
                .newLatLngZoom(mPoints.get(0), ZOOM_LEVEL));

        mMarkers = new ArrayList<>();
        mPolylines = new ArrayList<>();

        LatLng lastPoint = null;
        for(LatLng point: mPoints){

            //Si es la primera vez, no puedo dibujar ninguna linea
            if(lastPoint != null){
                mPolylines.add(addPolyline(lastPoint,point));
            }

            //Dibujar el marker
            lastPoint = point;
            mMarkers.add(addMarker(point));
        }
    }

    private Polyline addPolyline(LatLng a, LatLng b){

        Polyline line = mMapFragment.getMap().addPolyline(new PolylineOptions()
                .add(a, b)
                .width(LINE_WIDTH)
                .color(Color.DKGRAY));

        return line;

    }

    private Marker addMarker(LatLng point){

        MarkerOptions options = new MarkerOptions()
                .position(point)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

        return mMapFragment.getMap().addMarker(options);
    }



    /* ACCIONES DEL MENÚ ------------------------------------------------------------------------ */

    private void saveRoute(){

        String uri = String.format(Constants.Uri.ADD_NEW_ROUTE, mCurrentUserId);

        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.ADD_NEW_ROUTE,
                getJSONString()
        );
    }

    private void cancelRoute(){
        finish();
    }

    private String getJSONString(){
        JSONObject point;
        JSONObject newRoute = new JSONObject();
        JSONArray coordinates = new JSONArray();

        try {

            for(int i = 0; i < mPoints.size(); i++ ){
                point = new JSONObject();

                point.put(Constants.Routes.JSON_TAG_ROUTE_LATITUDE,
                        mPoints.get(i).latitude);
                point.put(Constants.Routes.JSON_TAG_ROUTE_LONGITUDE,
                        mPoints.get(i).longitude);

                coordinates.put(point);
            }

            newRoute.put(Constants.Routes.JSON_TAG_ROUTE_COORDINATES, coordinates);
            newRoute.put(Constants.Routes.JSON_TAG_ROUTE_NAME, mNewRouteName);

            return newRoute.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }



    /* RESPUESTAS DEL SERVER -------------------------------------------------------------------- */
    @Override
    public void onResponse(int requestCode, String result) {
        Log.d(TAG, result);

        try {
            JSONObject serverResponse = new JSONObject(result);
            int status = serverResponse.getInt("status");

            if(status == 200){
                Toast.makeText(this, R.string.snackbar_route_added, Toast.LENGTH_SHORT)
                    .show();
                finish();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Log.d(TAG, "Status Code: " + statusCode);
        Toast.makeText(this, R.string.server_error, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onPositiveButtonSelected(String routeName) {
        mNewRouteName = routeName;
        saveRoute();
    }
}
