package com.kashmir.keame.activity;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Constants;

import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HelpActivity extends AppCompatActivity {

    private static final String TAG = HelpActivity.class.getSimpleName();

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private CircleIndicator mIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        Log.d(TAG, "onCreate");

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int selectedIndex;
            private boolean mPageEnd = false;
            private boolean callHappened;
            private static final float thresholdOffset = 0.5f;
            private boolean scrollStarted, checkDirection;

            @Override
            public void onPageSelected(int arg0) {
                Log.d(TAG, "onPageSelected");
                selectedIndex = arg0;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                Log.d(TAG, "onPageScrolled");

                if (checkDirection) {
                    if (thresholdOffset > arg1) {
                        Log.i(TAG, "going left");
                        if(mPageEnd){
                            setHelpSeenOnPreferences();
                            finish();
                        }
                    } else {
                        Log.i(TAG, "going right");
                    }
                    checkDirection = false;
                }

                if( mPageEnd && arg0 == selectedIndex && !callHappened)
                {
                    Log.d(getClass().getName(), "Okay");
                    mPageEnd = false;//To avoid multiple calls.
                    callHappened = true;
                }else
                {
                    mPageEnd = false;
                }
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                Log.d(TAG, "onPageScrollStateChanged");

                if (!scrollStarted && arg0 == ViewPager.SCROLL_STATE_DRAGGING) {
                    scrollStarted = true;
                    checkDirection = true;
                } else {
                    scrollStarted = false;
                }

                if(selectedIndex == mSectionsPagerAdapter.getCount() - 1)
                {
                    mPageEnd = true;
                }
            }
        });

        mIndicator = (CircleIndicator) findViewById(R.id.circle_indicator);
        mIndicator.setViewPager(mViewPager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Destruyendo la ayuda.");
    }

    private void setHelpSeenOnPreferences() {
        getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE)
                .edit()
                .putBoolean(Constants.Preferences.IS_USER_FIRST_TIME, false)
                .commit();
    }

    /* PARA CUSTOMIZAR LA FUENTE ---------------------------------------------------------------- */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            String title = "";
            String content = "";

            switch (position){
                case 0:
                    title = getString(R.string.page_1_title);
                    content = getString(R.string.page_1_content);
                    break;
                case 1:
                    title = getString(R.string.page_2_title);
                    content = getString(R.string.page_2_content);
                    break;
                case 2:
                    title = getString(R.string.page_3_title);
                    content = getString(R.string.page_3_content);
                    break;
                case 3:
                    title = getString(R.string.page_4_title);
                    content = getString(R.string.page_4_content);
                    break;
                case 4:
                    title = getString(R.string.page_5_title);
                    content = getString(R.string.page_5_content);
                    break;
                case 5:
                    title = getString(R.string.page_6_title);
                    content = getString(R.string.page_6_content);
                    break;
                case 6:
                    title = getString(R.string.page_7_title);
                    content = getString(R.string.page_7_content);
                    break;
                case 7:
                    title = getString(R.string.page_8_title);
                    content = getString(R.string.page_8_content);
                    break;
                case 8:
                    title = getString(R.string.page_9_title);
                    content = getString(R.string.page_9_content);
                    break;

            }

            return PlaceholderFragment.newInstance(title, content);
        }

        @Override
        public int getCount() {
            // Show 9 total pages.
            return 9;
        }

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_PAGE_TITLE = "page_title";
        private static final String ARG_PAGE_CONTENT = "page_content";

        private static TextView mPageTitle;
        private static TextView mPageContent;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(String pageTitle, String pageContent) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PAGE_TITLE, pageTitle);
            args.putString(ARG_PAGE_CONTENT, pageContent);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_help, container, false);
            mPageTitle = (TextView) rootView.findViewById(R.id.page_title);
            mPageContent = (TextView) rootView.findViewById(R.id.page_content);

            mPageTitle.setText(getArguments().getString(ARG_PAGE_TITLE));
            mPageContent.setText(getArguments().getString(ARG_PAGE_CONTENT));

            return rootView;
        }
    }
}
