package com.kashmir.keame.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kashmir.keame.ClaseNFC;
import com.kashmir.keame.R;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.controller.GoogleMapsApiConnection;
import com.kashmir.keame.controller.TabContactsFragment;
import com.kashmir.keame.dialog.GetDirectionsDialog;
import com.kashmir.keame.interfaces.UserLocationListener;
import com.kashmir.keame.service.RouteTrackerService;
import com.kashmir.keame.model.Contact;

import com.github.alexkolpa.fabtoolbar.FabToolbar;
import com.kashmir.keame.model.Destination;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Route;
import com.kashmir.keame.controller.DestinationsFragment;
import com.kashmir.keame.controller.GroupsFragment;
import com.kashmir.keame.dialog.RouteTrackerDialog;
import com.kashmir.keame.view.RoutesFragment;
import com.kashmir.keame.view.SettingsFragment;
import com.melnykov.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.drakeet.materialdialog.*;

import java.util.ArrayList;
import java.util.Iterator;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainMapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMapLongClickListener, GoogleMap.OnMapClickListener,
        DestinationsFragment.OnDestinationSelected, RoutesFragment.OnRouteSelected,
        Data.OnQueryListener, UserLocationListener, GetDirectionsDialog.OnDialogButtonSelected {

    //Constantes de uso general
    private static final String TAG = MainMapActivity.class.getSimpleName();
    private static final int FRAGMENT_CONTACTS = R.id.drawer_menu_contacts;
    private static final int FRAGMENT_GROUPS = R.id.drawer_menu_groups;
    private static final int FRAGMENT_DESTINATIONS = R.id.drawer_menu_destinations;
    private static final int FRAGMENT_ROUTES = R.id.drawer_menu_routes;
    private static final int FRAGMENT_ABOUT_US = R.id.drawer_menu_about_us;
    private static final int ZOOM_LEVEL = 10;
    private static final int TILT_LEVEL = 30;
    private static final int LINE_WIDTH  = 2;

    //Componentes visuales
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout; //Side menu
    private NavigationView mNavigationView;
    private TabLayout mTabLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private CoordinatorLayout mSnackbar;
    private Fragment mFragment;
    private FabToolbar mFabToolbar;
    private FloatingActionButton mFabButton;
    private ActionMode mActionMode;

    //Posición de los contactos
    ArrayList<Contact> mContactPosition;


    //Para manejar el shaking
    //private SensorManager mSensorManager;
    //private ShakingSensor mShakingSensor;

    //Conexion con la API
    private GoogleMapsApiConnection mGoogleMapsApi;
    //private Location mLastKnownLocation;

    //Para a�adir rutas y destinos
    private ArrayList<LatLng> geopoints;
    private ArrayList<Marker> tempMarkers;
    private ArrayList<Polyline> tempPolylines;
    private LatLng mTempDestination;
    private Circle mTempDestinationRange;
    private Marker mTempDestinationMarker;
    private boolean isAddDestionationEnabled;
    private boolean isAddRouteEnabled;

    //Para mostrar un destino
    private boolean isDisplayDestinationEnabled = false;
    private boolean isDisplayRouteEnabled = false;

    //Animations
    private Animation fadeOut;
    private Animation fadeIn;

    //Para el FAB button
    private boolean isOnAboutScreen = false;

    //Para el menu
    private MenuItem mCurrentMenuItem;
    private MenuItem mPreviousMenuItem = null;

    //Para mostrar rutas y destinos seleccionados
    private Destination mDestinationItemSelected;
    private Route mRouteItemSelected;
    private int mDestinationItemIndex;
    private int mRouteItemIndex;

    //Para crear una ruta a partir de las direcciones de Google Maps
    private ArrayList<LatLng> mGoogleRoute;


    //Para inicializar la preferencias
    private SharedPreferences settings = null;

    //-NFC
    private ClaseNFC nfc;
    private String nfcuid;
    private boolean flagañadirtag;
    private boolean flagborrartag;
    private EditText textopin;
    private boolean flagautenticado; //Para el toast de intentelo de nuevo.
    private boolean flagdialog; //Para que no cree otros dialog arriba, si ya hay uno.

    /* CALLBACKS PARA LOS ACTION MODE ----------------------------------------------------------- */
    /* Se usa cuando un usuario agrega una ruta o destino --------------------------------------- */

    private ActionMode.Callback mDestinationActionMode = new ActionMode.Callback() {

        private int opt;

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_add_route, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            opt = item.getItemId();

            switch (item.getItemId()){
                case R.id.action_cancel:
                    clearTempDataOnMap();
                    cancelDestination();
                    mode.finish();
                    return true;
                case R.id.action_save:
                    if(mTempDestination != null){
                        showConfirmationDialogForDestination();
                        mode.finish();
                    } else
                        Snackbar.make(mSnackbar, R.string.snackbar_invalid_destination,
                                Snackbar.LENGTH_LONG).show();

                    return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            if(opt != R.id.action_save){
                clearTempDataOnMap();
                cancelDestination();
            }

            mActionMode = null;
        }
    };

    private ActionMode.Callback mRouteActionMode = new ActionMode.Callback() {

        private int opt;

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_add_route, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            //Nothing to do here :)
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            opt = item.getItemId();

            switch (item.getItemId()){
                case R.id.action_cancel:
                    clearTempDataOnMap();
                    cancelRoute();
                    mode.finish();
                    return true;
                case R.id.action_save:
                    if(geopoints.size() >= 2){
                        showConfirmationDialogForRoute();
                        mode.finish();
                    } else
                        Snackbar.make(mSnackbar, R.string.snackbar_invalid_route,
                                Snackbar.LENGTH_LONG).show();
                    return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            if(opt != R.id.action_save){
                clearTempDataOnMap();
                cancelRoute();
            }

            mActionMode = null;
        }
    };




    /* CICLO DE VIDA DE LA ACTVITY -------------------------------------------------------------- */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);

        //Preparo el fragment
        setUpMapFragment();

        //Obtengo referencia al archivo de configuraciones
        settings = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE);

        //Si es la primera vez que ingresa el usuario, se muestra la ayuda
        if(settings.getBoolean(Constants.Preferences.IS_USER_FIRST_TIME, false)){
            showHelp();
        }

        //Set up GUI
        mGoogleMapsApi = new GoogleMapsApiConnection(MainMapActivity.this);
        mGoogleMapsApi.buildGoogleApiClient();
        instanciateItems();
        setUpToolbar();
        setUpFabToolbar();
        setUpNavigationDrawer();

        //Animations
        fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_fab_out);
        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_fab_in);

        //Initializing variables
        isAddDestionationEnabled = false;
        isAddRouteEnabled = false;
        geopoints = new ArrayList<LatLng>();
        tempMarkers = new ArrayList<Marker>();
        tempPolylines = new ArrayList<Polyline>();

        //-NFC
        nfc = new ClaseNFC(NfcAdapter.getDefaultAdapter(this),this,null);
        nfcuid = nfc.handleIntent(getIntent());//nfcuid = "0";
        Log.d(TAG, "nfcuid 1 :"+nfcuid);
        flagañadirtag=false;
        flagborrartag=false;
        flagautenticado=false;
        flagdialog=false;
    }

    //-NFC
    @Override
    protected void onNewIntent(Intent intent){
        nfcuid = nfc.handleIntent(intent);
        Log.d(TAG, "nfcuid 3 :" + nfcuid);
        if(!flagautenticado)
            authenticateWithTag(nfcuid);
        if(flagañadirtag){
            nfc.ligarTag();
            flagañadirtag=false;
            //TODO-Lore: Aca se debe mandar el nfcuid a la BD
            updateTag(nfcuid);
        }
        if(flagborrartag){
            nfc.desligarTag();
            flagborrartag=false;
            updateTag("");
            //TODO-Lore: Aca se debe borrar el nfcuid de la bd
        }
    }

    //-NFC
    public void setFlagañadirtag(boolean b){
        flagañadirtag=b;
        Toast.makeText(this, "Debe acercar su Tag NFC", Toast.LENGTH_SHORT).show();
    }

    //-NFC
    public void setFlagborrartag(boolean b){
        flagborrartag=b;
        Toast.makeText(this, "Debe acercar su Tag NFC", Toast.LENGTH_SHORT).show();
    }

    private void showHelp() {
        Intent helpIntent = new Intent(this, HelpActivity.class);
        startActivity(helpIntent);
    }


    @Override
    protected void onResume() {
        super.onResume();

        //-NFC -Autenticacion
        if(!flagdialog)
            authenticateWithTag(nfcuid);
        nfc.setupForegroundDispatch(this);
    }


    @Override
    protected void onPause() {
        //mSensorManager.unregisterListener(mShakingSensor);
        //-NFC
        flagautenticado=false;
        nfcuid="0"; //Para volver a abrir un dialog, si la app se abre manualmente
        nfc.stopForegroundDispatch(this);
        super.onPause();
    }


    public Toolbar getmToolbar() {
        return mToolbar;
    }

    public NavigationView getNavigationView(){
        return  mNavigationView;
    }

    public TabLayout getTabLayout(){
        return mTabLayout;
    }




    /* PARA CUSTOMIZAR LA FUENTE ---------------------------------------------------------------- */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }




    @Override
    public void onBackPressed() {

        Log.d(TAG, "onBackPressed()");
        super.onBackPressed();

        Fragment currentFragment = getSupportFragmentManager()
                .findFragmentById(R.id.container);

        if(!(currentFragment instanceof TabContactsFragment)){
            mTabLayout.setVisibility(View.GONE);
        }

        if(isOnAboutScreen){
            mFabButton.show();
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            getSupportFragmentManager().popBackStack();
            if(mPreviousMenuItem != null){
                mPreviousMenuItem.setChecked(true);
                mCurrentMenuItem.setChecked(false);
                mCurrentMenuItem = mPreviousMenuItem;
            }
            isOnAboutScreen = false;
        } else if(isDisplayDestinationEnabled){

            //Muestro el FabButton y habilito el Drawer
            //mFabButton.show();
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            //Cambio el boton de la Toolbar
            mDrawerToggle.setDrawerIndicatorEnabled(true);

            //Elimino los datos temporales del mapa
            mTempDestinationRange.remove();
            mTempDestinationRange = null;
            mTempDestinationMarker.remove();
            mTempDestinationMarker = null;

            isDisplayDestinationEnabled = false;
        } else if(isDisplayRouteEnabled){

            //Muestro el FabButton y habilito el Drawer
            //mFabButton.show();
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            //Cambio el bot�n de la Toolbar
            mDrawerToggle.setDrawerIndicatorEnabled(true);

            //Elimino los datos temporales del mapa
            for(Marker m: tempMarkers)
                m.remove();
            for(Polyline p: tempPolylines)
                p.remove();

            //Limpio los array
            tempMarkers.clear();
            tempPolylines.clear();

            isDisplayRouteEnabled = false;
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        Log.d(TAG, "dispatchKeyevent = is " + isAddDestionationEnabled);
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                if(isAddDestionationEnabled){
                    clearTempDataOnMap();
                    cancelDestination();
                    return true; // consumes the back key event - ActionMode is not finished
                } else if (isAddRouteEnabled){
                    clearTempDataOnMap();
                    cancelRoute();
                    return true;
                }
            }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.d(TAG, "onOptionsItemSelected");

        if(item.getItemId() == android.R.id.home &&
                (isDisplayDestinationEnabled || isDisplayRouteEnabled || isOnAboutScreen)){
            this.onBackPressed();
            return true;
        } else if(isDisplayRouteEnabled){
            switch (item.getItemId()){
                case R.id.action_activate:
                    //startRouteTrakingService();
                    checkLocationSettings();
                    break;
                case R.id.action_delete:
                    requestDeleteRoute();
                    break;
                case R.id.action_modify:
                    //modifyRoute();
                    break;
            }
            return true;
        } else if(isDisplayDestinationEnabled){
            switch (item.getItemId()){
                case R.id.action_delete:
                    requestDeleteDestination();
                    break;
                case R.id.action_modify:
                    //modifyDestination();
                    break;
            }
            return true;
        } //else if(mFragment instanceof GroupsFragment || mFragment instanceof RoutesFragment){
            //return false;
        //} //else {
            //mDrawerLayout.openDrawer(GravityCompat.START);
            //return true;
        //}

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Log.d(TAG, "onCreateOptionsMenu()");
        if(isDisplayDestinationEnabled){
            getMenuInflater().inflate(R.menu.menu_display_destination, menu);
            return true;
        } else if(isDisplayRouteEnabled){
            getMenuInflater().inflate(R.menu.menu_display_route, menu);
            return true;
        /*} else if(mFragment instanceof ContactsFragment){
            getMenuInflater().inflate(R.menu.menu_contacts, menu);

            SearchManager searchManager =
                    (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView =
                    (SearchView) menu.findItem(R.id.search).getActionView();
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getComponentName()));
            searchView.setQueryHint("Nombre, email, teléfono...");

            return true;*/
        } else {
            return super.onCreateOptionsMenu(menu);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = mNavigationView.getMenu().findItem(R.id.drawer_menu_help);

        if(item.isChecked()){
            item.setChecked(false);
            mNavigationView.getMenu().findItem(R.id.drawer_menu_home).setChecked(true);
        }

        return super.onPrepareOptionsMenu(menu);
    }







    /* UTILITY FUNCTIONS ------------------------------------------------------------------------ */

    private void instanciateItems(){
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.navigationView);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.app_name,
                R.string.app_name);
        mFabToolbar = ((FabToolbar) findViewById(R.id.fab_toolbar));
        mFabButton = (FloatingActionButton) mFabToolbar.getChildAt(0);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mSnackbar = ((CoordinatorLayout) findViewById(R.id.snackbarPosition));
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
    }

    public void setUpMapFragment() {

        //Opciones del mapa
        GoogleMapOptions configuration = new GoogleMapOptions();
        configuration.mapType(GoogleMap.MAP_TYPE_NORMAL)
                .compassEnabled(false)
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(false);

        //Creamos el Fragment con su mapa
        mFragment = SupportMapFragment.newInstance(configuration);
        ((SupportMapFragment) mFragment).getMapAsync(this);
        FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, mFragment);
        fragmentTransaction.commit();

    }


    /**
     * Seteamos las propiedades de la mToolbar
     *
     */
    private void setUpToolbar(){
        setSupportActionBar(mToolbar);
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }


    /**
     * Inicializamos las propiedades del Fab Button, as� como los men�es que se desplegar�n al
     * presionar el �cono
     *
     */
    private void setUpFabToolbar(){
        mFabToolbar.setColor(getResources().getColor(R.color.primary));

        findViewById(R.id.menu_manual_route).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                isAddRouteEnabled = true;
                mToolbar.setTitle(R.string.toolbar_add_route);
                Snackbar.make(mSnackbar, R.string.instructions_routes, Snackbar.LENGTH_INDEFINITE)
                        .show();
                mFabToolbar.hide(); //Escondo el menu

                //Lanzamos el Actionmode para el destino
                if (mActionMode == null) {
                    mActionMode = MainMapActivity.this.startActionMode(mRouteActionMode);
                }

                mMap.clear(); //Limpio lo que haya en el mapa
            }
        });

        findViewById(R.id.menu_destination).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //Activamos el modo agregar destino
                isAddDestionationEnabled = true;
                //Ocultamos el men�
                mFabToolbar.hide();

                //Cambiamos el titulo en la Action Bar
                mToolbar.setTitle(R.string.toolbar_add_destination);

                //Mostramos las instrucciones para agregar un destino al usuario
                Snackbar.make(mSnackbar, R.string.instructions_destinations, Snackbar.LENGTH_INDEFINITE)
                        .show();

                //Lanzamos el Actionmode para el destino
                if (mActionMode == null) {
                    mActionMode = MainMapActivity.this.startActionMode(mDestinationActionMode);
                }

                //Limpio los datos del mapa
                mMap.clear();
            }
        });

        findViewById(R.id.menu_google_route).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDirectionsDialog();
                mFabToolbar.hide();
            }
        });

        mFabButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_fab_in));

    }


    /**
     * Configuraci�n inicial del Navigation Drawer
     */

    private void setUpNavigationDrawer(){
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(

                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem menuItem) {

                            Log.d(TAG, "NavigatioOnOptionsItemSelected, " +
                                    "Selecciondo: " + menuItem.getTitle());

                            switch (menuItem.getItemId()){
                                case R.id.drawer_menu_help:
                                    showHelp();
                                    break;
                                case R.id.drawer_menu_close_session:
                                    closeCurrentSession();
                                    break;
                                default:
                                    menuItem.setChecked(true);
                                    loadFragment(menuItem.getItemId());
                            }

                            //Depende de donde estoy en la pantalla, muestro u oculto el botón
                            if(menuItem.getItemId() == R.id.drawer_menu_home){
                                mFabButton.setVisibility(View.VISIBLE);
                                mFabButton.show();
                            } else {
                                if(mFabToolbar.isShown()){
                                    mFabToolbar.hide();
                                }

                                if(mFabButton.isShown()){
                                    mFabButton.hide();
                                }
                            }

                            //loadFragment(menuItem.getItemId());
                            mDrawerLayout.closeDrawers();
                            return true;
                        }
                    });

            // Set the drawer toggle as the DrawerListener
            mDrawerLayout.setDrawerListener(mDrawerToggle);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);


            //Configuración del header con el nombre de usuario y el email
            String firstName = settings.getString(Constants.Preferences.CURRENT_USER_FIRST_NAME, null);
            String lastName = settings.getString(Constants.Preferences.CURRENT_USER_LAST_NAME, null);
            String userEmail = settings.getString(Constants.Preferences.CURRENT_USER_MAIL, null);

            LinearLayout headerView = (LinearLayout) getLayoutInflater().inflate(R.layout.drawer_header, null);
            ((TextView) headerView.findViewById(R.id.header_username)).setText(firstName + " " + lastName);
            ((TextView) headerView.findViewById(R.id.header_user_email)).setText(userEmail);

            mNavigationView.addHeaderView(headerView);
        }
    }

    /**
     * Carga el fragmento seleccionado por el usuario
     *
     * @param id Del Fragment
     */
    public void loadFragment(int id){
        switch (id) {
            case R.id.drawer_menu_home:
                mFragment = SupportMapFragment.newInstance();
                ((SupportMapFragment) mFragment).getMapAsync(this);
                mToolbar.setTitle("Recientes");
                break;
            case FRAGMENT_CONTACTS:
                mFragment = TabContactsFragment.newInstance();
                mTabLayout.setVisibility(View.VISIBLE);
                break;
            case FRAGMENT_GROUPS:
                mFragment = GroupsFragment.newInstance();
                break;
            case FRAGMENT_DESTINATIONS:
                mFragment = DestinationsFragment.newInstance();
                break;
            case FRAGMENT_ROUTES:
                mFragment = RoutesFragment.newInstance();
                break;
            //BEGIN
            case R.id.drawer_settings:
                mFragment = new SettingsFragment();
                break;
            //END

            /*case FRAGMENT_ABOUT_US:
                    mFragment = AboutUsFragment.newInstance();
                    mToolbar.setTitle(getResources().getString(R.string.drawer_menu_about_us));
                    if(mFabButton.isVisible())
                        mFabButton.hide();
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                    //Cambiar icono en la Toolbar
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setHomeAsUpIndicator(null);
                    isOnAboutScreen = true;
                    break;
            case R.id.drawer_settings:
                mFragment = new SettingsFragment();
                break;*/
            /*case R.id.drawer_menu_close_session:
                closeCurrentSession();*/
        }

        if(!(mFragment instanceof TabContactsFragment)){
            Log.d(TAG, "No es Contactos");
            mTabLayout.setVisibility(View.GONE);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, mFragment).commit();
        //performFabButtonAnimation();



        /*if(id != R.id.drawer_menu_close_session){
            //TODO: Pasar mFragment a cada uno de los case
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if(id == FRAGMENT_ABOUT_US)
                transaction.addToBackStack(null);
            else
                performFabButtonAnimation();
            if( id != R.id.drawer_menu_home )
                transaction.replace(R.id.container, mFragment).commit();
            else
                transaction.replace(R.id.container, mFragment).commit();
        }*/
    }

    public void performFabButtonAnimation(){
        if(mFabButton.isVisible())
            mFabButton.startAnimation(fadeOut);
        else
            mFabButton.show();
    }





    /* FUNCIONES DEL MAPA ----------------------------------------------------------------------- */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(isDisplayDestinationEnabled){
            displayDestinationOnMap();
        } else if(isDisplayRouteEnabled){
            displayRouteOnMap();
        } else {
            //Configuracion del mapa
            mMap.setOnMapLongClickListener(this);
            mMap.setOnMapClickListener(this);
            mMap.setIndoorEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.setMyLocationEnabled(true);

            //Conectamos con la API de Google
            mGoogleMapsApi.connect();

            //Pedimos la posición de mis contactos
            requestContactsPosition();

        }

    }

    @Override
    public void onLocationReceived(Location userLocation) {
        if(userLocation != null){
            LatLng loc = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(loc)      // Sets the center of the map to Mountain View
                    .zoom(ZOOM_LEVEL)                   // Sets the zoom
                    .tilt(TILT_LEVEL)                   // Sets the tilt of the camera to 30 degrees
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            //No tendria que agregar el marker porque ya lo hace el mapa
            /*mMap.addMarker(new MarkerOptions()
                    .position(loc)
                    .title("Yo"));*/
        }
    }

    private void requestContactsPosition(){
        Data.query(
                this,
                this,
                Constants.ApiRequestCode.REQUEST_CONTACTS_POSITION);
    }



    /**
     * Este evento es disparado cuando el usuario mantiene presionado en un lugar del mapa.
     * Se utiliza esta funci�n para agregar un destino y para agregar los puntos de quiebre de las
     * rutas del mapa
     *
     * @param latLng Latitud y Longitud de la posici�n donde se mantuvo presionado el dedo.
     */
    @Override
    public void onMapLongClick(LatLng latLng) {
        if(isAddDestionationEnabled){
            //Si vuelve a presionar, elimino los datos viejos y cargo los nuevos
            if(mTempDestination != null){
                mTempDestinationRange.remove();
                mTempDestinationMarker.remove();
            }

            mTempDestination = latLng;
            addMarker(latLng);
            addTemporalCircle(latLng);
        }
        else if(isAddRouteEnabled) {

            geopoints.add(latLng); //Agrego la longitud a la lista
            tempMarkers.add(addMarker(latLng)); //Agrego el marker individual

            if(geopoints.size() != 1){
                //Tengo que dibujar la linea
                tempPolylines.add(addPolyline(geopoints.get(geopoints.size() - 1), geopoints.get(geopoints.size() - 2)));
            }
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if(mFabToolbar.isShown()){
            mFabToolbar.hide();
        }
    }




    public void clearTempDataOnMap(){
        if(isAddRouteEnabled){
            //Elimino los marcadores temporales del mapa
            for(Marker m: tempMarkers)
                m.remove();

            //Elimino las lineas temporales del mapa
            for(Polyline p: tempPolylines)
                p.remove();

            //Limpio todas las listas
            geopoints.clear();
            tempPolylines.clear();
            tempMarkers.clear();

            //Ya no permito mas que se agregue una ruta
            isAddRouteEnabled = false;
        }

        if (isAddDestionationEnabled){
            mTempDestination = null;
            if(mTempDestinationMarker != null)
                mTempDestinationMarker.remove();
            if(mTempDestinationRange != null)
                mTempDestinationRange.remove();
        }

        //Vuelvo a cargar los datos en la pantalla principal
        loadMarkersOnMap();
    }


    /**
     * A�ade en el mapa la posici�n actual del usuario y de sus contactos. Esto se realiza
     * al inicio de la aplicaci�n, as� como luego de que el usuario a�ade una ruta o un destino.
     */
    public void loadMarkersOnMap(){

        if(mContactPosition == null){
            //Inicializar con los datos del server
        } else {
            //Cargar con los datos del array
        }

        //loadContactsPosition(loadDummyData());
        loadContactsPosition();
        /*mMap.addMarker(new MarkerOptions()
                .position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()))
                .title("Yo"));*/
    }


    /**
     * Muestra un dialogo para confirmar la creacion de un destino. El usuario deber� ingresar un
     * nombre y guardar el destino, o bien descartarlo y no guardar� en la base de datos.
     *
     */
    public void showConfirmationDialogForDestination(){

        final View dialogLayout = getLayoutInflater()
                .inflate(R.layout.dialog_confirm_destination, null, false);

        final MaterialDialog mMaterialDialog = new MaterialDialog(this)
                .setTitle(R.string.dialog_confirm_destination_title)
                .setMessage(R.string.dialog_confirm_destination_message)
                .setCanceledOnTouchOutside(true)
                .setContentView(dialogLayout);

        mMaterialDialog.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Obtengo el nombre ingresado para la ruta
                        //TODO: VALIDAR
                        String destinationName =
                                ((EditText) dialogLayout.findViewById(R.id.destinationNameField))
                                        .getText().toString().trim();

                        String latitude = String.valueOf(mTempDestination.latitude);
                        String longitude = String.valueOf(mTempDestination.longitude);
                        clearTempDataOnMap();

                        addDestination(destinationName,
                                latitude, longitude);

                        mMaterialDialog.dismiss();
                    }
                });

        mMaterialDialog.setNegativeButton(getResources().getString(R.string.dialog_button_cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clearTempDataOnMap();
                        cancelDestination();
                        mMaterialDialog.dismiss();
                    }
                });

        mMaterialDialog.show();
    }

    public void cancelDestination(){
        //Cambiamos el t�tulo de la Toolbar
        mToolbar.setTitle(R.string.toolbar_main);

        //Mostramos el mensaje al usuario
        Snackbar.make(mSnackbar, R.string.snackbar_destination_discarded, Snackbar.LENGTH_LONG).show();

        //Cambiamos el estado de la variable
        isAddDestionationEnabled = false;
    }


    /*public void addDestination(String name){
        //Creo el nuevo destino
        Destination newDestination =
                new Destination(name, mTempDestination);

        //Lo agrego a la lista de destinos
        availableDestinations.add(newDestination);

        //Seteo el t�tulo de la Toolbar
        mToolbar.setTitle(R.string.toolbar_main);

        //Mostramos el mensaje al usuario
        Snackbar.make(mSnackbar, R.string.snackbar_destination_added, Snackbar.LENGTH_LONG).show();

        //Cambiamos el estado de la variable
        isAddDestionationEnabled = false;
    }*/


    public void addDestination(String name, String latitude, String longitude){

        //Envío al servidor
        Data.query(this,
                this,
                Constants.ApiRequestCode.ADD_DESTINATION,
                name,
                latitude,
                longitude);

        //Seteo el titulo de la Toolbar
        mToolbar.setTitle(R.string.toolbar_main);

        //Cambiamos el estado de la variable
        isAddDestionationEnabled = false;
    }

    public void showConfirmationDialogForRoute(){

        final View dialogLayout = getLayoutInflater()
                .inflate(R.layout.dialog_confirm_destination, null, false);

        final MaterialDialog mMaterialDialog = new MaterialDialog(this)
                .setTitle(R.string.dialog_confirm_destination_title)
                .setMessage(R.string.dialog_confirm_destination_message)
                .setCanceledOnTouchOutside(true)
                .setContentView(dialogLayout);

        mMaterialDialog.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //TODO: PASAR A UNA FUNCION Y VALIDAR
                        String routeName = ((EditText) dialogLayout
                                .findViewById(R.id.destinationNameField))
                                .getText()
                                .toString();

                        addRoute(routeName);
                        clearTempDataOnMap();
                        mMaterialDialog.dismiss();
                    }
                });

        mMaterialDialog.setNegativeButton(getResources().getString(R.string.dialog_button_cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clearTempDataOnMap();
                        cancelRoute();
                        mMaterialDialog.dismiss();
                    }
                });

        mMaterialDialog.show();
    }


    public void cancelRoute(){
        //Seteo el titulo de la Toolbar
        mToolbar.setTitle(R.string.toolbar_main);
        //Muestro el mensaje al usuario
        Snackbar.make(mSnackbar, R.string.snackbar_route_canceled, Snackbar.LENGTH_LONG).show();
        //No permito m�s que se agregue una ruta
        isAddRouteEnabled = false;
    }

    /*public void addRoute(String name){
        //Creo una nueva ruta
        Route newRoute = new Route(name, geopoints);
        //La agrego a la lista de rutas disponibles
        availableRoutes.add(newRoute);
        //Seteo el titulo de la Toolbar
        mToolbar.setTitle(R.string.toolbar_main);
        //Muestro el mensaje al usuario
        Snackbar.make(mSnackbar, R.string.snackbar_route_added, Snackbar.LENGTH_LONG)
                .show();
        //Ya no permito mas que se agregue una ruta
        isAddRouteEnabled = false;
    }*/

    public void addRoute(String name){

        JSONObject point;
        JSONObject newRoute = new JSONObject();
        JSONArray coordinates = new JSONArray();

        try {

            for(int i = 0; i < geopoints.size(); i++ ){
                point = new JSONObject();

                    point.put(Constants.Routes.JSON_TAG_ROUTE_LATITUDE,
                            geopoints.get(i).latitude);
                    point.put(Constants.Routes.JSON_TAG_ROUTE_LONGITUDE,
                            geopoints.get(i).longitude);

                    coordinates.put(point);
            }

            newRoute.put(Constants.Routes.JSON_TAG_ROUTE_COORDINATES, coordinates);
            newRoute.put(Constants.Routes.JSON_TAG_ROUTE_NAME, name);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Data.query(this,
                this,
                Constants.ApiRequestCode.ADD_NEW_ROUTE,
                newRoute.toString());

        Log.d(TAG, "JSON a Enviar: " + newRoute.toString());

        //Seteo el titulo de la Toolbar
        mToolbar.setTitle(R.string.toolbar_main);
    }


    private Marker addMarker(LatLng point){

        MarkerOptions options = new MarkerOptions()
                .position(point)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                //.draggable(true);

        mTempDestinationMarker = mMap.addMarker(options);

        return mTempDestinationMarker;
    }

    private Polyline addPolyline(LatLng a, LatLng b){

        Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(a, b)
                .width(LINE_WIDTH)
                .color(Color.DKGRAY));

        return line;

    }

    private void addTemporalCircle(LatLng point){

        CircleOptions options = new CircleOptions()
                .center(point)
                .radius(Constants.Geofence.RADIUS)
                .fillColor(0x40ff0000)
                .strokeColor(Color.GREEN)
                .strokeWidth(2);

        mTempDestinationRange = mMap.addCircle(options);
    }




    /* SERVICIOS DE LOCALIZACION ---------------------------------------------------------------- */

    /*private void loadContactsPosition(ArrayList<Contact> list){
        Iterator<Contact> iterator = list.iterator();
        Contact currentContact;
        while(iterator.hasNext()){
            currentContact = iterator.next();
            mMap.addMarker(new MarkerOptions()
                    .position(currentContact.getPosition())
                    .title(currentContact.getFullName()));
        }
    }*/

    private void loadContactsPosition(){

        if(mContactPosition == null){
            requestContactsPosition();
            return;
        }

        Iterator<Contact> iterator = mContactPosition.iterator();
        Contact currentContact;

        while(iterator.hasNext()){
            currentContact = iterator.next();
            mMap.addMarker(new MarkerOptions()
                    .position(currentContact.getPosition())
                    .title(currentContact.getFullName())
                    .icon(BitmapDescriptorFactory
                            .defaultMarker(currentContact.hasAnEmergency() ?
                                    BitmapDescriptorFactory.HUE_ORANGE :
                                    BitmapDescriptorFactory.HUE_GREEN
                            )));
        }
    }

    private void parseContactPosition( String jsonString ){

        try {
            JSONObject response = new JSONObject(jsonString);
            JSONArray contactsPosition = response.getJSONArray("contacts_position");

            mContactPosition = new ArrayList<>();

            for(int i = 0; i < contactsPosition.length(); i++){
                mContactPosition.add(new Contact(
                        contactsPosition.getJSONObject(i),
                        Constants.ApiRequestCode.REQUEST_CONTACTS_POSITION
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    /* SELECCION DE UN DESTINO ------------------------------------------------------------------ */

    @Override
    public void onDestinationSelected(int position, Destination destination) {

        isDisplayDestinationEnabled = true;
        mFragment = SupportMapFragment.newInstance();

        mDestinationItemSelected = destination;
        mDestinationItemIndex = position;

        //Reemplazar el fragment actual por el mapa
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, mFragment);
        transaction.addToBackStack(null);
        transaction.commit();

        //Cuando el mapa este listo, se actualizan los datos
        ((SupportMapFragment) mFragment).getMapAsync(this);
        mFragment.setHasOptionsMenu(true);

        //Oculto el FabButton e inhabilito el Drawer
        mFabButton.hide();
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        //Cambio el icono de la Toolbar
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(null);

    }

    public void displayDestinationOnMap(){
        //Muevo la camara para centrarme en eldestino
        mMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(mDestinationItemSelected.getPosition(), ZOOM_LEVEL));

        //Agregar marker
        addMarker(mDestinationItemSelected.getPosition());

        //Agregar circulo del rango
        addTemporalCircle(mDestinationItemSelected.getPosition());
    }


    private void requestDeleteDestination(){
        Data.query(
                this,
                this,
                Constants.ApiRequestCode.DELETE_DESTINATION,
                String.valueOf(mDestinationItemSelected.getId())
        );
    }





    /* SELECCION DE UNA RUTA -------------------------------------------------------------------- */
    @Override
    public void onRouteSelected(int position, Route route) {

        isDisplayRouteEnabled = true;
        mFragment = SupportMapFragment.newInstance();

        mRouteItemSelected = route;
        mRouteItemIndex = position;

        //Reemplazar el fragment actual por el mapa
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, mFragment);
        transaction.addToBackStack(null);
        transaction.commit();

        //Cuando el mapa este listo, se actualizan los datos
        ((SupportMapFragment) mFragment).getMapAsync(this);
        mFragment.setHasOptionsMenu(true);

        //Oculto el FabButton e inhabilito el Drawer
        mFabButton.hide();
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        //Cambio el icono de la Toolbar
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(null);

    }

    public void displayRouteOnMap(){

        //Muevo la camara para centrarme en eldestino
        mMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(mRouteItemSelected.getGeopoints().get(0), ZOOM_LEVEL));

        LatLng lastPoint = null;

        for(LatLng point: mRouteItemSelected.getGeopoints()){

            //Si es la primera vez, no puedo dibujar ninguna linea
            if(lastPoint != null){
                tempPolylines.add(addPolyline(lastPoint,point));
            }

            //Dibujar el marker
            lastPoint = point;
            tempMarkers.add(addMarker(point));
        }
    }

    private void requestDeleteRoute(){
        Data.query(
                this,
                this,
                Constants.ApiRequestCode.DELETE_ROUTE,
                String.valueOf(mRouteItemSelected.getId())
        );
    }



    /**
     * Closes current user's session, so the next time he enters the app, it will show
     * login screen again.
     */
    private void closeCurrentSession(){
        settings.edit()
                .putBoolean(Constants.Preferences.IS_USER_LOGGED_IN, false)
                .commit();

        //Finaliza la activity
        finish();
    }

    private void startRouteTrakingService(){
        if(isRouteTrakingServiceRunning()){
            //Mostrar un diálogo al usuario
            Log.d(TAG, "El servicio se encuentra corriendo");
            RouteTrackerDialog dialog = new RouteTrackerDialog();
            dialog.show(getSupportFragmentManager(), "DIALOG_ROUTE");
        } else {
            activateRouteForVigilance();
            setServiceRunningInSettings();
        }
    }

    public void activateRouteForVigilance(){

        Intent routeIntent = new Intent(this, RouteTrackerService.class);
        Bundle extras = new Bundle();
        extras.putParcelable(Constants.RouteTraker.EXTRA_ROUTE, mRouteItemSelected);
        routeIntent.putExtras(extras);
        startService(routeIntent);

        Toast.makeText(this, R.string.vigilance_active, Toast.LENGTH_SHORT).show();
    }

    private boolean isRouteTrakingServiceRunning(){
        return settings.getBoolean(Constants.Preferences.IS_ROUTE_SERVICE_RUNNING, false);
    }

    public void setServiceRunningInSettings(){
        settings.edit()
                .putBoolean(Constants.Preferences.IS_ROUTE_SERVICE_RUNNING, true)
                .commit();
    }





    /* CREACIÓN AUTOMÁTICA DE RUTAS MEDIANTE GOOGLE DIRECTIONS ---------------------------------- */

    private void showDirectionsDialog(){
        GetDirectionsDialog dialog = new GetDirectionsDialog();
        dialog.show(this.getSupportFragmentManager(), Constants.Routes.DIALOG_GET_DIRECTIONS);
    }

    @Override
    public void onPositiveButtonSelected(String originAdress, String destinationAdress, String mode) {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("maps.googleapis.com")
                .appendPath("maps")
                .appendPath("api")
                .appendPath("directions")
                .appendPath("json?")
                .appendQueryParameter("origin", originAdress)
                .appendQueryParameter("destination", destinationAdress)
                .appendQueryParameter("mode", mode)
                .appendQueryParameter("key", "AIzaSyAgnPEKayPBxWk336_K8M1Kz1pBIPVx9ZA");

        String s = builder.build().toString().replace("%20", "+").replace("%3F", "");

        Log.v(TAG, s);
        requestGoogleRoute(s);
    }

    private void requestGoogleRoute(String uri ){
        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.GET,
                null,
                Constants.ApiRequestCode.GET_GOOGLE_DIRECTIONS,
                null
        );
    }

    private void decodeRoute(String response){
        try {
            JSONObject routeObject = new JSONObject(response);
            JSONArray routes = routeObject.getJSONArray("routes");
            JSONObject route = routes.getJSONObject(0);
            JSONObject overviewPolylines = route
                    .getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            mGoogleRoute = decodePoly(encodedString);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showRoute(){
        Intent showRoute = new Intent(this, DisplayRouteActivity.class);
        showRoute.putParcelableArrayListExtra(Constants.Routes.EXTRA_ROUTE, mGoogleRoute);
        startActivity(showRoute);
    }

    private ArrayList<LatLng> decodePoly(String encoded) {

        Log.i(TAG, "String received: " + encoded);
        ArrayList<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),(((double) lng / 1E5)));
            poly.add(p);
        }

        for(int i=0;i<poly.size();i++){
            Log.i(TAG, "Point sent: Latitude: "+poly.get(i).latitude+" Longitude: "+poly.get(i).longitude);
        }

        return poly;
    }



    /* INTERFAZ DE COMUNICACIÒN CON EL SERVER --------------------------------------------------- */

    @Override
    public void onResponse(int requestCode, String result) {

        Log.d(TAG, result);

        if(requestCode != Constants.ApiRequestCode.GET_GOOGLE_DIRECTIONS
                && getResponseStatus(result) != 200){

            switch(requestCode){
                case Constants.ApiRequestCode.AUTHENTICATE_PIN:
                    //JONI: HUBO ALGÚN PROBLEMA AL AUTENTICARSE POR PIN
                    //(NO COINCIDE EL PIN INGRESADO CON EL DE LA BD)
                    if(!flagautenticado){
                        Toast.makeText(MainMapActivity.this, "Intentelo nuevamente!", Toast.LENGTH_SHORT).show();
                        flagdialog=false; //Necesario para que cree un dialog nuevo.
                        onResume();
                    }
                    break;
                case Constants.ApiRequestCode.AUTHENTICATE_TAG:
                    //JONI: HUBO ALGÚN PROBLEMA AL AUTENTICARSE POR TAG
                    //(NO COINCIDE EL TAG INGRESADO CON EL DE LA BD)
                    AlertDialog.Builder ventana = new AlertDialog.Builder(this);
                    ventana.setMessage("Ingresar PIN o acercar Tag NFC");
                    ventana.setTitle("Autenticación");
                    textopin = new EditText(this);
                    ventana.setView(textopin);
                    ventana.setCancelable(false);
                    flagdialog=true;
                    ventana.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String pin = textopin.getText().toString().trim();
                            authenticateWithPin(pin);
                        }
                    });
                    ventana.show();
                    break;
            }

            return;
        }

        switch (requestCode){
            case Constants.ApiRequestCode.ADD_DESTINATION:
                //Mostramos el mensaje al usuario
                Snackbar.make(mSnackbar, R.string.snackbar_destination_added, Snackbar.LENGTH_LONG)
                        .show();
                break;
            case Constants.ApiRequestCode.DELETE_DESTINATION:
                Snackbar.make(mSnackbar, R.string.snackbar_destination_deleted, Snackbar.LENGTH_LONG)
                        .show();
                onBackPressed();
                break;
            case Constants.ApiRequestCode.ADD_NEW_ROUTE:
                //Muestro el mensaje al usuario
                Snackbar.make(mSnackbar, R.string.snackbar_route_added, Snackbar.LENGTH_LONG)
                        .show();
                break;
            case Constants.ApiRequestCode.DELETE_ROUTE:
                Snackbar.make(mSnackbar, R.string.snackbar_route_deleted, Snackbar.LENGTH_LONG)
                        .show();
                onBackPressed();
                break;
            case Constants.ApiRequestCode.REQUEST_CONTACTS_POSITION:
                Log.d(TAG, "Posiciones de Contactos: " + result);
                parseContactPosition(result);
                loadContactsPosition();
                break;
            case Constants.ApiRequestCode.GET_GOOGLE_DIRECTIONS:
                decodeRoute(result);
                showRoute();
                break;
            case Constants.ApiRequestCode.AUTHENTICATE_PIN:
                //JONI: EL USUARIO SE AUTENFICIÓ CORRECTAMENTE POR SU PIN
                flagautenticado = true;
                flagdialog=false;
                break;
            case Constants.ApiRequestCode.AUTHENTICATE_TAG:
                //JONI: EL USUARIO SE AUTENFICIÓ CORRECTAMENTE POR SU TAG
                if(flagdialog){
                    textopin.setText("Cuenta autenticada! Presione aceptar!");
                    textopin.setEnabled(false);
                    Log.d(TAG, "lo autentico por aca");
                }
                flagautenticado=true;
                flagdialog=false;
                break;
            case Constants.ApiRequestCode.UPDATE_TAG:
                Toast.makeText(this, "Tag actualizado correctamente", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Snackbar.make(mSnackbar, R.string.server_error, Snackbar.LENGTH_LONG).show();

        switch (requestCode){
            case Constants.ApiRequestCode.AUTHENTICATE_PIN:
                //JONI: HUBO ALGÚN PROBLEMA AL AUTENTICARSE POR PIN
                //(NORMALMENTE ERROR DE CONEXIÓN CON LA API)
                if(!flagautenticado){
                    Toast.makeText(MainMapActivity.this, "Intentelo nuevamente!", Toast.LENGTH_SHORT).show();
                    flagdialog=false; //Necesario para que cree un dialog nuevo.
                    onResume();
                }
                break;
            case Constants.ApiRequestCode.AUTHENTICATE_TAG:
                //JONI: HUBO ALGÚN PROBLEMA AL AUTENTICARSE POR TAG
                //(NORMALMENTE ERROR DE CONEXIÓN CON LA API)
                if(!flagautenticado){
                    Toast.makeText(MainMapActivity.this, "Intentelo nuevamente!", Toast.LENGTH_SHORT).show();
                    flagdialog=false; //Necesario para que cree un dialog nuevo.
                    onResume();
                }
                break;
        }

        Log.d(TAG, "Ooops, hubo algún problema. Error " + statusCode);
    }

    private int getResponseStatus(String result){

        Log.d(TAG, "getResponseStatus: " + result);

        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }






    /* PARA CHEACKEAR SI EL USUARIO TIENE ACTIVA LA LOCALIZACIÓN -------------------------------- */

    /**
     * Creamos una instancia de GoogleApiClient que contiene la información de las APIs
     * que necesitamos
     */
    private synchronized GoogleApiClient buildGoogleApiClient() {
        return new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Crea el objeto que contiene los datos para recibir actualizaciones de posicion
     */
    private LocationRequest createLocationRequest() {
        return new LocationRequest()
                .setInterval(Constants.RouteTraker.TIME_INTERVAL)
                .setFastestInterval(Constants.RouteTraker.FASTEST_TIME_INTERVAL)
                .setSmallestDisplacement(Constants.RouteTraker.SMALLEST_DISPLACEMENT)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Dispara una solicitud para checkear si se encuentra activa algún método de localización
     */
    private void checkLocationSettings() {

        GoogleApiClient apiClient = buildGoogleApiClient();
        apiClient.connect();
        LocationRequest locationRequest = createLocationRequest();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
                .setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(apiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates states = result.getLocationSettingsStates();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startRouteTrakingService();
                        Log.d(TAG, "SUCCESS");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.d(TAG, "RESOLUTION_REQUIRED");
                        try {
                            status.startResolutionForResult(
                                    MainMapActivity.this,
                                    Constants.RouteTraker.REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.d(TAG, "settings_change_UNAVAILABLE");
                        break;
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);

        switch (requestCode) {
            case Constants.RouteTraker.REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "RESULT_OK");
                        startRouteTrakingService();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.d(TAG, "RESULT_CANCELLED");
                        //Mostrar un cartel que le informe al usuario que sin eso no puede funcionar
                        break;
                    default:
                        break;
                }
                break;
        }
    }






    /* AUTENTICACIÓN DEL USUARIO CON EL PIN/TAG ------------------------------------------------- */

    private void authenticateWithPin(String pin){

        int userId = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.AUTHENTICATE_PIN, userId);

        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.AUTHENTICATE_PIN,
                getPinJSON(pin));

    }

    private String getPinJSON(String pin) {

        JSONObject object = new JSONObject();

        try {
            object.put("pin", pin);

            Log.d(TAG, object.toString());

            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void authenticateWithTag(String tag){

        int userId = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.AUTHENTICATE_TAG, userId);

        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.AUTHENTICATE_TAG,
                getTagJSON(tag));
    }

    private String getTagJSON(String tag) {

        JSONObject object = new JSONObject();

        try {
            object.put("tag", tag);

            Log.d(TAG, object.toString());

            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void updateTag(String newTag){

        int userId = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.UPDATE_TAG, userId);

        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.PUT,
                null,
                Constants.ApiRequestCode.UPDATE_TAG,
                getNewTagJSON(newTag));

    }

    private String getNewTagJSON(String newTag){
        JSONObject object = new JSONObject();

        try {
            object.put("new_tag", newTag);

            Log.d(TAG, object.toString());

            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


}