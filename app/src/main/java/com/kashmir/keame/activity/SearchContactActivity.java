package com.kashmir.keame.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kashmir.keame.R;
import com.kashmir.keame.adapter.SearchResultsAdapter;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.controller.RestApiConnection;
import com.kashmir.keame.dialog.ConfirmFriendRequestDialog;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Contact;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchContactActivity extends AppCompatActivity implements
        SearchResultsAdapter.OnItemListClickListener, Data.OnQueryListener,
        ConfirmFriendRequestDialog.OnDialogButtonClick {

    private static final String TAG = SearchContactActivity.class.getSimpleName();

    Toolbar mToolbar;

    private ProgressBar mProgressBar;
    private View mMessage;

    private RecyclerView mSearchResults;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Contact> mContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContacts = new ArrayList<>();

        setContentView(R.layout.activity_search_contact);

        //Configurando la lista
        mSearchResults = (RecyclerView) findViewById(R.id.search_results);
        mSearchResults.setHasFixedSize(true);
        mSearchResults.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this).build());

        mLayoutManager = new LinearLayoutManager(this);
        mSearchResults.setLayoutManager(mLayoutManager);

        mAdapter = new SearchResultsAdapter(mContacts, this);
        mSearchResults.setAdapter(mAdapter);

        //Para mostrar la lista de vacíos
        mMessage = findViewById(R.id.empty_message);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        setUpToolbar();
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            Log.d(TAG, "Buscaste: " + query);
            getContactsStartingWith(query);
        }
    }


    public void getContactsStartingWith(String toSearch){
        String url = "https://keame.herokuapp.com/api/v1/users/1/contacts/search_by_name/?term=" +
                toSearch;

        StringRequest jsonRequest = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        response = response.replaceAll("\\\\", "");
                        response = response.substring(1, response.length() - 1);

                        Log.d(TAG, "Recibi " + response);

                        try {
                            JSONObject object = new JSONObject(response);
                            JSONArray users = object.getJSONArray("users");

                            for(int i = 0; i < users.length(); i++ ){
                                mContacts.add(new Contact(users.getJSONObject(i), 0));
                            }

                            updateUI();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        updateUI();
                        Log.d(TAG, "Hubo un error al realizar la query");
                    }
                }
        );

        RestApiConnection.getInstance(this).addToRequestQueue(jsonRequest);
    }



    /* UTILITY FUNCTIONS ------------------------------------------------------------------------ */
    private void setUpToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onListItemClick(int position) {
        showConfirmationDialog(position);
    }

    private void showConfirmationDialog(int position) {
        ConfirmFriendRequestDialog dialog = ConfirmFriendRequestDialog.newInstance(
                position,
                mContacts.get(position).getFullName()
        );

        dialog.show(getSupportFragmentManager(), Constants.Contacts.CONFIRM_SEND_REQUEST_DIALOG);
    }

    @Override
    public void onResponse(int requestCode, String result) {

        Log.d(TAG, result);

        switch (requestCode){
            case Constants.ApiRequestCode.SEND_FRIEND_REQUEST:
                if(getResponseStatus(result) == 200){
                    Toast.makeText(this, "Se agrego un contacto", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Toast.makeText(this, "Status Code: " + statusCode, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPositiveButtonClick(int position) {
        sendFriendRequest(position);
        mContacts.remove(position);
        mAdapter.notifyItemRemoved(position);
        updateUI();
    }

    private void updateUI() {

        if( mContacts.size() == 0){
            mMessage.setVisibility(View.VISIBLE);
        } else {
            mAdapter.notifyDataSetChanged();
            mMessage.setVisibility(View.GONE);
        }

        mProgressBar.setVisibility(View.GONE);
    }

    private void sendFriendRequest(int position){
        Data.query(this,
                this,
                Constants.ApiRequestCode.SEND_FRIEND_REQUEST,
                String.valueOf(mContacts.get(position).getId())
        );
    }

    private int getResponseStatus(String result){
        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }
}
