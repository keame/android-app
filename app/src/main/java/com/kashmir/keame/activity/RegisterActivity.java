package com.kashmir.keame.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.kashmir.keame.R;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.service.FollowingService;
import com.kashmir.keame.service.RegistrationIntentService;
import com.kashmir.keame.service.RequestDestinationsService;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity implements Data.OnQueryListener{

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private Button mRegisterButton;
    private Toolbar mToolbar;
    private SharedPreferences settings;

    private EditText mMail;
    private EditText mPassword;
    private EditText mRepeatPassword;
    private EditText mFirstName;
    private EditText mLastName;
    private EditText mTelephoneNumber;
    private EditText mPin;

    private String email;
    private String password;
    private String repeatedPassword;
    private String firstName;
    private String lastName;
    private String telephoneNumber;
    private String pin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Obtengo el archivo de preferencias
        settings = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE);

        getViews();
        setUpListeners();
        setUpToolbar();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void getViews(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mMail = (EditText) findViewById(R.id.emailField);
        mPassword = (EditText) findViewById(R.id.passwordField);
        mRepeatPassword = (EditText) findViewById(R.id.repeatPasswordField);
        mFirstName = (EditText) findViewById(R.id.firstNameField);
        mLastName = (EditText) findViewById(R.id.lastNameField);
        mTelephoneNumber = (EditText) findViewById(R.id.telephoneNumberField);
        mPin = (EditText) findViewById(R.id.pinField);
        mRegisterButton = (Button) findViewById(R.id.registerButton);
    }

    private void setUpToolbar(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpListeners(){
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isComplete = true;
                boolean passwordsAreEqual = true;

                //Obtengo los datos de los campos
                email = mMail.getText().toString().trim();
                password = mPassword.getText().toString().trim();
                repeatedPassword = mRepeatPassword.getText().toString().trim();
                firstName = mFirstName.getText().toString().trim();
                lastName = mLastName.getText().toString().trim();
                telephoneNumber = mTelephoneNumber.getText().toString().trim();
                pin = mPin.getText().toString().trim();

                //Valido que todos estén correctos
                if (email.equalsIgnoreCase("") || password.equalsIgnoreCase("")
                        || repeatedPassword.equalsIgnoreCase("") || firstName.equalsIgnoreCase("")
                        || lastName.equalsIgnoreCase("") || telephoneNumber.equalsIgnoreCase("")
                        || pin.equalsIgnoreCase("")) {
                    isComplete = false;

                    Toast.makeText(getBaseContext(),
                            "Por favor, completá todos los campos",
                            Toast.LENGTH_SHORT)
                            .show();
                }

                //Chequeo que las password son iguales
                if (isComplete && !password.equals(repeatedPassword)) {
                    passwordsAreEqual = false;
                    Toast.makeText(getBaseContext(),
                            "Las contraseñas no coinciden", Toast.LENGTH_SHORT)
                            .show();
                }

                //Si no hubo errores, envío al servidor
                if (isComplete && passwordsAreEqual) {
                    sendRegisterRequest();
                }
            }
        });
    }


    private void sendRegisterRequest(){
        String uri = Constants.Uri.REGISTER_USER;

        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.REGISTER_USER,
                getJSONString()
        );
    }

    private String getJSONString(){
        JSONObject newUser = new JSONObject();
        try {
            newUser.put(Constants.Profile.EMAIL, email);
            newUser.put(Constants.Profile.FIRST_NAME, firstName);
            newUser.put(Constants.Profile.LAST_NAME, lastName);
            newUser.put(Constants.Profile.PASSWORD, password);
            newUser.put(Constants.Profile.TELEPHONE, telephoneNumber);
            newUser.put(Constants.Profile.PIN, pin);

            Log.d(TAG, newUser.toString());

            return newUser.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }



    /* PARA PROCESAR LA RESPUESTA DEL SERVIDOR -------------------------------------------------- */

    @Override
    public void onResponse(int requestCode, String result) {
        Log.d(TAG, result);

        try {
            JSONObject object = new JSONObject(result);
            int status = object.getInt("status");

            switch (status){
                case 200:
                    if(requestCode == Constants.ApiRequestCode.REGISTER_USER){
                        updatePreferences(object.getInt("id"));
                        registerDevice();
                        startMainActivity();

                        //Buscamos los destinos para poder disparar los geofences
                        RequestDestinationsService.handleDestinationRequest(this);

                        //Disparamos el servicio de Tracking
                        FollowingService.startTrackerService(this);
                    }
                    break;
                case 500:
                    String error = object.getString("error");
                    Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
                    break;

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Toast.makeText(this,
                R.string.server_error, Toast.LENGTH_SHORT).show();

        Log.d(TAG, "Error code:" + statusCode);
    }






    /* OTRAS FUNCIONES -------------------------------------------------------------------------- */

    /**
     * Actualiza la informaciòn del usuario en las preferencias, asì como también, indicando que
     * es la primera vez que ingresa el usuario a la aplicaciòn, para mostrar la ayuda
     *
     * @param id El nuevo Id del usuario
     */
    private void updatePreferences(int id){
        settings.edit()
                .putInt(Constants.Preferences.CURRENT_USER_ID, id)
                .putString(Constants.Preferences.CURRENT_USER_FIRST_NAME, firstName)
                .putString(Constants.Preferences.CURRENT_USER_LAST_NAME, lastName)
                .putString(Constants.Preferences.CURRENT_USER_MAIL, email)
                .putString(Constants.Preferences.CURRENT_USER_TELEPHONE, telephoneNumber)
                .putBoolean(Constants.Preferences.IS_USER_LOGGED_IN, true)
                .putBoolean(Constants.Preferences.IS_USER_FIRST_TIME, true)
                .commit();
    }

    /**
     * Un vez que se completa exitosamente el registro, se lo lleva al usuario a la pantalla
     * principal
     */
    public void startMainActivity(){
        Intent intent = new Intent(this, MainMapActivity.class);
        startActivity(intent);
        this.finish();
    }

    /**
     * Registrar el dispositivo para permitir recibir notificaciones
     */
    private void registerDevice(){
        if (checkPlayServices()) {
            //Getting current user id
            int mCurrentUserId = settings.getInt(Constants.Preferences.CURRENT_USER_ID, -1);

            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            intent.putExtra(Constants.Registration.CURRENT_USER_ID, mCurrentUserId);
            startService(intent);
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
