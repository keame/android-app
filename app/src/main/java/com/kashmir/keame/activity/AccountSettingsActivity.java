package com.kashmir.keame.activity;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.kashmir.keame.R;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountSettingsActivity extends AppCompatActivity
        implements Data.OnQueryListener{

    private SharedPreferences settings;
    private int mCurrentUserId;
    private Toolbar mToolbar;
    private EditText mFirstName;
    private EditText mLastName;
    private EditText mEmail;
    private EditText mTelephone;

    private String firstName;
    private String lastName;
    private String email;
    private String telephone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);

        settings = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE);
        mCurrentUserId = settings.getInt(Constants.Preferences.CURRENT_USER_ID, -1);
        firstName = settings.getString(Constants.Preferences.CURRENT_USER_FIRST_NAME, "Nombre");
        lastName = settings.getString(Constants.Preferences.CURRENT_USER_LAST_NAME, "Apellido");
        email = settings.getString(Constants.Preferences.CURRENT_USER_MAIL, "usuario@ejemplo.com");
        telephone = settings.getString(Constants.Preferences.CURRENT_USER_TELEPHONE, "Teléfono");

        setViews();
        setUpToolbar();
    }

    private void setViews(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mFirstName = (EditText) findViewById(R.id.account_first_name);
        mFirstName.setText(firstName);
        mLastName = (EditText) findViewById(R.id.account_last_name);
        mLastName.setText(lastName);
        mEmail = (EditText) findViewById(R.id.account_email);
        mEmail.setText(email);
        mTelephone = (EditText) findViewById(R.id.account_telephone);
        mTelephone.setText(telephone);
    }

    private void setUpToolbar(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ajustes de cuenta");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_save:
                updateSettings();
                return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_account_settings, menu);
        return true;
    }

    private void updateSettings(){

        firstName = mFirstName.getText().toString().trim();
        lastName = mLastName.getText().toString().trim();
        email = mEmail.getText().toString().trim();
        telephone = mTelephone.getText().toString().trim();

        String uri = String.format(Constants.Uri.UPDATE_USER_CONFIG, mCurrentUserId);

        Log.d("Settings", uri);

        //Enviar al servidor
        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.PUT,
                null,
                Constants.ApiRequestCode.UPDATE_USER_CONFIG,
                getJSONString()
        );
    }

    private String getJSONString(){

        try {
            JSONObject newSettings = new JSONObject();
            newSettings.put("id", mCurrentUserId);
            newSettings.put("email", email);
            newSettings.put("telephone_number", telephone);
            newSettings.put("first_name", firstName);
            newSettings.put("last_name", lastName);

            return newSettings.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onResponse(int requestCode, String result) {
        Log.d("Settings", result);

        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");
            if(status == 200){
                settings.edit()
                        .putString(Constants.Preferences.CURRENT_USER_FIRST_NAME, firstName)
                        .putString(Constants.Preferences.CURRENT_USER_LAST_NAME, lastName)
                        .putString(Constants.Preferences.CURRENT_USER_TELEPHONE, telephone)
                        .putString(Constants.Preferences.CURRENT_USER_MAIL, email)
                        .apply();

                Toast.makeText(this, "Preferencias cambiadas correctamente", Toast.LENGTH_SHORT)
                        .show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {

    }
}
