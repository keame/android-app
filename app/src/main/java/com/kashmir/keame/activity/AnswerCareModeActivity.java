package com.kashmir.keame.activity;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.service.SendAlertService;

import java.util.Random;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AnswerCareModeActivity extends AppCompatActivity {

    private static final String TAG = AnswerCareModeActivity.class.getSimpleName();

    private static final String ARG_A = "arg_a";
    private static final String ARG_B = "arg_b";

    private TextView mQuestionTextView;
    private Button mAcceptButton;
    private EditText mAnswerEditText;

    private int a;
    private int b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_care_mode);

        //Inicializo los componentes visuales
        initComponents();

        //Si se llega a rotar la pantalla o la activity vuelve de haber sido pausada, recupero
        //los números ya calculados. De otra forma, los calculo por primera vez.

        if(savedInstanceState != null){
            a = savedInstanceState.getInt(ARG_A);
            b = savedInstanceState.getInt(ARG_B);
        } else {
            getRandomNumbers();
        }

        String question = String.format(getResources().getString(R.string.care_mode_text), a, b);
        mQuestionTextView.setText(question);

        mAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Obtengo la respuesta ingresada
                int answer = Integer.parseInt(mAnswerEditText.getText().toString().trim());

                //Comparo lo escrito por el usuario con lo que deberia dar
                if (answer != a + b) {
                    Log.d(TAG, "Lanzando alerta");
                    sendAlert();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Respuesta correcta! Perdoná la molestia",
                            Toast.LENGTH_SHORT).show();
                }

                closeActivity();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sendAlert();
        closeActivity();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(ARG_A, a);
        outState.putInt(ARG_B, b);

        super.onSaveInstanceState(outState);
    }

    private void sendAlert() {
        //Informamos al usuario
        Toast.makeText(getApplicationContext(), "Alerta enviada", Toast.LENGTH_SHORT).show();

        //Enviamos la alerta
        SendAlertService.sendAlert(getApplicationContext());
    }

    private void initComponents() {
        mQuestionTextView = (TextView) findViewById(R.id.questionTextView);
        mAnswerEditText = (EditText) findViewById(R.id.answerEditText);
        mAcceptButton = (Button) findViewById(R.id.acceptButton);
    }

    private void getRandomNumbers() {
        Random random = new Random();

        a = random.nextInt(1000);
        b = random.nextInt(1000);
    }

    private void closeActivity() {
        //Elimino la notificación
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(Constants.RouteTraker.NOTIFICATION_CARE_ID);

        //Elimino la alarma para que no envíe la alerta nuevamente
        Intent intent = SendAlertService.getCancelIntent(this);
        PendingIntent pIntent = PendingIntent.getService(this, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pIntent);

        //Finalizo la activity
        finish();
    }


    /* PARA CUSTOMIZAR LA FUENTE ---------------------------------------------------------------- */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
