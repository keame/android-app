package com.kashmir.keame.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.kashmir.keame.R;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.service.FollowingService;
import com.kashmir.keame.service.RegistrationIntentService;
import com.kashmir.keame.service.RequestDestinationsService;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener, Data.OnQueryListener {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private String username;
    private String password;

    private Button loginButton;
    private EditText mUsernameField;
    private EditText mPasswordField;
    private SharedPreferences settings;
    private GoogleApiClient mGoogleApiClient;
    private SignInButton mSignInGoogle;

    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;

    /* Should we automatically resolve ConnectionResults when possible? */
    private boolean mShouldResolve = false;

    /* RequestCode for resolutions involving sign-in */
    private static final int RC_SIGN_IN = 9001;

    /* Keys for persisting instance variables in savedInstanceState */
    private static final String KEY_IS_RESOLVING = "is_resolving";
    private static final String KEY_SHOULD_RESOLVE = "should_resolve";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Obtengo una referencia a las preferencias. Si el usuario se encuentra logueado,
        //entonces lo llevo al inicio. Si no est� logueado, caego la interfaz de startMainActivity.
        settings = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE);

        //Conecto con los servicios de Google
        buildGoogleApiClient();

        if(userIsLoggedIn()){
            //Lanzo la pantalla de Inicio y modifico preferencias
            Log.d(TAG, "El usuario ya esta logueado.");
            startMainActivity();
        } else {
            Log.d(TAG, "El usuario NO esta logueado.");

            //Inicializo la interfaz
            setContentView(R.layout.activity_login);

            mUsernameField = (EditText) findViewById(R.id.usernameField);
            mPasswordField = (EditText) findViewById(R.id.passwordField);

            loginButton = (Button) findViewById(R.id.loginButton);
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getLoginData();
                }
            });

            //mSignInGoogle = (SignInButton) findViewById(R.id.sign_in_with_google);
            //mSignInGoogle.setOnClickListener(this);
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }


    /* UTILITY FUNCTIONS -------------------------------------------------------------------------*/
    public void startMainActivity(){
        Intent intent = new Intent(this, MainMapActivity.class);
        startActivity(intent);
        this.finish();
    }

    private void getLoginData(){
        username = mUsernameField.getText().toString().trim();
        password = mPasswordField.getText().toString();

        //Validar algo
        requestLogin();

        Log.d(TAG, "Usuario: " + username + " Contraseña: " + password);
    }

    private void requestLogin(){
        Data.query(
                this,
                this,
                Constants.ApiRequestCode.LOGIN,
                username,
                password);
    }

    /**
     * Checks the state of the session, saved in the preference files of the application
     * @return true if the user is currently logged in, otherwise false
     */
    private boolean userIsLoggedIn(){
        return settings
                .getBoolean(Constants.Preferences.IS_USER_LOGGED_IN, false);
    }

    /**
     *
     */
    private void changeSessionState(){
        settings.edit()
                .putBoolean(Constants.Preferences.IS_USER_LOGGED_IN, true)
                .commit();
    }

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.
        Log.d(TAG, "onConnected:" + bundle);
        mShouldResolve = false;

        // Show the signed-in UI
        showSignedInUI();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Nothing to do here :)
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);

        if (!mIsResolving && mShouldResolve) {
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(this, RC_SIGN_IN);
                    mIsResolving = true;
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, "Could not resolve ConnectionResult.", e);
                    mIsResolving = false;
                    mGoogleApiClient.connect();
                }
            } else {
                // Could not resolve the connection result, show the user an
                // error dialog.
                showErrorDialog(connectionResult);
            }
        } else {
            // Show the signed-out UI
            showSignedOutUI();
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);

        if (requestCode == RC_SIGN_IN) {
            // If the error resolution was not successful we should not resolve further.
            if (resultCode != RESULT_OK) {
                mShouldResolve = false;
            }

            mIsResolving = false;
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onClick(View v) {
        /*if(v.getId() == R.id.sign_in_with_google){
            onSignInClicked();
        }*/
    }

    private void onSignInClicked(){
        // User clicked the sign-in button, so begin the sign-in process and automatically
        // attempt to resolve any errors that occur.
        mShouldResolve = true;
        mGoogleApiClient.connect();

        // Show a message to the user that we are signing in.
        //mStatus.setText(R.string.signing_in);
    }

    private void showSignedInUI() {
        updateUI(true);
    }

    private void showSignedOutUI() {
        updateUI(false);
    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            // Show signed-in user's name
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            if (currentPerson != null) {
                String name = currentPerson.getDisplayName();
                Log.d(TAG, "Hola, " + name + "!");
            } else {
                Log.w(TAG, "No exist�s, vieja");
                //mStatus.setText(getString(R.string.signed_in_err));
            }

            // Set button visibility
            //findViewById(R.id.sign_in_with_google).setVisibility(View.GONE);
            //findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            // Show signed-out message
            //mStatus.setText(R.string.signed_out);

            // Set button visibility
            //findViewById(R.id.sign_in_with_google).setEnabled(true);
            //findViewById(R.id.sign_in_with_google).setVisibility(View.VISIBLE);
            //findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }

    private void showErrorDialog(ConnectionResult connectionResult) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, RC_SIGN_IN,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                mShouldResolve = false;
                                showSignedOutUI();
                            }
                        }).show();
            } else {
                Log.w(TAG, "Google Play Services Error:" + connectionResult);
                String errorString = apiAvailability.getErrorString(resultCode);
                Toast.makeText(this, errorString, Toast.LENGTH_SHORT).show();

                mShouldResolve = false;
                showSignedOutUI();
            }
        }
    }




    /* SERVER RESPONSE -------------------------------------------------------------------------- */

    @Override
    public void onResponse(int requestCode, String result) {
        Log.d(TAG, result);

        switch (requestCode){
            case Constants.ApiRequestCode.LOGIN:
                parseLoginResponse(result);
                break;
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {

    }

    private void parseLoginResponse(String result){

        try {
            JSONObject jsonResponse = new JSONObject(result);
            JSONObject info;
            int status = jsonResponse.getInt("status");

            switch (status){
                case 200:
                    info = jsonResponse.getJSONObject("info");
                    int id = info.getInt("id");
                    String firstName = info.getString("first_name");
                    String lastName = info.getString("last_name");
                    String email = info.getString("email");
                    String telephone = info.getString("telephone_number");

                    updatePreferences(id, firstName, lastName, email, telephone);
                    changeSessionState();
                    registerDevice();
                    startMainActivity();

                    //Buscamos los destinos para poder disparar los geofences
                    RequestDestinationsService.handleDestinationRequest(this);

                    //Disparamos el servicio de Tracking
                    FollowingService.startTrackerService(this);
                    break;
                case 500:
                    String error = jsonResponse.getString("error");
                    Toast.makeText(
                            this,
                            "Usuario o contraseña incorrectos",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void updatePreferences(int id, String first, String last, String email, String phone){

        boolean isFirstTime = !settings.contains(Constants.Preferences.IS_USER_FIRST_TIME);

        settings.edit()
                .putInt(Constants.Preferences.CURRENT_USER_ID, id)
                .putString(Constants.Preferences.CURRENT_USER_FIRST_NAME, first)
                .putString(Constants.Preferences.CURRENT_USER_LAST_NAME, last)
                .putString(Constants.Preferences.CURRENT_USER_MAIL, email)
                .putString(Constants.Preferences.CURRENT_USER_TELEPHONE, phone)
                .putBoolean(Constants.Preferences.IS_USER_FIRST_TIME, isFirstTime)
                .apply();
    }

    private void registerDevice(){
        if (checkPlayServices()) {
            //Getting current user id
            int mCurrentUserId = settings.getInt(Constants.Preferences.CURRENT_USER_ID, -1);

            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            intent.putExtra(Constants.Registration.CURRENT_USER_ID, mCurrentUserId);
            startService(intent);
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


}