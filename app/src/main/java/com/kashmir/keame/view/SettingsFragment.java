package com.kashmir.keame.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.kashmir.keame.R;
import com.kashmir.keame.activity.AccountSettingsActivity;
import com.kashmir.keame.activity.MainMapActivity;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Lorena Soledad on 26/09/2015.
 */
public class SettingsFragment extends PreferenceFragmentCompat
        implements Data.OnQueryListener{

    private int mCurrentUserId;
    //-NFC
    private Activity mCallingActivity;
    private static final String TAG = SettingsFragment.class.getSimpleName(); //Para pruebas.

    Preference mChangePassword;
    EditTextPreference pinField;
    Preference accountPreference;
    SwitchPreferenceCompat invisibleMode;
    //-NFC
    Preference añadirTagPreference;
    Preference borrarTagPreference;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences);

        mCurrentUserId = getContext().getSharedPreferences
                (Constants.Preferences.PREFERENCES_FILE, Context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        //Cambio de detalles de cuenta
        accountPreference = (Preference) findPreference("account");
        accountPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent accountSettings = new Intent(getActivity(), AccountSettingsActivity.class);
                startActivity(accountSettings);
                return false;
            }
        });

        //-NFC
        //Añadir Tag
        añadirTagPreference = (Preference) findPreference("añadir_tag");
        añadirTagPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (mCallingActivity instanceof MainMapActivity) {
                    MainMapActivity m = (MainMapActivity) mCallingActivity;
                    m.setFlagañadirtag(true);
                    m.setFlagborrartag(false);
                }
                return true;
            }
        });

        //-NFC
        //Borrar Tag
        borrarTagPreference = (Preference) findPreference("borrar_tag");
        borrarTagPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (mCallingActivity instanceof MainMapActivity) {
                    Log.d(TAG, "Paso --- 1");
                    MainMapActivity m = (MainMapActivity) mCallingActivity;
                    m.setFlagborrartag(true);
                    m.setFlagañadirtag(false);
                }
                Log.d(TAG, "Paso --- 2");
                return true;
            }
        });

        //Cambio de la contraseña
        mChangePassword = (Preference) findPreference("password");
        mChangePassword.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Log.d(TAG, "Preferencia de password");

                LayoutInflater inflater = getActivity().getLayoutInflater();

                //Instanciamos los elementos
                View root = inflater.inflate(R.layout.change_password, null);

                final EditText oldPasswordField = (EditText) root.findViewById(R.id.old_password);
                final EditText newPasswordField = (EditText) root.findViewById(R.id.new_password);
                final EditText repeatNewPasswordField = (EditText) root.findViewById(R.id.repeat_new_password);

                //Creamos el dialogo
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                builder.setView(root)
                        .setTitle("Cambiar password")
                        .setPositiveButton(R.string.dialog_positive_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String oldPassword = oldPasswordField.getText().toString().trim();
                                String newPassword = newPasswordField.getText().toString().trim();
                                String repeatedNewPassword = repeatNewPasswordField.getText().toString().trim();

                                updatePassword(oldPassword, newPassword, repeatedNewPassword);
                            }
                        })
                        .setNegativeButton(R.string.dialog_negative_button, null);

                builder.create().show();
                return false;
            }
        });

        //Switch del modo invisible
        invisibleMode = (SwitchPreferenceCompat) findPreference("is_invisible_mode_active");
        invisibleMode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Log.d("Settings", "Cambio el modo invisible");
                updateInvisibleMode();
                return true;
            }
        });

        //Cambio de la contraseña
        pinField = (EditTextPreference) findPreference("change_pin");
        pinField.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Log.d("SettingsFragment", "Se cambió el pin por " + (String) o);
                updatePIN((String) o);
                return true;
            }
        });
    }

    private void updatePIN(String newPin) {

        String uri = String.format(Constants.Uri.UPDATE_PIN, mCurrentUserId );

        Data.newRequest(
                getContext(),
                this,
                uri,
                Request.Method.PUT,
                null,
                Constants.ApiRequestCode.UPDATE_INVISIBLE_MODE,
                getPinJSON(newPin));
    }

    private String getPinJSON(String newPin) {

        JSONObject object = new JSONObject();

        try {
            object.put("new_pin", newPin);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    //-NFC
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallingActivity = activity;
    }

    private void updateInvisibleMode(){
        String uri = String.format(Constants.Uri.UPDATE_INVISIBLE_MODE, mCurrentUserId);

        HashMap<String, String> params = new HashMap();
        params.put("status", invisibleMode.isChecked() ? "down" : "up");

        Data.newRequest(
                getContext(),
                this,
                uri,
                Request.Method.PUT,
                params,
                Constants.ApiRequestCode.UPDATE_INVISIBLE_MODE,
                null);
    }

    private void updatePassword(String oldPassword, String newPassword, String repeatedPassword){
        String uri = String.format(Constants.Uri.UPDATE_PASSWORD, mCurrentUserId);

        Data.newRequest(
                getContext(),
                this,
                uri,
                Request.Method.PUT,
                null,
                Constants.ApiRequestCode.UPDATE_PASSWORD,
                getJSONString(oldPassword, newPassword, repeatedPassword));
    }

    private String getJSONString(String oldPassword, String newPassword, String repeatedPassword) {

        JSONObject object = new JSONObject();
        try {
            object.put("current_password", oldPassword);
            object.put("new_password", newPassword);
            object.put("new_password_confirmation", repeatedPassword);

            Log.d(TAG, object.toString());

            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onResponse(int requestCode, String result) {

        if( requestCode == Constants.ApiRequestCode.UPDATE_PASSWORD && getResponseStatus(result) != 200 ){
            Toast.makeText(getContext(), "Verifique las contraseñas", Toast.LENGTH_SHORT)
                    .show();
        } else {
            Toast.makeText(getContext(), "Preferencias modificadas correctamente", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        if(statusCode == Constants.ApiRequestCode.UPDATE_PASSWORD ){

        }
    }

    private int getResponseStatus(String result){

        Log.d(TAG, "getResponseStatus: " + result);

        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }
}
