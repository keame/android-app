package com.kashmir.keame.view;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.kashmir.keame.R;
import com.kashmir.keame.adapter.ContactsAdapter;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Contact;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactsFragment extends Fragment
        implements Data.OnQueryListener, View.OnClickListener {

    private static final String TAG = ContactsFragment.class.getSimpleName();

    private ListView mContactsList;
    private ProgressBar mProgressBar;
    private View mMessage;

    private ContactsAdapter mAdapter;
    private SwipeRefreshLayout mSwipeToRefresh;
    private ArrayList<Contact> mContacts;

    private int mCurrentUserId;

    //Para agregar, editar o eliminar un grupo
    private static int sTempItemIndex;
    private static String sTempItemName;

    private int mPositionToDelete;

    public ContactsFragment() {
        // Required empty public constructor
    }

    public static ContactsFragment newInstance(){
        ContactsFragment fragment = new ContactsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContacts = new ArrayList<Contact>();
        mAdapter = new ContactsAdapter(mContacts, getActivity(), this);

        mCurrentUserId = getActivity().getSharedPreferences
                (Constants.Preferences.PREFERENCES_FILE, Context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view;

        view = inflater.inflate(R.layout.contacts_list_fragment, container, false);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mMessage = view.findViewById(R.id.empty_list);

        //Seteamos el adaptador de la lista
        mContactsList = (ListView) view.findViewById(R.id.list_contacts);
        mContactsList.setAdapter(mAdapter);
        /*mContactsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mPositionToDelete = position;
                deleteContactWithId(String.valueOf(mContacts.get(position).getId()));
                return true;
            }
        });*/

        //Configuramos el swipe to Refresh
        mSwipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_to_refresh);
        mSwipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getContacts();
            }
        });

        getContacts();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        /*((MainMapActivity) getActivity()).getSupportActionBar().setTitle(
                getResources().getString(R.string.drawer_menu_contacts));
        ((MainMapActivity) getActivity()).getNavigationView()
                .getMenu().getItem(1).setChecked(true);*/
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        Log.d(TAG, "onSaveInstanceState");

        super.onSaveInstanceState(outState);
        outState.putInt(Constants.Contacts.STATE_CURRENT_USER_ID, mCurrentUserId);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        Log.d(TAG, "onActivityCreated()");

        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null){
            mCurrentUserId = savedInstanceState
                    .getInt(Constants.Contacts.STATE_CURRENT_USER_ID, mCurrentUserId);
        }
    }

    @Override
    public void onClick(final View v) {
        v.post(new Runnable() {
            @Override
            public void run() {
                showPopupMenu(v);
            }
        });
    }

    private void showPopupMenu(View view) {

        sTempItemIndex = (int) view.getTag();
        PopupMenu popup = new PopupMenu(getActivity(), view);
        popup.getMenuInflater().inflate(R.menu.popup_contact_item, popup.getMenu());

        if(mContacts.get(sTempItemIndex).isEmergencyContact()){
            popup.getMenu().getItem(3).setTitle(R.string.popup_contacts_emergency_remove);
        }

        // Set a listener so we are notified if a menu item is clicked
        popup.setOnMenuItemClickListener(new android.support.v7.widget.PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.popup_contacts_delete:
                        mPositionToDelete = sTempItemIndex;
                        deleteContactWithId(String.valueOf(mContacts.get(sTempItemIndex).getId()));
                        break;
                    case R.id.popup_contacts_state:
                        sendStateRequest();
                        break;
                    case R.id.popup_contacts_position:
                        sendPositionRequest();
                        break;
                    case R.id.popup_contacts_emergency:
                        if(mContacts.get(sTempItemIndex).isEmergencyContact()){
                            removeEmergencyContact();
                        }
                        else {
                            addEmergencyContacts();
                        }
                        break;
                }
                return false;
            }
        });

        popup.show();
    }

    private void sendStateRequest(){

        int userId = getActivity()
                .getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, Context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.SEND_STATE_REQUEST, userId);

        Log.d(TAG, uri);

        Data.newRequest(
                getActivity(),
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.SEND_STATE_REQUEST,
                getJSONString());
    }

    private String getJSONString() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("contact_id", mContacts.get(sTempItemIndex).getId());

            Log.d(TAG, jsonObject.toString());

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void sendPositionRequest(){
        /*Data.query(
                getActivity(),
                this,
                Constants.ApiRequestCode.SEND_POSITION_REQUEST,
                String.valueOf(mContacts.get(sTempItemIndex).getId())
        );*/

        int userId = getActivity()
                .getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, Context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.SEND_POSITION_REQUEST, userId);

        Log.d(TAG, uri);

        Data.newRequest(
                getActivity(),
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.SEND_POSITION_REQUEST,
                getJSONString());
    }


    /**
     * Para agregar un contacto a la lista de emergencia
     */
    private void addEmergencyContacts(){
        String uri = String.format(Constants.Uri.ADD_EMERGENCY_CONTACT, mCurrentUserId);
        Log.d(TAG, "Current User id: " + mCurrentUserId);

        HashMap<String, String> params = new HashMap<>();
        params.put("contact_id", String.valueOf(mContacts.get(sTempItemIndex).getId()));

        Data.newRequest(
                getActivity(),
                this,
                uri,
                Request.Method.POST,
                params,
                Constants.ApiRequestCode.ADD_EMERGENCY_CONTACT,
                null);
    }


    /**
     * Para quitar un contacto de la lista de emergencia
     */
    private void removeEmergencyContact(){
        String uri = String.format(Constants.Uri.REMOVE_EMERGENCY_CONTACT,
                mCurrentUserId, mContacts.get(sTempItemIndex).getId());

        Log.d(TAG, "Current User id: " + mCurrentUserId);

        Data.newRequest(
                getActivity(),
                this,
                uri,
                Request.Method.DELETE,
                null,
                Constants.ApiRequestCode.REMOVE_EMERGENCY_CONTACT,
                null);
    }




    private void populateContactsList(String result){

        Log.d(TAG, "Cargando contactos...");

        mContacts = new ArrayList<Contact>();

        try {
            JSONObject j = new JSONObject(result);
            JSONArray users = j.getJSONArray(Constants.Contacts.JSON_ARRAY_CONTACTS);

            for(int i = 0; i < users.length(); i++ ){
                mContacts.add(new Contact(users.getJSONObject(i), 0));
            }

            updateUI();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void updateUI() {
        if( mContacts.size() == 0){
            mMessage.setVisibility(View.VISIBLE);
        } else {
            mAdapter.updateList(mContacts);
            mMessage.setVisibility(View.GONE);
        }

        mProgressBar.setVisibility(View.GONE);
    }






    /* INTERFACE DE COMUNICACIÓN CON EL SERVIDOR ------------------------------------------------ */

    @Override
    public void onResponse(int requestCode, String result) {

        Log.d(TAG, result);

        if( getResponseStatus(result) != 200){
            Toast.makeText(getContext(),
                    "Oops, algo salió mal.",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if( getResponseStatus(result) == 200 ){
            switch (requestCode){
                case Constants.ApiRequestCode.GET_ALL_CONTACTS:
                    populateContactsList(result);
                    mSwipeToRefresh.setRefreshing(false);
                    break;
                case Constants.ApiRequestCode.DELETE_FRIEND:
                    mContacts.remove(mPositionToDelete);
                    mAdapter.updateList(mContacts);
                    updateUI();
                    Toast.makeText(getActivity(),
                            "Se elimino el contacto", Toast.LENGTH_SHORT).show();
                    break;
                case Constants.ApiRequestCode.SEND_STATE_REQUEST:
                    Toast.makeText(getActivity(),
                            "Se envió solicitud de Estado", Toast.LENGTH_SHORT).show();
                    break;
                case Constants.ApiRequestCode.SEND_POSITION_REQUEST:
                    Toast.makeText(getActivity(),
                            "Se envió solicitud de posición", Toast.LENGTH_SHORT).show();
                    break;
                case Constants.ApiRequestCode.ADD_EMERGENCY_CONTACT:
                    Toast.makeText(getActivity(),
                            "Se agregó un contacto de emergencia", Toast.LENGTH_SHORT).show();
                    mContacts.get(sTempItemIndex).setIsEmergencyContact(true);
                    break;
                case Constants.ApiRequestCode.REMOVE_EMERGENCY_CONTACT:
                    Toast.makeText(getActivity(),
                            "Se eliminó un contacto de emergencia", Toast.LENGTH_SHORT).show();
                    mContacts.get(sTempItemIndex).setIsEmergencyContact(false);
                    break;
            }
        }

    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        mSwipeToRefresh.setRefreshing(false);
        Log.d(TAG, "Status code: " + statusCode);

        Toast.makeText(getContext(),
                "Oops, algo salió mal.",
                Toast.LENGTH_SHORT).show();

        updateUI();
    }

    private int getResponseStatus(String result){
        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }






    /* QUERIES AL SERVIDOR ---------------------------------------------------------------------- */

    public void getContacts(){
        Data.query(
                getActivity(),
                this,
                Constants.ApiRequestCode.GET_ALL_CONTACTS
        );
    }

    public void deleteContactWithId(String contactId){
        Data.query(
                getActivity(),
                this,
                Constants.ApiRequestCode.DELETE_FRIEND,
                contactId);
    }
}
