package com.kashmir.keame.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;

import com.github.alexkolpa.fabtoolbar.FabToolbar;

/**
 * Created by Lorena Soledad on 19/08/2015.
 */
public class FabButtonBehavior extends CoordinatorLayout.Behavior<com.github.alexkolpa.fabtoolbar.FabToolbar> {

    public FabButtonBehavior(Context parent, AttributeSet attrs) {}

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, FabToolbar child, View dependency) {
        return dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, FabToolbar child, View dependency) {
        float translationY = Math.min(0, dependency.getTranslationY() - dependency.getHeight());
        child.setTranslationY(translationY);
        return true;
    }

    @Override
    public void onDependentViewRemoved(CoordinatorLayout parent, FabToolbar child, View dependency) {
        super.onDependentViewRemoved(parent, child, dependency);
        float translationY = Math.min(0, dependency.getTranslationY() - dependency.getHeight());
        ObjectAnimator anim = ObjectAnimator
                .ofFloat(child, "translationY", translationY)
                .setDuration(50);
        anim.start();
    }
}
