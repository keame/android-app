package com.kashmir.keame.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.kashmir.keame.R;
import com.kashmir.keame.activity.MainMapActivity;
import com.kashmir.keame.adapter.RoutesAdapter;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.dialog.GetDirectionsDialog;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Route;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class RoutesFragment extends Fragment implements Data.OnQueryListener,
        GetDirectionsDialog.OnDialogButtonSelected {


    /* PARA COMUNICARSE CON LA ACTIVITY PADRE --------------------------------------------------- */
    public interface OnRouteSelected {
        public void onRouteSelected(int position, Route route);
    }

    private static final String TAG = RoutesFragment.class.getSimpleName();

    private ListView routesList;
    private ProgressBar mProgressBar;
    private View mMessage;

    private RoutesAdapter mAdapter;
    private ArrayList<Route> mRoutes;
    private OnRouteSelected mCallback;

    //private ArrayList<LatLng> mGoogleRoute;

    //Para las animaciones
    private View fadeIn;
    private View fadeOut;

    public static RoutesFragment newInstance() {
        RoutesFragment fragment = new RoutesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public RoutesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mRoutes = new ArrayList<>();
        mAdapter = new RoutesAdapter(mRoutes, getActivity());
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();
        requestRoutes();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.routes_list_fragment, container, false);

        mMessage = view.findViewById(R.id.empty_list);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        routesList = (ListView) view.findViewById(R.id.list_routes);

        routesList.setAdapter(mAdapter);
        routesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {mCallback.onRouteSelected(position, mRoutes.get(position));
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainMapActivity) getActivity()).getmToolbar().setTitle(
                getResources().getString(R.string.drawer_menu_routes));
        ((MainMapActivity) getActivity()).getNavigationView()
                .getMenu().getItem(4).setChecked(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Para asegurarnos que la activity padre implementa la interfaz
        // Si no tiene se lanza una excepcion
        try {
            mCallback = (OnRouteSelected) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnRouteSelected");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }






    /* PARA PROCESAR LA RESPUESTA DEL SERVIDOR -------------------------------------------------- */

    @Override
    public void onResponse(int requestCode, String result) {

        switch (requestCode){
            case Constants.ApiRequestCode.GET_ALL_ROUTES:
                if( getResponseStatus(result) != 200 ){
                    Toast.makeText(getContext(),
                            "Oops, hubo un error",
                            Toast.LENGTH_SHORT).show();

                    updateUI();
                }
                populateRoutesList(result);
                break;
            /*case Constants.ApiRequestCode.GET_GOOGLE_DIRECTIONS:
                decodeRoute(result);
                showRoute();
                break;*/
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Toast.makeText(getContext(),
                "Oops, hubo un error",
                Toast.LENGTH_SHORT).show();

        updateUI();
    }





    /* REQUEST AL SERVER ------------------------------------------------------------------------ */

    private void requestRoutes(){
        Data.query(
                getActivity(),
                this,
                Constants.ApiRequestCode.GET_ALL_ROUTES
        );
    }

    private void requestGoogleRoute(String uri ){
        Data.newRequest(
                getActivity(),
                this,
                uri,
                Request.Method.GET,
                null,
                Constants.ApiRequestCode.GET_GOOGLE_DIRECTIONS,
                null
        );
    }





    /* OTRAS FUNCIONES -------------------------------------------------------------------------- */

    private int getResponseStatus(String result){
        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private void populateRoutesList(String result){

        mRoutes = new ArrayList<>();

        try {
            JSONObject response = new JSONObject(result);
            JSONArray routes =
                    response.getJSONArray("routes");

            for (int i = 0; i < routes.length(); i++){
                mRoutes.add(new Route(routes.getJSONObject(i)));
            }

            updateUI();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*private void updateUI() {
        if( mRoutes.size() == 0){
            mMessage.setVisibility(View.VISIBLE);
        } else {
            mAdapter.updateList(mRoutes);
            mMessage.setVisibility(View.GONE);
        }

        mProgressBar.setVisibility(View.GONE);
    }*/

    private void updateUI() {

        if( mRoutes.size() == 0 && !mMessage.isShown() && mProgressBar.isShown()){
            Log.d(TAG, "1");
            mAdapter.updateList(mRoutes);
            fadeIn = mMessage;
            fadeOut = mProgressBar;
        } else if( mRoutes.size() == 0 && !mMessage.isShown()) {
            Log.d(TAG, "2");
            fadeIn = mMessage;
            fadeOut = routesList;
        } else if(mRoutes.size() > 0 && mMessage.isShown()){
            Log.d(TAG, "3");
            mAdapter.updateList(mRoutes);
            fadeIn = routesList;
            fadeOut = mMessage;
        } else if(mRoutes.size() > 0 && mProgressBar.isShown()){
            Log.d(TAG, "4");
            mAdapter.updateList(mRoutes);
            fadeIn = routesList;
            fadeOut = mProgressBar;
        } else {
            Log.d(TAG, "5");
            mAdapter.updateList(mRoutes);
            return;
        }

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        fadeIn.setAlpha(0f);
        fadeIn.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        fadeIn.animate()
                .alpha(1f)
                .setDuration(300)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        fadeOut.animate()
                .alpha(0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        fadeOut.setVisibility(View.GONE);
                    }
                });
    }






    /* DIALOGO PARA OBTENER LAS DIRECCIONES DE ORIGEN Y DESTINO --------------------------------- */

    @Override
    public void onPositiveButtonSelected(String originAdress, String destinationAdress, String mode) {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("maps.googleapis.com")
                .appendPath("maps")
                .appendPath("api")
                .appendPath("directions")
                .appendPath("json?")
                .appendQueryParameter("origin", originAdress)
                .appendQueryParameter("destination", destinationAdress)
                .appendQueryParameter("mode", mode)
                .appendQueryParameter("key", "AIzaSyAgnPEKayPBxWk336_K8M1Kz1pBIPVx9ZA");

        String s = builder.build().toString().replace("%20", "+").replace("%3F", "");

        Log.v(TAG, s);
        requestGoogleRoute(s);
    }
}
