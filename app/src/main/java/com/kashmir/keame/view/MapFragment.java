package com.kashmir.keame.view;

import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Lorena Soledad on 14/10/2015.
 */
public class MapFragment extends SupportMapFragment implements OnMapReadyCallback,
GoogleMap.OnMapLongClickListener{

    public static SupportMapFragment newInstance(){
        SupportMapFragment fragment = new SupportMapFragment().newInstance();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }
}
