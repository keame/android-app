package com.kashmir.keame.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kashmir.keame.R;

public class AboutUsFragment extends Fragment {

    private TextView likeUsOnFacebook;

    public static AboutUsFragment newInstance() {
        AboutUsFragment fragment = new AboutUsFragment();
        return fragment;
    }

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_about_us, container, false);
        likeUsOnFacebook = (TextView) root.findViewById(R.id.about_us_like_fb);
        likeUsOnFacebook.setMovementMethod(LinkMovementMethod.getInstance());

        return root;
    }




}
