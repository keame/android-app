package com.kashmir.keame.view;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.activity.RegisterActivity;
import com.kashmir.keame.activity.ResetPasswordActivity;


/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends Fragment {

    private EditText mUsernameField = null;
    private EditText mPasswordField = null;
    private EditText mRepeatPasswordField = null;
    private Button mLogInButton = null;
    private TextView mSignUpTextView = null;
    private TextView mForgotPasswordTextView = null;
    private View mUsernamePasswordContainer = null;

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);
        Typeface sourceSansPro = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/SourceSansPro.otf");

        getViewElements(view); //Obtengo los elementos instanciados
        setPersonalizedFont(sourceSansPro); //Aplico la fuente
        setListeners(); //Configuracion de Listeners

        return view;
    }



    /* FUNCIONES DE CONFIGURACION DE INTERFAZ --------------------------------------------------- */

    private void setPersonalizedFont(Typeface typeface){
        mUsernameField.setTypeface(typeface);
        mPasswordField.setTypeface(typeface);
        mRepeatPasswordField.setTypeface(typeface);
        mLogInButton.setTypeface(typeface);
        mSignUpTextView.setTypeface(typeface);
        mForgotPasswordTextView.setTypeface(typeface);
    }

    private void getViewElements(View view){
        mUsernameField = (EditText) view.findViewById(R.id.usernameField);
        mPasswordField = (EditText) view.findViewById(R.id.passwordField);
        mRepeatPasswordField = (EditText) view.findViewById(R.id.repeatPasswordField);
        mLogInButton = (Button) view.findViewById(R.id.loginButton);
        mSignUpTextView = (TextView) view.findViewById(R.id.textView_sign_up);
        mForgotPasswordTextView = (TextView) view.findViewById(R.id.textView_forgot_password);
        mUsernamePasswordContainer = view.findViewById(R.id.login_wrapper);
    }

    private void setListeners(){
        //Registro de un nuevo usuario
        mSignUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isRepeatPasswordVisible()) {
                    //showRepeatPasswordAnimated();
                    Intent register = new Intent(getActivity(), RegisterActivity.class);
                    startActivity(register);
                }
            }
        });

        //Recuperacion de la password
        mForgotPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchResetPassword = new Intent(getActivity(), ResetPasswordActivity.class);
                startActivity(launchResetPassword);
            }
        });
    }




    /* FUNCIONES PARA EL MANEJO DE LA ANIMACION ------------------------------------------------- */

    private boolean isRepeatPasswordVisible() {
        return mRepeatPasswordField.getVisibility() == View.VISIBLE;
    }

    private int getRepeatPasswordAnimationDuration() {
        return getResources().getInteger(android.R.integer.config_mediumAnimTime);
    }

    private void showRepeatPasswordAnimated() {
        RevealInPlaceAnimator animation = new RevealInPlaceAnimator(mUsernamePasswordContainer, mRepeatPasswordField);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.setDuration(getRepeatPasswordAnimationDuration());
        animation.start();
    }

}
