package com.kashmir.keame.view;

import android.app.Application;

import com.kashmir.keame.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Lorena Soledad on 19/07/2015.
 */
public class KeaMe extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/SourceSansPro.otf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
