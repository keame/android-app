package com.kashmir.keame.controller;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kashmir.keame.R;
import com.kashmir.keame.activity.MainMapActivity;
import com.kashmir.keame.adapter.GroupsAdapter;
import com.kashmir.keame.dialog.AddGroupDialog;
import com.kashmir.keame.dialog.ModifyGroupDestinationsDialog;
import com.kashmir.keame.dialog.ModifyGroupMembersDialog;
import com.kashmir.keame.dialog.ModifyGroupPeriodsDialog;
import com.kashmir.keame.dialog.ModifyGroupPermissionsDialog;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Group;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupsFragment extends Fragment implements View.OnClickListener,
        Data.OnQueryListener, AddGroupDialog.OnDialogButtonSelected {

    private static final String TAG = GroupsFragment.class.getSimpleName();

    //Para las animaciones
    private View fadeIn;
    private View fadeOut;

    //Carga de la lista principal
    private ListView groupsList;
    private GroupsAdapter mAdapter;
    private ArrayList<Group> mGroups;
    private ProgressBar mProgressBar;
    private View mMessage;

    //Para agregar, editar o eliminar un grupo
    private static int sTempItemIndex;
    private static String sTempItemName;

    public GroupsFragment() {
        // Required empty public constructor
    }

    public static GroupsFragment newInstance(){
        GroupsFragment fragment = new GroupsFragment();
        return  fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mGroups = new ArrayList<>();
        mAdapter = new GroupsAdapter(mGroups, getContext(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.groups_list_fragment, container, false);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mMessage = view.findViewById(R.id.empty_list);
        groupsList = (ListView) view.findViewById(R.id.list_groups);

        groupsList.setAdapter(mAdapter);

        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainMapActivity) getActivity()).getmToolbar().setTitle(
                getResources().getString(R.string.drawer_menu_groups));
        ((MainMapActivity) getActivity()).getNavigationView()
                .getMenu().getItem(2).setChecked(true);
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        requestAllGroups();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("key", "value");
        Log.i(TAG, "onSaveInstanceState()");
    }



    public void parseGroupsData(String response){

        mGroups = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(response);
            JSONArray groups = object.getJSONArray(Constants.Groups.JSON_ARRAY_GROUPS);

            for(int i = 0; i < groups.length(); i++){
                mGroups.add(new Group(groups.getJSONObject(i)));
            }


            updateUI();
            /*if(mGroups.size() == 0){
                //TODO: CAMBIAR LAYOUT
                Toast.makeText(getActivity(), "No hay contactos en la lista", Toast.LENGTH_SHORT)
                    .show();
            }

            mAdapter.updateList(mGroups);*/

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(final View v) {
        // We need to post a Runnable to show the popup to make sure that the PopupMenu is
        // correctly positioned. The reason being that the view may change position before the
        // PopupMenu is shown.
        v.post(new Runnable() {
            @Override
            public void run() {
                showPopupMenu(v);
            }
        });
    }

    private void showPopupMenu(View view) {

        // Retrieve the clicked item from view's tag
        //final int position = (int) view.getTag();

        sTempItemIndex = (int) view.getTag();

        // Create a PopupMenu, giving it the clicked view for an anchor
        PopupMenu popup = new PopupMenu(getActivity(), view);

        // Inflate our menu resource into the PopupMenu's Menu
        popup.getMenuInflater().inflate(R.menu.popup_group_item, popup.getMenu());

        // Set a listener so we are notified if a menu item is clicked
        popup.setOnMenuItemClickListener(new android.support.v7.widget.PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.popup_groups_delete:
                        deleteItemAt(sTempItemIndex);
                        return true;
                    case R.id.popup_groups_edit:
                        editItemAt(sTempItemIndex);
                        return true;
                    case R.id.popup_groups_contacts:
                        showContactsDialog();
                        return true;
                    case R.id.popup_groups_permissions:
                        showPermissionsDialog();
                        break;
                    case R.id.popup_groups_destinations:
                        showDestinationsDialog();
                        break;
                    //case R.id.popup_groups_periods:
                      //  showPeriodsDialog();
                        //break;
                }
                return false;
            }
        });

        // Finally show the PopupMenu
        popup.show();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_groups, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_add:
                AddGroupDialog dialog = AddGroupDialog
                        .newInstance(
                                getActivity().getResources().getString(R.string.dialog_add_title),
                                getActivity().getResources().getString(R.string.dialog_add_message),
                                Constants.ApiRequestCode.ADD_NEW_GROUP);
                dialog.setTargetFragment(this, Constants.ApiRequestCode.ADD_NEW_GROUP);
                dialog.show(getActivity().getSupportFragmentManager(),
                        String.valueOf(Constants.ApiRequestCode.ADD_NEW_GROUP));

                return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /* ACCIONES DEL MENÚ DE CADA ITEM ---------------------------------------------------------- */
    public void deleteItemAt(final int position) {
        Log.d(TAG, "deleteItemAt: #" + String.valueOf(position));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_delete_message)
                .setTitle(R.string.dialog_delete_title)
                .setPositiveButton(R.string.dialog_delete_positive_button,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                delete(position);
                        }
                })
                .setNegativeButton(R.string.dialog_delete_negative_button, null);

        builder.create().show();
    }

    public void editItemAt(int position) {
        Log.d(TAG, "editItemAt: #" + String.valueOf(position));

        String title = getActivity().getResources().getString(R.string.dialog_edit_title);
        String message = getActivity().getResources().getString(R.string.dialog_edit_message);

        AddGroupDialog dialog = AddGroupDialog
                .newInstance(
                        title,
                        message,
                        Constants.ApiRequestCode.EDIT_GROUP);
        dialog.setTargetFragment(this, Constants.ApiRequestCode.EDIT_GROUP);
        dialog.show(getActivity().getSupportFragmentManager(),
                String.valueOf(Constants.ApiRequestCode.EDIT_GROUP));

    }

    public void showContactsDialog(){
        ModifyGroupMembersDialog dialog = ModifyGroupMembersDialog
                .newInstance(mGroups.get(sTempItemIndex).getId());

        dialog.show(getActivity().getSupportFragmentManager(),
                Constants.Groups.DIALOG_MODIFY_GROUP_MEMBER);
    }

    public void showPermissionsDialog(){
        ModifyGroupPermissionsDialog dialog = ModifyGroupPermissionsDialog
                .newInstance(mGroups.get(sTempItemIndex).getId());

        dialog.show(getActivity().getSupportFragmentManager(),
                Constants.Groups.DIALOG_MODIFY_GROUP_PERMISSION);
    }

    private void showDestinationsDialog(){
        ModifyGroupDestinationsDialog dialog = ModifyGroupDestinationsDialog
                .newInstance(mGroups.get(sTempItemIndex).getId());

        dialog.show(getActivity().getSupportFragmentManager(),
                Constants.Groups.DIALOG_MODIFY_GROUP_DESTINATION);
    }

    private void showPeriodsDialog(){
        ModifyGroupPeriodsDialog dialog = ModifyGroupPeriodsDialog
                .newInstance(mGroups.get(sTempItemIndex).getId());

        dialog.show(getActivity().getSupportFragmentManager(),
                Constants.Groups.DIALOG_MODIFY_GROUP_PERIODS);
    }




    /* RESUELVEN LAS RESPUESTAS DEL SERVIDOR ---------------------------------------------------- */
    @Override
    public void onResponse(int requestCode, String result) {

        Log.d(TAG, result);

        switch (requestCode){
            case Constants.ApiRequestCode.GET_ALL_GROUPS:
                if(getResponseStatus(result) == 200){
                    parseGroupsData(result);
                }
                break;
            case Constants.ApiRequestCode.ADD_NEW_GROUP:
                if(getResponseStatus(result) == 200){
                    showMessage(getActivity()
                            .getResources().getString(R.string.info_group_added_ok));

                    //TODO: Optimizar este método
                    updateList();
                }
                break;
            case Constants.ApiRequestCode.EDIT_GROUP:
                if( getResponseStatus(result) == 200 ){
                    showMessage(getActivity()
                            .getResources().getString(R.string.info_group_edited_ok));

                    mGroups.get(sTempItemIndex).setName(sTempItemName);
                    mAdapter.updateList(mGroups);
                }
                break;
            case Constants.ApiRequestCode.DELETE_GROUP:
                if(getResponseStatus(result) == 200 ){
                    showMessage(getActivity()
                            .getResources().getString(R.string.info_group_deleted_ok));

                    mGroups.remove(sTempItemIndex);
                    //mAdapter.updateList(mGroups);
                    updateUI();
                }
                break;
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Toast.makeText(getActivity(),
                "Esto es vergonzoso! Error " + statusCode, //Plagio, ni ahi
                Toast.LENGTH_LONG).show();

        updateUI();
    }





    /* INTERFACE DE COMUNICACIÓN CON EL DIÁLOGO ------------------------------------------------- */
    @Override
    public void onPositiveButtonSelected(String groupName, int requestCode) {
        Log.d(TAG, "El nuevo grupo se llama: " + groupName + " y el request code es: " + requestCode);

        sTempItemName = groupName;

        switch (requestCode){
            case Constants.ApiRequestCode.ADD_NEW_GROUP:
                addNewItem();
                break;
            case Constants.ApiRequestCode.EDIT_GROUP:
                changeItemNameAt();
        }

    }

    @Override
    public void onNegativeButtonSelected() {

    }





    /* FUNCIONES DE QUERY HTTP ------------------------------------------------------------------ */

    /**
     * Crea una query para eliminar un grupo
     *
     * @param position La posición de la lista marcada
     */
    private void delete(int position){
        Data.query(getActivity(),
                this,
                Constants.ApiRequestCode.DELETE_GROUP,
                String.valueOf(mGroups.get(position).getId()));
    }

    /**
     * Crea una query para cambiar el nombre del grupo seleccionado
     */
    private void changeItemNameAt(){
        Data.query(getActivity(),
                this,
                Constants.ApiRequestCode.EDIT_GROUP,
                String.valueOf(mGroups.get(sTempItemIndex).getId()),
                sTempItemName);
    }

    /**
     * Crea una query para crear un nuevo grupo
     */
    private void addNewItem(){
        Data.query(getActivity(),
                this,
                Constants.ApiRequestCode.ADD_NEW_GROUP,
                sTempItemName
        );
    }


    private void requestAllGroups(){
        Data.query(getActivity(),
                this,
                Constants.ApiRequestCode.GET_ALL_GROUPS);
    }



    /* FUNCIONES VARIAS ------------------------------------------------------------------------- */

    /**
     * Parsea la respuesta del servidor para obtener el status
     *
     * @param result Respuesta del servidor
     * @return Código de respuesta del servidor
     */
    private int getResponseStatus(String result){
        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Muestra un mensaje al usuario si la respuesta del servidor fue exitosa
     *
     * @param message El mensaje a mostrar
     */
    private void showMessage(String message){
        Toast.makeText(getActivity(),
                message,
                Toast.LENGTH_SHORT).show();
    }

    private void updateList(){
        Data.query(getActivity(),
                this,
                Constants.ApiRequestCode.GET_ALL_GROUPS);
    }


    /*private void updateUI() {
        if( mGroups.size() == 0){
            mMessage.setVisibility(View.VISIBLE);
        } else {
            mAdapter.updateList(mGroups);
            mMessage.setVisibility(View.GONE);
        }

        mProgressBar.setVisibility(View.GONE);
    }*/

    private void updateUI() {

        if( mGroups.size() == 0 && !mMessage.isShown() && mProgressBar.isShown()){
            fadeIn = mMessage;
            fadeOut = mProgressBar;
        } else if( mGroups.size() == 0 && !mMessage.isShown()) {
            fadeIn = mMessage;
            fadeOut = groupsList;
        } else if(mGroups.size() > 0 && mMessage.isShown()){
            mAdapter.updateList(mGroups);
            fadeIn = groupsList;
            fadeOut = mMessage;
        } else if(mGroups.size() > 0 && mProgressBar.isShown()){
            mAdapter.updateList(mGroups);
            fadeIn = groupsList;
            fadeOut = mProgressBar;
        } else {
            mAdapter.updateList(mGroups);
            return;
        }

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        fadeIn.setAlpha(0f);
        fadeIn.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        fadeIn.animate()
                .alpha(1f)
                .setDuration(300)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        fadeOut.animate()
                .alpha(0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        fadeOut.setVisibility(View.GONE);
                    }
                });
    }
}
