package com.kashmir.keame.controller;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.kashmir.keame.activity.MainMapActivity;
import com.kashmir.keame.interfaces.UserLocationListener;

/**
 * Created by Lorena Soledad on 26/08/2015.
 */
public class GoogleMapsApiConnection implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    public static final String TAG = GoogleMapsApiConnection.class.getSimpleName();

    protected GoogleApiClient mGoogleApiClient;
    private Context mContext;
    private Location mCurrentLocation;
    private UserLocationListener mLocationListener;


    public GoogleMapsApiConnection(Context context){
        mContext = context;
        mLocationListener = (UserLocationListener) context;
    }

    /**/
    @Override
    public void onConnected(Bundle bundle) {

        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.

        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if(mCurrentLocation == null){
            //Fuerzo la obtención de una localización
        } else {
            mLocationListener.onLocationReceived(mCurrentLocation);
        }

        Log.d(TAG, mCurrentLocation == null? "No hay lozalización" : "Se encontró una localización");

        mGoogleApiClient.disconnect();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /* Si falla la conexi�n con la API de Google */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //TODO: VALIDAR QUE EL TELEFONO TENGA LOS GOOGLE PLAY SERVICES Y ESTEN ACTULIZADOS
    }

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void connect(){
        mGoogleApiClient.connect();
    }

}
