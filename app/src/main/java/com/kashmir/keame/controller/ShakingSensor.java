package com.kashmir.keame.controller;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Contact;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lorena Soledad on 26/08/2015.
 */
public class ShakingSensor implements SensorEventListener, Data.OnQueryListener {

    private static final String TAG = ShakingSensor.class.getSimpleName();

    private SensorManager mSensorManager;
    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity
    private Context mContext;

    private ArrayList<Contact> mEmergencyContacts;

    public ShakingSensor(Context context, float mAccel, float mAccelCurrent, float mAccelLast){
        mContext = context;
        this.mAccel = mAccel;
        this.mAccelCurrent = mAccelCurrent;
        this.mAccelLast = mAccelLast;

        getEmergencyContacts();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        mAccelLast = mAccelCurrent;
        mAccelCurrent = (float) Math.sqrt((double) (x*x + y*y + z*z));
        float delta = mAccelCurrent - mAccelLast;
        mAccel = mAccel * 0.9f + delta; // perform low-cut filter

        if (mAccel > 12) {
            Toast toast = Toast.makeText(mContext.getApplicationContext(), "Se lanzó una alerta manual.", Toast.LENGTH_LONG);
            toast.show();

            //TODO: ENVIAR POR SMS
            SmsManager sms = SmsManager.getDefault();
            String message = mContext.getSharedPreferences
                    ("com.kashmir.keame_preferences", mContext.MODE_PRIVATE)
                    .getString("emergency_message", "Ayuda!");

            for(int i = 0; i < mEmergencyContacts.size(); i++){
                String phoneNumber = mEmergencyContacts.get(i).getTelephoneNumber();
                //sms.sendTextMessage("+" + phoneNumber, null, message, null, null);
                Log.d(TAG, "Enviando un mensaje a: " + "+" + phoneNumber);
            }

            //TODO: ENVIAR AL SERVIDOR PARA NOTIFICACIONES

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Nothing to do here :)
    }


    private void getEmergencyContacts(){

        int currentUserId = mContext.getSharedPreferences(Constants.Preferences.PREFERENCES_FILE,
                mContext.MODE_PRIVATE).getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        Log.d(TAG, "User ID: " +currentUserId );

        String uri = String.format(Constants.Uri.GET_ALL_EMERGENCY_CONTACTS, currentUserId);

        Data.newRequest(
                mContext,
                this,
                uri,
                Request.Method.GET,
                null,
                Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS,
                null);

    }

    @Override
    public void onResponse(int requestCode, String result) {
        try {
            JSONObject response = new JSONObject(result);
            int status = response.getInt("status");

            if(status == 200){
               switch (requestCode){
                   case Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS:
                       JSONArray contact = response.getJSONArray("contacts");

                       mEmergencyContacts = new ArrayList<>();

                       for(int i = 0; i < contact.length(); i++){
                           mEmergencyContacts.add(new Contact(contact.getJSONObject(i),
                                   Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS));
                       }
                       break;
               }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {

    }


}
