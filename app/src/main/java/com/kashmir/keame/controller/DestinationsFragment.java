package com.kashmir.keame.controller;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kashmir.keame.R;
import com.kashmir.keame.activity.MainMapActivity;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Destination;

import java.util.ArrayList;

import com.kashmir.keame.adapter.DestinationsAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class DestinationsFragment extends Fragment
    implements Data.OnQueryListener{

    private static final String TAG = Destination.class.getSimpleName();

    private ListView destinationsList;
    private ProgressBar mProgressBar;
    private View mMessage;

    private DestinationsAdapter mAdapter;
    private ArrayList<Destination> mDestinations;
    private OnDestinationSelected mCallback;

    //Para las animaciones
    private View fadeIn;
    private View fadeOut;


    public static DestinationsFragment newInstance() {
        DestinationsFragment fragment = new DestinationsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public DestinationsFragment() {
        // Required empty public constructor
    }




    /* CICLO DE VIDA DE LA ACTIVITY ------------------------------------------------------------- */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDestinations = new ArrayList<Destination>();
        mAdapter = new DestinationsAdapter(mDestinations, getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.destinations_list_fragment, container, false);

        destinationsList = (ListView) view.findViewById(R.id.list_destinations);
        mMessage = view.findViewById(R.id.empty_list);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        destinationsList.setAdapter(mAdapter);
        destinationsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCallback.onDestinationSelected(position, mDestinations.get(position));
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainMapActivity) getActivity()).getmToolbar().setTitle(
                getResources().getString(R.string.drawer_menu_destinations));
        ((MainMapActivity) getActivity()).getNavigationView()
                .getMenu().getItem(3).setChecked(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        requestDestinations();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Para asegurarnos que la activity padre implementa la interfaz
        // Si no tiene se lanza una excepcion
        try {
            mCallback = (OnDestinationSelected) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " debe implementar OnDestinationSelected");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }





    /* PARA PROCESAR LA RESPUESTA DEL SERVIDRO -------------------------------------------------- */

    @Override
    public void onResponse(int requestCode, String result) {

        Log.d(TAG, result);

        if( getResponseStatus(result) != 200 ){
            updateUI();
            return;
        }

        switch (requestCode){
            case Constants.ApiRequestCode.GET_ALL_DESTINATIONS:
                populateDestinationsList(result);
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Toast.makeText(getContext(),
                "Oops, hubo un error",
                Toast.LENGTH_SHORT).show();

        updateUI();
    }





    /* PARA COMUNICARSE CON LA ACTIVITY PADRE --------------------------------------------------- */

    public interface OnDestinationSelected {
        public void onDestinationSelected(int position, Destination destination);
    }






    /* OTRAS FUNCIONES -------------------------------------------------------------------------- */

    private int getResponseStatus(String result){
        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private void populateDestinationsList(String result){

        Log.d(TAG, result);

        mDestinations = new ArrayList<>();

        try {
            JSONObject response = new JSONObject(result);
            JSONArray destinations =
                    response.getJSONArray(Constants.Destinations.JSON_ARRAY_DESTINATIONS);

            for (int i = 0; i < destinations.length(); i++){
                mDestinations.add(new Destination(destinations.getJSONObject(i)));
            }

            updateUI();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*private void updateUI() {
        if( mDestinations.size() == 0){
            mMessage.setVisibility(View.VISIBLE);
        } else {
            mAdapter.updateList(mDestinations);
            mMessage.setVisibility(View.GONE);
        }

        mProgressBar.setVisibility(View.GONE);
    }*/

    private void updateUI() {

        if( mDestinations.size() == 0 && !mMessage.isShown() && mProgressBar.isShown()){
            mAdapter.updateList(mDestinations);
            fadeIn = mMessage;
            fadeOut = mProgressBar;
        } else if( mDestinations.size() == 0 && !mMessage.isShown()) {
            fadeIn = mMessage;
            fadeOut = destinationsList;
        } else if(mDestinations.size() > 0 && mMessage.isShown()){
            mAdapter.updateList(mDestinations);
            fadeIn = destinationsList;
            fadeOut = mMessage;
        } else if(mDestinations.size() > 0 && mProgressBar.isShown()){
            mAdapter.updateList(mDestinations);
            fadeIn = destinationsList;
            fadeOut = mProgressBar;
        } else {
            mAdapter.updateList(mDestinations);
            return;
        }

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        fadeIn.setAlpha(0f);
        fadeIn.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        fadeIn.animate()
                .alpha(1f)
                .setDuration(300)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        fadeOut.animate()
                .alpha(0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        fadeOut.setVisibility(View.GONE);
                    }
                });
    }






    /* REQUEST AL SERVIDOR ---------------------------------------------------------------------- */

    private void requestDestinations(){
        Data.query(
                getActivity(),
                this,
                Constants.ApiRequestCode.GET_ALL_DESTINATIONS
        );
    }

}
