package com.kashmir.keame.controller;


import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kashmir.keame.R;
import com.kashmir.keame.activity.MainMapActivity;
import com.kashmir.keame.view.ContactsFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabContactsFragment extends Fragment {

    private static final String TAG = TabContactsFragment.class.getSimpleName();

    private MainMapActivity mCallingActivity;
    private ViewPager mPager;
    private TabLayout mTabLayout;


    public TabContactsFragment() {
        // Required empty public constructor
    }

    public static TabContactsFragment newInstance() {

        Bundle args = new Bundle();

        TabContactsFragment fragment = new TabContactsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainMapActivity) getActivity()).getSupportActionBar().setTitle(
                getResources().getString(R.string.drawer_menu_contacts));
        ((MainMapActivity) getActivity()).getNavigationView()
                .getMenu().getItem(1).setChecked(true);

        Log.d(TAG, "onStart()");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_contacts, container, false);

        getViews(view);
        setUpViewPager();
        setUpTabs();

        return view;
    }

    private void getViews(View view){
        mPager = (ViewPager) view.findViewById(R.id.pager);
        mTabLayout = mCallingActivity.getTabLayout();
    }

    private void setUpViewPager(){
        PagerAdapter adapter = new PagerAdapter(mCallingActivity.getSupportFragmentManager());
        adapter.addFragment(ContactsFragment.newInstance(), "MIS CONTACTOS");
        adapter.addFragment(PendingContactsFragment.newInstance(), "PENDIENTES");
        mPager.setAdapter(adapter);
    }

    private void setUpTabs(){
        mTabLayout.addTab(mTabLayout.newTab());
        mTabLayout.addTab(mTabLayout.newTab());
        mTabLayout.setupWithViewPager(mPager);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallingActivity = ((MainMapActivity) context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallingActivity = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contacts, menu);

        SearchManager searchManager =
                (SearchManager) getActivity()
                        .getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setQueryHint("Nombre, email, teléfono...");
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {

        private final ArrayList<Fragment> pages = new ArrayList<>();
        private final ArrayList<String> title = new ArrayList<>();

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment newFragment, String newTitle){
            pages.add(newFragment);
            title.add(newTitle);
        }

        @Override
        public Fragment getItem(int position) {
            return pages.get(position);
        }

        @Override
        public int getCount() {
            return pages.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title.get(position);
        }
    }
}
