package com.kashmir.keame.controller;

        import android.app.IntentService;
        import android.content.Intent;
        import android.location.Location;
        import android.os.Bundle;
        import android.util.Log;

        import com.google.android.gms.common.ConnectionResult;
        import com.google.android.gms.common.api.GoogleApiClient;
        import com.google.android.gms.location.LocationListener;
        import com.google.android.gms.location.LocationRequest;
        import com.google.android.gms.location.LocationServices;
        import com.google.android.gms.maps.model.LatLng;
        import com.google.maps.android.PolyUtil;
        import com.kashmir.keame.model.Destination;
        import com.kashmir.keame.model.Route;

        import java.text.DateFormat;
        import java.util.ArrayList;
        import java.util.Date;

/**
 * Created by Lorena Soledad on 27/08/2015.
 */
public class TrakerService extends IntentService implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {

    private static final String TAG = TrakerService.class.getSimpleName();
    private static final int MIN_DISTANCE_CHANGE_FOR_UPDATE = 500; //metros
    private static final int MIN_TIME_BETWEEN_UPDATES = 10000; //milisegundos
    private static final int FASTEST_TIME_BETWEEN_UPDATES = 5000; //milisegundos
    private static final int MAX_DISTANCE_FROM_ROUTE = 500; //meters
    private static final String DESTINATIONS_LIST = "DESTINATIONS_LIST";
    private static final String ROUTES_LIST = "ROUTES_LIST";

    private Location mCurrentLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String mLastUpdateTime;

    private ArrayList<Destination> mDestinations;
    private ArrayList<Route> mRoutes;

    public TrakerService(){
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        buildGoogleApiClient(); //Creamos el Google API Client
        createLocationRequest();

        if(mGoogleApiClient != null)
            mGoogleApiClient.connect();

        Log.d(TAG, "Se ha creado el servicio");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.d(TAG, "onHandleIntent()");

        mDestinations = intent.getParcelableArrayListExtra(DESTINATIONS_LIST);
        mRoutes = intent.getParcelableArrayListExtra(ROUTES_LIST);

        Log.d(TAG, "Recibi " + mDestinations.get(0).getName() + "en la ubicacion (" +
                mDestinations.get(0).getPosition().latitude + " , " +
                mDestinations.get(0).getPosition().longitude + " )");

        while(true){
            // Gets data from the incoming Intent
            //String dataString = intent.getDataString();

            // Do work here, based on the contents of dataString
        }


    }

    @Override
    public void onConnected(Bundle bundle) {
            startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //TODO
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        for(Destination d : mDestinations){
            isInsideCircle(d.getPosition(), mCurrentLocation);
        }

        for(Route r: mRoutes){
            isOnRoute(r,location);
        }

        //Actualizar en la API la nueva localizacion del usuario

        updateUI();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //TODO
    }


    /* UTILITY FUNCTIONS ------------------------------------------------------------------------ */

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(MIN_TIME_BETWEEN_UPDATES);
        //mLocationRequest.setSmallestDisplacement(MIN_DISTANCE_CHANGE_FOR_UPDATE);
        mLocationRequest.setFastestInterval(FASTEST_TIME_BETWEEN_UPDATES);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void updateUI() {
        Log.d(TAG, "Nueva posicion en (" + mCurrentLocation.getLatitude() + " , " + mCurrentLocation.getLongitude() + " ) a las " + mLastUpdateTime);
    }



    /* PARA DETECTAR SI EL USUARIO ESTA EN UN DESTINO ------------------------------------------- */
    private boolean isInsideCircle(LatLng center, Location point){
        float[] distance = new float[2];

        Location.distanceBetween(point.getLatitude(),
                point.getLongitude(), center.latitude,
                center.longitude, distance);

        if( distance[0] > MAX_DISTANCE_FROM_ROUTE  ){
            Log.d(TAG, "Outside");
            return false;
        } else {
            Log.d(TAG, "Inside");
            return true;
        }
    }

    /* PARA DETECTAR SI EL USUARIO ESTA EN UNA RUTA --------------------------------------------- */
    private boolean isOnRoute(Route currentRoute, Location currentPosition) {
        ArrayList<LatLng> route = currentRoute.getGeopoints();

        LatLng point = new LatLng(currentPosition.getLatitude(), currentPosition.getLongitude());
        if (PolyUtil.isLocationOnPath(point, route, false, MAX_DISTANCE_FROM_ROUTE)) {
            Log.d(TAG, "Esta en la ruta");
            return true;
        } else {
            Log.d(TAG, "No esta en la ruta");
            return false;
        }
    }
}


