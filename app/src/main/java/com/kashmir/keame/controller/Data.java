package com.kashmir.keame.controller;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kashmir.keame.model.Constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lorena Soledad on 28/09/2015.
 */
public class Data {

    private static final String TAG = Data.class.getSimpleName();

    private static Context mContext;
    private static OnQueryListener mListener;


    public interface OnQueryListener {
        void onResponse(int requestCode, String result);
        void onErrorResponse(int statusCode, int requestCode);
    }

    public static void query(Context context, OnQueryListener listener, int requestCode, String... param){

        mListener = listener;
        mContext = context;

        switch (requestCode){

            case Constants.ApiRequestCode.LOGIN:
                performLogin(context, listener, param[0], param[1]);
                break;

            case Constants.ApiRequestCode.GET_ALL_GROUPS:
                getAllGroups();
                break;
            case Constants.ApiRequestCode.ADD_NEW_GROUP:
                addNewGroup(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.EDIT_GROUP:
                editGroupName(context, listener, param[0], param[1]);
                break;
            case Constants.ApiRequestCode.DELETE_GROUP:
                deleteGroup(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.UPDATE_GROUP_CONTACTS:
                updateGroupMembers(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.UPDATE_GROUP_DESTINATION:
                updateGroupDestinations(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.GET_GROUP_PERMISSIONS:
                getGroupPermissions(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.UPDATE_GROUP_PERMISSIONS:
                updateGroupPermissions(context, listener, param[0]);
                break;


            case Constants.ApiRequestCode.GET_ALL_CONTACTS:
                getAllContacts(context, listener);
                break;
            case Constants.ApiRequestCode.GET_ALL_PENDING_CONTACS:
                getAllPendingContacts(context, listener);
                break;
            case Constants.ApiRequestCode.SEARCH_CONTACTS: break;
            case Constants.ApiRequestCode.SEND_FRIEND_REQUEST:
                sendFriendRequest(param[0]);
                break;
            case Constants.ApiRequestCode.ACCEPT_FRIEND:
                addFriend(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.REJECT_FRIEND:
                rejectFriend(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.DELETE_FRIEND:
                deleteFriend(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.GET_CONTACTS_IN_GROUP:
                getContactsInGroup(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.SEND_POSITION_REQUEST:
                sendPositionRequest(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.SEND_STATE_REQUEST:
                sendStateRequest(context, listener, param[0]);
                break;


            case Constants.ApiRequestCode.GET_ALL_DESTINATIONS:
                getAllDestinations(context, listener);
                break;
            case Constants.ApiRequestCode.ADD_DESTINATION:
                addDestination(context, listener, param[0], param[1], param[2]);
                break;
            case Constants.ApiRequestCode.DELETE_DESTINATION:
                deleteDestination(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.GET_DESTINATIONS_IN_GROUP:
                getDestinationsInGroup(context, listener, param[0]);
                break;


            case Constants.ApiRequestCode.ADD_NEW_ROUTE:
                addRoute(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.GET_ALL_ROUTES:
                getAllRoutes(context, listener);
                break;
            case Constants.ApiRequestCode.DELETE_ROUTE:
                deleteRoute(context, listener, param[0]);
                break;

            case Constants.ApiRequestCode.SEND_LOCATION_UPDATE:
                sendLocationUpdate(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.SEND_GEOFENCE_UPDATE:
                sendGeofenceUpdate(context, listener, param[0]);
                break;
            case Constants.ApiRequestCode.REQUEST_CONTACTS_POSITION:
                requestContactsPosition(context, listener);
                break;
        }
    }




    private static void performLogin
            (Context context, final OnQueryListener listener, final String username, final String password){

        final String url = "https://keame.herokuapp.com/api/v1/auth/?email="+username+"&pass="+password;

        Log.d(TAG, url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onResponse(
                                Constants.ApiRequestCode.LOGIN,
                                Data.transformToJsonString(response)
                        );
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        listener.onErrorResponse(status, 0);
                    }
                }){

            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("email", username);
                params.put("pass", password);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<String, String>();
                header.put("Content-Type", "application/json; charset=utf-8");
                return header;
            }*/
        };

        //Agreamos la Query a la cola
        RestApiConnection.getInstance(mContext)
                .addToRequestQueue(stringRequest);
    }








    private static void getAllGroups(){
        final int defaultUserID = 1;
        final String url = "https://keame.herokuapp.com/api/v1/users/" + getCurrentUserId(mContext) + "/groups";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mListener.onResponse(
                                Constants.ApiRequestCode.GET_ALL_GROUPS,
                                Data.transformToJsonString(response)
                        );
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        mListener.onErrorResponse(status, 0);
                    }
                });

        //Agreamos la Query a la cola
        RestApiConnection.getInstance(mContext)
                .addToRequestQueue(stringRequest);
    }

    public static void addNewGroup(Context context, final OnQueryListener listener, String groupName){
        final int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/groups/add/?name=" + groupName;

        Log.d(TAG, url);

        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mListener.onResponse(
                                Constants.ApiRequestCode.ADD_NEW_GROUP,
                                Data.transformToJsonString(response));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        mListener.onErrorResponse(status, 0);
                    }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(request);
    }

    public static void editGroupName(Context context, final OnQueryListener listener, String groupId, String newName){

        final int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/groups/update_name/?group_id=" + groupId + "&name=" + newName;

        Log.d(TAG, url);

        StringRequest request = new StringRequest(
                Request.Method.PUT,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mListener.onResponse(
                                Constants.ApiRequestCode.EDIT_GROUP,
                                Data.transformToJsonString(response)
                        );
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        mListener.onErrorResponse(status, 0);
                    }
                }
        );

        RestApiConnection.getInstance(context).addToRequestQueue(request);
    }

    public static void deleteGroup(Context context, final OnQueryListener listener, String groupId){
        final int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/groups/remove/?group_id=" + groupId;

        Log.d(TAG, url);

        StringRequest request = new StringRequest(
                Request.Method.DELETE,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mListener.onResponse(
                                Constants.ApiRequestCode.DELETE_GROUP,
                                Data.transformToJsonString(response)
                        );
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        mListener.onErrorResponse(status, 0);
                    }
                }
        );

        RestApiConnection.getInstance(context).addToRequestQueue(request);
    }

    private static void updateGroupMembers
            (Context context, final OnQueryListener listener, final String json){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/groups/update_members/";

        Log.d(TAG, url);

        StringRequest request = new StringRequest
                (Request.Method.PUT, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onResponse(
                                Constants.ApiRequestCode.UPDATE_GROUP_CONTACTS,
                                transformToJsonString(response)
                        );
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        listener.onErrorResponse(status, 0);
                    }
                }){

            @Override
            public byte[] getBody() throws AuthFailureError {
                return json.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        RestApiConnection.getInstance(context).addToRequestQueue(request);

    }

    private static void updateGroupDestinations
            (Context context, final OnQueryListener listener, final String json){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) +
                "/arrived/update_destinations/";

        Log.d(TAG, url);

        StringRequest request = new StringRequest
                (Request.Method.PUT, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onResponse(
                                Constants.ApiRequestCode.UPDATE_GROUP_DESTINATION,
                                transformToJsonString(response)
                        );
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        listener.onErrorResponse(status, 0);
                    }
                }){

            @Override
            public byte[] getBody() throws AuthFailureError {
                return json.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            };

            RestApiConnection.getInstance(context).addToRequestQueue(request);

    }


    private static void getGroupPermissions
            (Context context, final OnQueryListener listener, final String groupId){

        final int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/groups/permissions/?group_id=" + groupId;

        Log.d(TAG, url);

        StringRequest request = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onResponse(
                                Constants.ApiRequestCode.GET_GROUP_PERMISSIONS,
                                Data.transformToJsonString(response)
                        );
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        listener.onErrorResponse(status, 0);
                    }
                }
        ){
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("group_id", groupId);
                return param;
            }*/
        };

        RestApiConnection.getInstance(context).addToRequestQueue(request);

    }

    private static void updateGroupPermissions
            (Context context, final OnQueryListener listener, final String json){

        final int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/groups/update_permissions/";

        Log.d(TAG, url);

        StringRequest request = new StringRequest(
                Request.Method.PUT,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onResponse(
                                Constants.ApiRequestCode.UPDATE_GROUP_PERMISSIONS,
                                Data.transformToJsonString(response)
                        );
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        listener.onErrorResponse(status, 0);
                    }
                }
        ){
            @Override
            public byte[] getBody() throws AuthFailureError {
                return json.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        RestApiConnection.getInstance(context).addToRequestQueue(request);

    }








    /* BUSQUEDA DE CONTACTOS -------------------------------------------------------------------- */

    public static void sendFriendRequest(String toUser){
        final int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/contacts/add/?contact_id=" + toUser;

        Log.d(TAG, url);

        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mListener.onResponse(
                                Constants.ApiRequestCode.SEND_FRIEND_REQUEST,
                                Data.transformToJsonString(response)
                        );
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int status = 0;
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            status = error.networkResponse.statusCode;
                        }
                        mListener.onErrorResponse(status, 0);
                    }
                }
        );

        RestApiConnection.getInstance(mContext).addToRequestQueue(request);
    }


    public static void getAllContacts(Context context, final OnQueryListener listener){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/contacts";

        StringRequest strRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.GET_ALL_CONTACTS,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }

    public static void getAllPendingContacts(Context context, final OnQueryListener listener){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext)
                +"/contacts/pending_contacts";

        StringRequest strRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.GET_ALL_PENDING_CONTACS,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }

    private static void addFriend(Context context, final OnQueryListener listener, String id){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) +
                "/contacts/accept/?contact_id=" + id;

        StringRequest strRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.ACCEPT_FRIEND,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }


    public static void rejectFriend(Context context, final OnQueryListener listener, String id){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) +
                "/contacts/reject/?contact_id=" + id;

        StringRequest strRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.REJECT_FRIEND,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }


    private static void deleteFriend(
            Context context, final OnQueryListener listener, String contactId){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) +
                "/contacts/remove/?contact_id=" + contactId;

        StringRequest strRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.DELETE_FRIEND,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);
    }

    private static void getContactsInGroup
            (Context context, final OnQueryListener listener, String groupId){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/groups/members/?group_id=" + groupId;

        StringRequest strRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.GET_CONTACTS_IN_GROUP,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);
    }

    private static void sendPositionRequest
            (Context context, final OnQueryListener listener, final String contactId){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/query/position/";

        StringRequest strRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.SEND_POSITION_REQUEST,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                Log.d(TAG, error.getMessage());

                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contact_id", contactId);
                return param;
            }
        };

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);
    }

    private static void sendStateRequest
            (Context context, final OnQueryListener listener, final String contactId){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/query/status/";

        StringRequest strRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.SEND_STATE_REQUEST,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                Log.d(TAG, error.getMessage());

                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contact_id", contactId);
                return param;
            }
        };

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }









    /* DESTINATIONS ----------------------------------------------------------------------------- */

    private static void getAllDestinations(Context context, final OnQueryListener listener){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) +
                "/destination/";

        StringRequest strRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.GET_ALL_DESTINATIONS,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);


    }


    private static void addDestination
            (Context context, final OnQueryListener listener,
             String name, String latitude, String longitude){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/destination/add/?name=" + name + "&longitude=" + longitude
                + "&latitude=" + latitude;

        Log.d(TAG + "addDestination", url);

        StringRequest strRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.ADD_DESTINATION,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }


    private static void deleteDestination(Context context, final OnQueryListener listener,
                                          String destinationId){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) +
                "/destination/remove/?destination_id=" + destinationId;

        Log.d(TAG, url);

        StringRequest strRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.DELETE_DESTINATION,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }

    private static void getDestinationsInGroup
            (Context context, final OnQueryListener listener, String groupId) {

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/arrived/destinations/?group_id=" + groupId;

        StringRequest strRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.GET_DESTINATIONS_IN_GROUP,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }




    /* ROUTES ----------------------------------------------------------------------------------- */

    private static void addRoute
            (Context context, final OnQueryListener listener, final String json){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/" +  getCurrentUserId(mContext) + "/routes/add/";

        Log.d(TAG, url);

        StringRequest jsonRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    listener.onResponse(
                            Constants.ApiRequestCode.ADD_NEW_ROUTE,
                            transformToJsonString(response)
                    );
                }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                return json.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        RestApiConnection.getInstance(context).addToRequestQueue(jsonRequest);

    }


    private static void getAllRoutes(Context context, final OnQueryListener listener){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/" + getCurrentUserId(mContext) + "/routes/";

        Log.d(TAG, url);

        StringRequest strRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.GET_ALL_ROUTES,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }


    private static void deleteRoute
            (Context context, final OnQueryListener listener, String routeId){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) +"/routes/remove/?route_id=" + routeId;

        Log.d(TAG, url);

        StringRequest strRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponse(
                        Constants.ApiRequestCode.DELETE_ROUTE,
                        transformToJsonString(response)
                );
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

    }





    /* LOCATION UPDATES ------------------------------------------------------------------------- */

    private static void sendLocationUpdate
            (Context context, OnQueryListener listener, final String json){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) +
                "/update_position";

        Log.d(TAG, url);

        StringRequest request = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse(): " + response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "onResponse(): " + error.getMessage());
                    }
                }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                return json.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        RestApiConnection.getInstance(context).addToRequestQueue(request);
    }


    private static void sendGeofenceUpdate
            (Context context, OnQueryListener listener, final String destinationId){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/arrived/";

        Log.d(TAG, url);

        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse(): " + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onResponse(): " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("destination_id", destinationId);
                return param;
            }
        };

        RestApiConnection.getInstance(context).addToRequestQueue(request);
    }

    private static void requestContactsPosition
            (Context context, final OnQueryListener listener){

        int defaultUser = 1;
        String url = "https://keame.herokuapp.com/api/v1/users/"
                + getCurrentUserId(mContext) + "/tracking";

        Log.d(TAG, url);

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onResponse(
                                Constants.ApiRequestCode.REQUEST_CONTACTS_POSITION,
                                transformToJsonString(response)
                        );
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onResponse(): " + error.getMessage());

                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, 0);
            }
        });

        RestApiConnection.getInstance(context).addToRequestQueue(request);

    }


    private static int getCurrentUserId(Context context){
        return context.getSharedPreferences
                (Constants.Preferences.PREFERENCES_FILE, context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);
    }



    public static String transformToJsonString(String response){
        response = response.replaceAll("\\\\", "");
        response = response.substring(1, response.length() - 1);

        return response;
    }

























    /* PARA DESNEGREAR EL CÓDIGO ---------------------------------------------------------------- */

    public static void newRequest
            (Context context, final OnQueryListener listener, String url, int method,
             final Map<String, String> params, final int requestType, final String body){

        StringRequest request = new StringRequest(method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listener.onResponse(
                                requestType,
                                requestType == Constants.ApiRequestCode.GET_GOOGLE_DIRECTIONS ?
                                        response : transformToJsonString(response)
                        );
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onResponse(): " + error.getMessage());

                int status = 0;
                if (error.networkResponse != null) {
                    Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                    status = error.networkResponse.statusCode;
                }
                listener.onErrorResponse(status, requestType);
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if(params != null){
                    return params;
                } else {
                    return super.getParams();
                }
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                if(body != null){
                    return body.getBytes();
                } else {
                    return super.getBody();
                }
            }

            @Override
            public String getBodyContentType() {
                if(body != null){
                    return "application/json";
                } else {
                    return super.getBodyContentType();
                }
            }
        };

        RestApiConnection.getInstance(context).addToRequestQueue(request);
    }


}
