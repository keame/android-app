package com.kashmir.keame.controller;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kashmir.keame.R;
import com.kashmir.keame.adapter.PendingContactsAdapter;
import com.kashmir.keame.interfaces.OnDialogButtonSelected;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Contact;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendingContactsFragment extends Fragment
        implements Data.OnQueryListener, OnDialogButtonSelected {

    private static final String TAG = PendingContactsAdapter.class.getSimpleName();

    private ArrayList<Contact> mPendingContacts;
    private RecyclerView mPendingList;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private SwipeRefreshLayout mSwipeToRefresh;
    private ProgressBar mProgressBar;
    private View mMessage;
    private int mPositionSelected;


    public PendingContactsFragment() {
        // Required empty public constructor
    }

    public static PendingContactsFragment newInstance() {

        Bundle args = new Bundle();

        PendingContactsFragment fragment = new PendingContactsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pending_contacts, container, false);

        mMessage = view.findViewById(R.id.empty_list);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        //Busco las views correspondientes al fragment
        mPendingList = (RecyclerView) view.findViewById(R.id.pending_list);
        mPendingList.setHasFixedSize(true);
        mPendingList.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getActivity()).build());

        //Seteo el adaptador con los datos
        mPendingContacts = new ArrayList<Contact>();
        mAdapter = new PendingContactsAdapter(mPendingContacts, this);
        mPendingList.setAdapter(mAdapter);

        //Instanciamos el LayoutManager del RecyclerView
        mLayoutManager = new LinearLayoutManager(getActivity());
        mPendingList.setLayoutManager(mLayoutManager);

        //Configuramos el swipe to Refresh
        mSwipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_to_refresh);
        mSwipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllPendingContacts();
            }
        });

        getAllPendingContacts();

        return view;
    }


    @Override
    public void onResponse(int requestCode, String result) {

        Log.d(TAG, result);

        if(getResponseStatus(result) == 200){
            switch (requestCode){
                case Constants.ApiRequestCode.GET_ALL_PENDING_CONTACS:
                    populatePendingList(result);
                    break;
                case Constants.ApiRequestCode.ACCEPT_FRIEND:
                    Toast.makeText(getActivity(), "Se aceptó un amigo", Toast.LENGTH_SHORT).show();
                    removeItemFromList(mPositionSelected);
                    break;
                case Constants.ApiRequestCode.REJECT_FRIEND:
                    Toast.makeText(getActivity(), "Se rechazó un amigo", Toast.LENGTH_SHORT).show();
                    removeItemFromList(mPositionSelected);
                    break;
            }
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Log.d(TAG, "Status code: " + statusCode);

        Toast.makeText(getContext(),
                "Oops, algo salió mal.",
                Toast.LENGTH_SHORT).show();

        updateUI();
    }


    private void populatePendingList(String result){

        Log.d(TAG, "Cargando contactos pendientes...");

        mPendingContacts.clear();

        try {
            JSONObject j = new JSONObject(result);
            JSONArray users = j.getJSONArray(Constants.Contacts.JSON_ARRAY_CONTACTS);

            for(int i = 0; i < users.length(); i++ ){
                mPendingContacts.add(new Contact(users.getJSONObject(i), 0));
            }

            updateUI();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void removeItemFromList(int position){
        mPendingContacts.remove(position);
        mAdapter.notifyItemRemoved(position);
        updateUI();
    }


    private int getResponseStatus(String result){
        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private void updateUI() {
        if( mPendingContacts.size() == 0){
            mMessage.setVisibility(View.VISIBLE);
        } else {
            mAdapter.notifyDataSetChanged();
            mMessage.setVisibility(View.GONE);
        }

        mProgressBar.setVisibility(View.GONE);
        mSwipeToRefresh.setRefreshing(false);
    }





    /* INTERFAZ DE COMUNICACIÓN CON EL DIALOGO -------------------------------------------------- */

    @Override
    public void onPositiveButtonClick(int position) {

        Log.d(TAG, "onPositiveButton()");
        mPositionSelected = position;

        addFriend(String.valueOf(
                mPendingContacts.get(position)
                        .getId()));
    }

    @Override
    public void onNegativeButtonClick(int position) {

        Log.d(TAG, "onNegativeButton()");
        mPositionSelected = position;

        rejectFriend(String.valueOf(
                mPendingContacts.get(position)
                        .getId()));
    }

    @Override
    public void onNeutralButtonClick() {
        Log.d(TAG, "onNeutralButton()");
    }





    /* QUERIES AL SERVIDOR ---------------------------------------------------------------------- */

    private void getAllPendingContacts(){
        Data.query(
                getContext(),
                this,
                Constants.ApiRequestCode.GET_ALL_PENDING_CONTACS
        );
    }


    private void addFriend(String contactId){
        Data.query(getContext(),
                this,
                Constants.ApiRequestCode.ACCEPT_FRIEND,
                contactId
                );
    }


    private void rejectFriend(String contactId){
       Data.query(getContext(),
               this,
               Constants.ApiRequestCode.REJECT_FRIEND,
               contactId
               );
    }
}
