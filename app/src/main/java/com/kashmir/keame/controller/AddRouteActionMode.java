package com.kashmir.keame.controller;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.kashmir.keame.R;
import com.kashmir.keame.activity.MainMapActivity;

/**
 * Created by Lorena Soledad on 25/08/2015.
 */
public class AddRouteActionMode implements ActionMode.Callback {

    private MainMapActivity mMainActivity;

    public AddRouteActionMode(MainMapActivity activity){
        mMainActivity = activity;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_add_route, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        //Nothing to do here :)
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_cancel:
               // mMainActivity.deleteAllTemporalMarkers();
                mMainActivity.loadMarkersOnMap();
                mode.finish();
                return true;
            case R.id.action_save:
                mMainActivity.showConfirmationDialogForRoute();
                mode.finish();
                return true;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        //Nothing to do here :)
    }

}
