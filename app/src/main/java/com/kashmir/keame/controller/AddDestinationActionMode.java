package com.kashmir.keame.controller;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.kashmir.keame.R;
import com.kashmir.keame.activity.MainMapActivity;

/**
 * Created by Lorena Soledad on 12/09/2015.
 */
public class AddDestinationActionMode implements android.view.ActionMode.Callback {

    private MainMapActivity mMainActivity;

    public AddDestinationActionMode (MainMapActivity activity){
        mMainActivity = activity;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_add_route, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_cancel:
                mMainActivity.cancelDestination();
                //mMainActivity.clearDestinationTempDataOnMap();
                mode.finish();
                return true;
            case R.id.action_save:
                mMainActivity.showConfirmationDialogForDestination();
                mode.finish();
                return true;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        //mMainActivity.clearDestinationTempDataOnMap();
        mode.finish();
    }

}
