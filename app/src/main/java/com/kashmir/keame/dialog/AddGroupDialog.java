package com.kashmir.keame.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.kashmir.keame.R;

/**
 * Created by Lorena Soledad on 12/10/2015.
 */
public class AddGroupDialog extends AppCompatDialogFragment{

    private OnDialogButtonSelected mListener;
    private EditText mEditName;
    private String mTitle;
    private String mMessage;
    private int mRequestCode;

    public static AddGroupDialog newInstance(String title, String message, int requestCode) {

        Bundle args = new Bundle();
        args.putString("DIALOG_TITLE", title);
        args.putString("DIALOG_MESSAGE", message);
        args.putInt("DIALOG_REQUEST_CODE", requestCode);

        AddGroupDialog fragment = new AddGroupDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public interface OnDialogButtonSelected {
        void onPositiveButtonSelected(String groupName, int requestCode);
        void onNegativeButtonSelected();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if(arguments != null){
            mTitle = arguments.getString("DIALOG_TITLE");
            mMessage = arguments.getString("DIALOG_MESSAGE");
            mRequestCode = arguments.getInt("DIALOG_REQUEST_CODE");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_add_group, null);
        mEditName = (EditText) view.findViewById(R.id.new_group_name);

        mEditName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean enableButton;
                if(s.toString().trim().equalsIgnoreCase(""))
                    enableButton = false;
                else
                    enableButton = true;

                ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE)
                        .setEnabled(enableButton);
            }
        });


        //Creamos la interfaz del diálogo
        builder.setView(view)
                .setTitle(mTitle)
                .setMessage(mMessage)
                .setPositiveButton(R.string.dialog_positive_button,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String name = mEditName.getText().toString().trim();
                                name = name.replace(" ", "%20");
                                mListener.onPositiveButtonSelected(name, mRequestCode);
                            }
                })
                .setNegativeButton(R.string.dialog_negative_button,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mListener.onNegativeButtonSelected();
                            }
                });


        //Una vez creado el diálogo, hacemos que inicialmente el botón de aceptar apareca
        //como desactivado, para evitar que el usuario pueda guardar un grupo con un nombre vacío.
        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE)
                        .setEnabled(false);
            }
        });

        return dialog;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mListener = (OnDialogButtonSelected) getTargetFragment();
    }
}
