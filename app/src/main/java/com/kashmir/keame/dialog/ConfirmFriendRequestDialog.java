package com.kashmir.keame.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import com.kashmir.keame.R;
import com.kashmir.keame.interfaces.OnDialogButtonSelected;
import com.kashmir.keame.model.Constants;

/**
 * Created by Lorena Soledad on 15/10/2015.
 */
public class ConfirmFriendRequestDialog extends AppCompatDialogFragment {

    private OnDialogButtonClick mListener;

    public interface OnDialogButtonClick {
        void onPositiveButtonClick(int position);
    }

    public static ConfirmFriendRequestDialog newInstance(int id, String name) {
        Bundle args = new Bundle();
        args.putInt(Constants.Contacts.ARG_CONTACT_POSITION, id);
        args.putString(Constants.Contacts.ARG_CONTACT_NAME, name);

        ConfirmFriendRequestDialog fragment = new ConfirmFriendRequestDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_confirm_request_title)
                .setMessage(String.format(getResources()
                        .getString(
                                R.string.dialog_confirm_request_message),
                                getArguments().getString(Constants.Contacts.ARG_CONTACT_NAME)))
                .setPositiveButton(R.string.dialog_confirm_request_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onPositiveButtonClick(
                                getArguments().getInt(Constants.Contacts.ARG_CONTACT_POSITION));
                    }
                })
                .setNegativeButton(R.string.dialog_confirm_request_negative, null);

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mListener = (OnDialogButtonClick) activity;
    }
}
