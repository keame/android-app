package com.kashmir.keame.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.kashmir.keame.R;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kashmir on 22/10/2015.
 */
public class ModifyGroupPeriodsDialog extends AppCompatDialogFragment implements
        Data.OnQueryListener {


    private static final String TAG = ModifyGroupPeriodsDialog.class.getSimpleName();


    private int mGroupId;

    private ProgressBar mProgressBar;
    private TextView mEmptyListTextView;



    public static ModifyGroupPeriodsDialog newInstance(int groupId) {
        
        Bundle args = new Bundle();
        args.putInt(Constants.Groups.ARGS_GROUP_ID, groupId);
        
        ModifyGroupPeriodsDialog fragment = new ModifyGroupPeriodsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if(getArguments() != null){
            mGroupId = getArguments().getInt(Constants.Groups.ARGS_GROUP_ID);
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();

        //Instanciamos los elementos
        View root = inflater.inflate(R.layout.modify_group_periods, null);
        mProgressBar = (ProgressBar) root.findViewById(R.id.progressBar);
        mEmptyListTextView = (TextView) root.findViewById(R.id.empty_list);
        /*mDestinationsList = (RecyclerView) root.findViewById(R.id.destinations_list);
        mDestinations = new ArrayList<>();*/

        //Configuramos la lista de Contactos
        //setUpRecyclerView();


        //Creamos el dialogo
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(root)
                .setTitle(R.string.dialog_modify_periods)
                .setPositiveButton(R.string.dialog_positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateGroupPeriods();
                    }
                })
                .setNegativeButton(R.string.dialog_negative_button, null);

        requestPeriods();

        return builder.create();
    }







    /* CONSULTAS -------------------------------------------------------------------------------- */
    private void requestPeriods(){

        int userId = getContext()
                .getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, Context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.GET_GROUP_PERIODS, userId, mGroupId);

        Data.newRequest(
                getContext(),
                this,
                uri,
                Request.Method.GET,
                null,
                Constants.ApiRequestCode.GET_GROUP_PERIODS,
                null
        );
    }

    private void updateGroupPeriods(){

    }





    /* PARA RECIBIR UNA RESPUESTA DEL SERVIDOR -------------------------------------------------- */

    @Override
    public void onResponse(int requestCode, String result) {

        Log.d(TAG, result);

        mProgressBar.setVisibility(View.GONE);

        if(getResponseStatus(result) != 200){
            mEmptyListTextView.setVisibility(View.VISIBLE);
            return;
        }

        switch (requestCode){
            case Constants.ApiRequestCode.GET_GROUP_PERIODS:
                //populateDestinationsList(result);
                break;
            case Constants.ApiRequestCode.UPDATE_GROUP_PERIODS:
                //Log.d(TAG, "Destinos modificados correctamente");
                break;
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        mProgressBar.setVisibility(View.GONE);
        mEmptyListTextView.setVisibility(View.VISIBLE);
    }





    /* OTRAS FUNCIONES -------------------------------------------------------------------------- */

    private int getResponseStatus(String result){
        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }


    private void populateDestinationsList(String result){
        Log.d(TAG, result);
    }

    /*private void setUpRecyclerView() {
        mDestinationsList.setHasFixedSize(true);
        mDestinationsList.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getActivity()).build());

        mAdapter = new DestinationsInGroupAdapter(mDestinations);
        mDestinationsList.setAdapter(mAdapter);

        mLayoutManager = new LinearLayoutManager(getContext());
        mDestinationsList.setLayoutManager(mLayoutManager);
    }*/


    /*private String createJSONString(){

        try {
            JSONObject destination;
            JSONObject data = new JSONObject();
            JSONArray destinations = new JSONArray();

            for(int i = 0; i < mDestinations.size(); i++){
                destination = new JSONObject();
                destination.put(Constants.Destinations.JSON_TAG_DESTINATION_ID, mDestinations.get(i).getId());
                destination.put(Constants.Destinations.JSON_TAG_DESTINATION_STATUS,
                        mDestinations.get(i).getStatusInGroup()
                                ? Constants.Destinations.JSON_OBJ_STATUS_UP
                                : Constants.Destinations.JSON_OBJ_STATUS_DOWN);
                destinations.put(destination);
            }

            data.put(Constants.Groups.JSON_TAG_GROUPID, mGroupId);
            data.put(Constants.Groups.JSON_TAG_GROUP_DESTINATIONS, destinations);

            Log.d(TAG, data.toString());
            return data.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }*/
}
