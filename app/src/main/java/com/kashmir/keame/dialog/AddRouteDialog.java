package com.kashmir.keame.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.kashmir.keame.R;

/**
 * Created by Lorena Soledad on 12/10/2015.
 */
public class AddRouteDialog extends AppCompatDialogFragment {

    private OnDialogButtonSelected mListener;
    private EditText mEditName;

    public static AddRouteDialog newInstance() {
        Bundle args = new Bundle();
        AddRouteDialog fragment = new AddRouteDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public interface OnDialogButtonSelected {
        void onPositiveButtonSelected(String routeName);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_add_route, null);
        mEditName = (EditText) view.findViewById(R.id.new_route_name);

        mEditName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean enableButton;
                if(s.toString().trim().equalsIgnoreCase(""))
                    enableButton = false;
                else
                    enableButton = true;

                ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE)
                        .setEnabled(enableButton);
            }
        });


        //Creamos la interfaz del diálogo
        builder.setView(view)
                .setTitle(R.string.dialog_title_new_route_name)
                .setMessage(R.string.dialog_message_new_route_name)
                .setPositiveButton(R.string.dialog_positive_button,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String name = mEditName.getText().toString().trim();
                                mListener.onPositiveButtonSelected(name);
                            }
                })
                .setNegativeButton(R.string.dialog_negative_button, null);


        //Una vez creado el diálogo, hacemos que inicialmente el botón de aceptar apareca
        //como desactivado, para evitar que el usuario pueda guardar una ruta con un nombre vacío.
        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE)
                        .setEnabled(false);
            }
        });

        return dialog;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (OnDialogButtonSelected) activity;
    }
}
