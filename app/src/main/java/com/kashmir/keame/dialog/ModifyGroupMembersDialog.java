package com.kashmir.keame.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kashmir.keame.R;
import com.kashmir.keame.adapter.ContactsInGroupAdapter;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Contact;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Kashmir on 17/10/2015.
 */
public class ModifyGroupMembersDialog extends AppCompatDialogFragment
    implements Data.OnQueryListener {


    private static final String TAG = ModifyGroupMembersDialog.class.getSimpleName();


    private int mGroupId;
    private ArrayList<Contact> mContacts;

    private ProgressBar mProgressBar;
    private TextView mEmptyListTextView;

    private RecyclerView mContactsList;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;


    public static ModifyGroupMembersDialog newInstance(int groupId) {

        Bundle args = new Bundle();
        args.putInt(Constants.Groups.ARGS_GROUP_ID, groupId);

        ModifyGroupMembersDialog fragment = new ModifyGroupMembersDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if(getArguments() != null){
            mGroupId = getArguments().getInt(Constants.Groups.ARGS_GROUP_ID);
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();

        //Instanciamos los elementos
        View root = inflater.inflate(R.layout.modify_group_members, null);
        mProgressBar = (ProgressBar) root.findViewById(R.id.progressBar);
        mEmptyListTextView = (TextView) root.findViewById(R.id.empty_list);
        mContactsList = (RecyclerView) root.findViewById(R.id.contacts_list);
        mContacts = new ArrayList<>();


        //Configuramos la lista de Contactos
        setUpRecyclerView();


        //Creamos el dialogo
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(root)
                .setTitle(R.string.dialog_select_contacts)
                .setPositiveButton(R.string.dialog_positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateGroupContacts();
                    }
                })
                .setNegativeButton(R.string.dialog_negative_button, null);

        requestContacts();

        return builder.create();

    }





    /* PARA RECIBIR LOS DATOS DEL SERVIDOR ------------------------------------------------------ */

    @Override
    public void onResponse(int requestCode, String result) {

        Log.d(TAG, result);

        mProgressBar.setVisibility(View.GONE);

        if(getResponseStatus(result) != 200){
            mEmptyListTextView.setVisibility(View.VISIBLE);
            return;
        }

        switch (requestCode){
            case Constants.ApiRequestCode.GET_CONTACTS_IN_GROUP:
                populateContactsList(result);
                break;
            case Constants.ApiRequestCode.UPDATE_GROUP_CONTACTS:
                Log.d(TAG, "Contactos modificados correctamente.");
                //Toast.makeText(getContext(),
                  //      R.string.update_group_member_success, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        mProgressBar.setVisibility(View.GONE);
        mEmptyListTextView.setVisibility(View.VISIBLE);
    }





    /* CONSULTAS -------------------------------------------------------------------------------- */

    private void requestContacts(){
        Data.query(
                getContext(),
                this,
                Constants.ApiRequestCode.GET_CONTACTS_IN_GROUP,
                String.valueOf(mGroupId)
        );
    }

    private void updateGroupContacts(){
        Data.query(
                getContext(),
                this,
                Constants.ApiRequestCode.UPDATE_GROUP_CONTACTS,
                createJSONString()
        );
    }




    /* OTRAS FUNCIONES -------------------------------------------------------------------------- */

    private int getResponseStatus(String result){
        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }


    private void populateContactsList(String result){

        Log.d(TAG, result);

        mContacts.clear();

        try {
            JSONObject j = new JSONObject(result);
            JSONArray groups = j.getJSONArray(Constants.Groups.JSON_TAG_GROUP_MEMBERS);

            for(int i = 0; i < groups.length(); i++ ){
                mContacts.add(new Contact(groups.getJSONObject(i),
                        Constants.ApiRequestCode.GET_CONTACTS_IN_GROUP));
            }

            if(mContacts.size() == 0){
                mEmptyListTextView.setVisibility(View.VISIBLE);
            }

            mAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setUpRecyclerView() {
        mContactsList.setHasFixedSize(true);
        mContactsList.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getActivity()).build());

        mAdapter = new ContactsInGroupAdapter(mContacts);
        mContactsList.setAdapter(mAdapter);

        mLayoutManager = new LinearLayoutManager(getContext());
        mContactsList.setLayoutManager(mLayoutManager);
    }

    private String createJSONString(){

        try {
            JSONObject member;
            JSONObject data = new JSONObject();
            JSONArray members = new JSONArray();

            for(int i = 0; i < mContacts.size(); i++){
                member = new JSONObject();
                member.put(Constants.Contacts.JSON_OBJ_CONTACT_ID, mContacts.get(i).getId());
                member.put(Constants.Contacts.JSON_OBJ_STATUS,
                        mContacts.get(i).getStatusInGroup()
                                ? Constants.Contacts.JSON_OBJ_STATUS_UP
                                : Constants.Contacts.JSON_OBJ_STATUS_DOWN);
                members.put(member);
            }

            data.put(Constants.Groups.JSON_TAG_GROUPID, mGroupId);
            data.put(Constants.Groups.JSON_TAG_GROUP_MEMBERS, members);

            Log.d(TAG, data.toString());
            return data.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

}
