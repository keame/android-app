package com.kashmir.keame.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kashmir.keame.R;
import com.kashmir.keame.model.Constants;

/**
 * Created by Lorena Soledad on 12/10/2015.
 */
public class GetDirectionsDialog extends AppCompatDialogFragment {

    private static final String TAG = GetDirectionsDialog.class.getSimpleName();

    private OnDialogButtonSelected mListener;
    private EditText mRouteOrigin;
    private EditText mRouteDestination;
    private RadioGroup mTransportationMode;

    public static GetDirectionsDialog newInstance() {
        Bundle args = new Bundle();
        GetDirectionsDialog fragment = new GetDirectionsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public interface OnDialogButtonSelected {
        void onPositiveButtonSelected(String originAdress, String destinationAdress, String mode);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_get_directions, null);
        mRouteOrigin = (EditText) view.findViewById(R.id.route_origin);
        mRouteDestination = (EditText) view.findViewById(R.id.route_destination);
        mTransportationMode = (RadioGroup) view.findViewById(R.id.transportationMode);

        TextWatcher changeListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean enableButton;

                boolean originIsEmpty =
                        mRouteOrigin.getText().toString().trim().equalsIgnoreCase("");
                boolean destinationIsEmpty =
                        mRouteDestination.getText().toString().trim().equalsIgnoreCase("");

                if(originIsEmpty || destinationIsEmpty)
                    enableButton = false;
                else
                    enableButton = true;

                ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE)
                        .setEnabled(enableButton);
            }
        };

        mRouteOrigin.addTextChangedListener(changeListener);
        mRouteDestination.addTextChangedListener(changeListener);


        //Creamos la interfaz del diálogo
        builder.setView(view)
                .setTitle(R.string.dialog_title_get_directions)
                .setMessage(R.string.dialog_message_get_directions)
                .setPositiveButton(R.string.dialog_positive_button,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String originAdress =
                                        mRouteOrigin.getText().toString().trim();
                                String destinationAdress =
                                        mRouteDestination.getText().toString().trim();

                                int radioButtonID = mTransportationMode.getCheckedRadioButtonId();
                                View buttonChecked = mTransportationMode.findViewById(radioButtonID);
                                int index = mTransportationMode.indexOfChild(buttonChecked);

                                String mode;

                                switch (index) {
                                    case 0:
                                        mode = Constants.Routes.MODE_DRIVING;
                                        break;
                                    case 1:
                                        mode = Constants.Routes.MODE_WALKING;
                                        break;
                                    case 2:
                                        mode = Constants.Routes.MODE_BICYCLING;
                                        break;
                                    default: mode = "";
                                }

                                Log.v(TAG, "Direccion de origen: " + originAdress + "Direccion de destino: " + destinationAdress
                                        + ". Y se seleccionó el modo de transporte: " + mode);

                                mListener.onPositiveButtonSelected(
                                        originAdress,
                                        destinationAdress,
                                        mode);
                            }
                        })
                .setNegativeButton(R.string.dialog_negative_button, null);


        //Una vez creado el diálogo, hacemos que inicialmente el botón de aceptar apareca
        //como desactivado, para evitar que el usuario pueda guardar una ruta con un nombre vacío.
        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE)
                        .setEnabled(false);
            }
        });

        return dialog;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //mListener = (OnDialogButtonSelected) getTargetFragment();
        mListener = (OnDialogButtonSelected) activity;
    }
}
