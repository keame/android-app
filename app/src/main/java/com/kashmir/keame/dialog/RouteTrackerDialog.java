package com.kashmir.keame.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import com.kashmir.keame.R;
import com.kashmir.keame.activity.MainMapActivity;
import com.kashmir.keame.service.RouteTrackerService;

/**
 * Created by Lorena Soledad on 27/09/2015.
 */
public class RouteTrackerDialog extends AppCompatDialogFragment {

    private Activity mCallingActivity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(R.string.alert_dialog_vigilance_message)
                .setPositiveButton(R.string.alert_dialog_vigilance_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent resultIntent = new Intent(getContext(),
                                RouteTrackerService.CancelServiceReceiver.class);

                        if (mCallingActivity instanceof MainMapActivity) {
                            MainMapActivity m = (MainMapActivity) mCallingActivity;
                            m.activateRouteForVigilance();
                            m.setServiceRunningInSettings();
                            //m.startForeground();
                            m.sendBroadcast(resultIntent);
                        }
                    }
                })
                .setNegativeButton(R.string.alert_dialog_vigilance_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //El usuario cancela, no hago nada
                    }
                });

        return dialog.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallingActivity = activity;
    }
}
