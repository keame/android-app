package com.kashmir.keame.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import com.kashmir.keame.interfaces.OnDialogButtonSelected;
import com.kashmir.keame.model.Constants;

/**
 * Created by Lorena Soledad on 16/10/2015.
 */
public class AddNewContactDialog extends AppCompatDialogFragment {

    private OnDialogButtonSelected mListener;

    public static AddNewContactDialog newInstance(int position) {

        Bundle args = new Bundle();
        args.putInt(Constants.Contacts.ARG_CONTACT_ID, position);

        AddNewContactDialog fragment = new AddNewContactDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Solicitud Pendiente")
                .setMessage("Qué desea hacer?")
                .setNegativeButton("Rechazar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onNegativeButtonClick(
                                getArguments().getInt(Constants.Contacts.ARG_CONTACT_ID));
                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onPositiveButtonClick(
                                getArguments().getInt(Constants.Contacts.ARG_CONTACT_ID));
                    }
                })
                .setNeutralButton("Cancelar", null);

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mListener = (OnDialogButtonSelected) getTargetFragment();
    }
}
