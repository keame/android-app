package com.kashmir.keame.dialog;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.kashmir.keame.R;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Permission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Kashmir on 18/10/2015.
 */
public class ModifyGroupPermissionsDialog extends AppCompatDialogFragment
    implements Data.OnQueryListener{

    private static final String TAG = ModifyGroupMembersDialog.class.getSimpleName();

    private int mGroupId;

    private ArrayList<Permission> mPermissions;
    private TextView[] mWeekDays;
    private int mStartHour = 0;
    private int mStartMinute = 0;
    private int mEndHour = 0;
    private int mEndMinute= 0;


    private boolean[] weekdays;

    private PermissionsAdapter mAdapter;

    private ListView mPermissionsList;
    private ProgressBar mProgressBar;
    private TextView mEmptyListTextView;

    private TextView mTimeBegin;
    private TextView mTimeEnd;


    public static ModifyGroupPermissionsDialog newInstance(int groupId) {

        Bundle args = new Bundle();
        args.putInt(Constants.Groups.ARGS_GROUP_ID, groupId);

        ModifyGroupPermissionsDialog fragment = new ModifyGroupPermissionsDialog();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if(getArguments() != null ){
            mGroupId = getArguments().getInt(Constants.Groups.ARGS_GROUP_ID);
        }

        requestGroupPermissions();

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View root = inflater.inflate(R.layout.group_permissions_dialog, null);

        mProgressBar = (ProgressBar) root.findViewById(R.id.progressBar);
        mEmptyListTextView = (TextView) root.findViewById(R.id.empty_list);
        mTimeBegin = (TextView) root.findViewById(R.id.begin_time);
        mTimeEnd = (TextView) root.findViewById(R.id.end_time);

        //Dias de semana
        mWeekDays = new TextView[7];
        mWeekDays[0] = (TextView) root.findViewById(R.id.sunday);
        mWeekDays[1] = (TextView) root.findViewById(R.id.monday);
        mWeekDays[2] = (TextView) root.findViewById(R.id.tuesday);
        mWeekDays[3] = (TextView) root.findViewById(R.id.wednesday);
        mWeekDays[4] = (TextView) root.findViewById(R.id.thrusday);
        mWeekDays[5] = (TextView) root.findViewById(R.id.friday);
        mWeekDays[6] = (TextView) root.findViewById(R.id.saturday);
        setWeekdaysListener();

        mPermissions = new ArrayList<>();
        mAdapter = new PermissionsAdapter(getContext(), mPermissions);

        //Configurando la lista
        mPermissionsList = (ListView) root.findViewById(R.id.permissions_List);
        mPermissionsList.setAdapter(mAdapter);
        mPermissionsList.setDivider(null);
        mPermissionsList.setDividerHeight(0);

        mTimeBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(mStartHour,mStartMinute,true);
            }
        });

        mTimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(mEndHour,mEndMinute,false);
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(root)
                .setTitle(R.string.popup_groups_permissions)
                .setPositiveButton(R.string.dialog_positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateGroupPermissions();
                    }
                })
                .setNegativeButton(R.string.dialog_negative_button, null);

        return builder.create();
    }

    private void showTimePicker(final int hour, int minute, final boolean beginHour){
        TimePickerDialog timePicker = new TimePickerDialog(
                getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if(beginHour){
                            mStartHour = hourOfDay;
                            mStartMinute = minute;
                        } else {
                            mEndHour = hourOfDay;
                            mEndMinute = minute;
                        }
                    }
                },
                hour,
                minute,
                true
        );

        timePicker.show();
    }

    private void setWeekdaysListener(){

        for (int i = 0; i < 7; i++){
            mWeekDays[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView view = ((TextView) v);
                    Typeface typeface = view.getTypeface();

                    if(typeface.isBold()){
                        view.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                    } else {
                        view.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                    }
                }
            });
        }
    }




    /* CONSULTAS Y RESPUESTAS DEL SERVIDOR ------------------------------------------------------ */

    private void requestGroupPermissions(){
        Data.query(
                getContext(),
                this,
                Constants.ApiRequestCode.GET_GROUP_PERMISSIONS,
                String.valueOf(mGroupId)
        );
    }

    private void updateGroupPermissions(){
        Data.query(
                getContext(),
                this,
                Constants.ApiRequestCode.UPDATE_GROUP_PERMISSIONS,
                createJSONString()
        );
    }

    @Override
    public void onResponse(int requestCode, String result) {
        Log.d(TAG, result);

        if(getResponseStatus(result) != 200){
            updateUI();
            return;
        }

        switch (requestCode){
            case Constants.ApiRequestCode.GET_GROUP_PERMISSIONS:
                populatePermissionsList(result);
                break;
            case Constants.ApiRequestCode.UPDATE_GROUP_PERMISSIONS:
                Log.d(TAG, "Permisos modificados correctamente");
                break;
        }

    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        mProgressBar.setVisibility(View.GONE);
        mEmptyListTextView.setVisibility(View.VISIBLE);
    }







    /* PARSEO DE DATOS -------------------------------------------------------------------------- */

    private String createJSONString(){
        try {
            JSONObject permission;
            JSONObject data = new JSONObject();
            JSONArray permissions = new JSONArray();
            JSONObject period;
            JSONObject days;

            //Valores de los permisos
            for(int i = 0; i < mPermissions.size(); i++){
                permission = new JSONObject();
                permission.put(Constants.Permission.JSON_TAG_PERMISSION_ID, mPermissions.get(i).getId());
                permission.put(Constants.Permission.JSON_TAG_PERMISSION_STATUS,
                        mPermissions.get(i).getValue()
                                ? Constants.Permission.JSON_OBJ_STATUS_UP
                                : Constants.Permission.JSON_OBJ_STATUS_DOWN);
                permissions.put(permission);
            }

            //Dias
            days = new JSONObject();
            days.put(Constants.Permission.SUNDAY, mWeekDays[0].getTypeface().isBold());
            days.put(Constants.Permission.MONDAY, mWeekDays[1].getTypeface().isBold());
            days.put(Constants.Permission.TUESDAY, mWeekDays[2].getTypeface().isBold());
            days.put(Constants.Permission.WEDNESDAY, mWeekDays[3].getTypeface().isBold());
            days.put(Constants.Permission.THURSDAY, mWeekDays[4].getTypeface().isBold());
            days.put(Constants.Permission.FRIDAY, mWeekDays[5].getTypeface().isBold());
            days.put(Constants.Permission.SATURDAY, mWeekDays[6].getTypeface().isBold());

            //Horarios
            period = new JSONObject();
            period.put(Constants.Permission.START_HOUR, mStartHour);
            period.put(Constants.Permission.START_MINUTE, mStartMinute);
            period.put(Constants.Permission.END_HOUR, mEndHour);
            period.put(Constants.Permission.END_MINUTE, mEndMinute);
            period.put(Constants.Permission.DAYS, days);

            data.put(Constants.Groups.JSON_TAG_GROUPID, mGroupId);
            data.put(Constants.Permission.JSON_ARRAY_PERMISSIONS, permissions);
            data.put(Constants.Permission.PERIOD, period);

            Log.d(TAG, data.toString());
            return data.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void populatePermissionsList(String result){

        mPermissions.clear();

        try {
            JSONObject object = new JSONObject(result);
            JSONArray permissions = object.getJSONArray(Constants.Groups.JSON_TAG_GROUP_PERMISSIONS);

            for(int i = 0; i < permissions.length(); i++ ){
                mPermissions.add(new Permission(permissions.getJSONObject(i)));
            }

            //Obtengo los horarios
            JSONObject period = object.getJSONObject(Constants.Permission.PERIOD);
            mStartHour = period.getInt(Constants.Permission.START_HOUR);
            mStartMinute = period.getInt(Constants.Permission.START_MINUTE);
            mEndHour = period.getInt(Constants.Permission.END_HOUR);
            mEndMinute = period.getInt(Constants.Permission.END_MINUTE);

            JSONObject days = period.getJSONObject(Constants.Permission.DAYS);
            weekdays = new boolean[7];
            weekdays[0] = days.getBoolean(Constants.Permission.SUNDAY);
            weekdays[1] = days.getBoolean(Constants.Permission.MONDAY);
            weekdays[2] = days.getBoolean(Constants.Permission.TUESDAY);
            weekdays[3] = days.getBoolean(Constants.Permission.WEDNESDAY);
            weekdays[4] = days.getBoolean(Constants.Permission.THURSDAY);
            weekdays[5] = days.getBoolean(Constants.Permission.FRIDAY);
            weekdays[6] = days.getBoolean(Constants.Permission.SATURDAY);
            initWeekdays();

            updateUI();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void initWeekdays() {
        for(int i = 0; i < 7; i++){
            if(weekdays[i]){
                mWeekDays[i].setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            } else {
                mWeekDays[i].setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            }
        }
    }

    private int getResponseStatus(String result){
        try {
            JSONObject obj = new JSONObject(result);
            int status = obj.getInt("status");

            return status;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private void updateUI() {
        if( mPermissions.size() == 0){
            mEmptyListTextView.setVisibility(View.VISIBLE);
        } else {
            mAdapter.notifyDataSetChanged();
        }

        mProgressBar.setVisibility(View.GONE);
    }






    /* ADAPTADOR DE LA LISTA -------------------------------------------------------------------- */

    private class PermissionsAdapter extends ArrayAdapter<Permission> {

        private ArrayList<Permission> mPermissions;
        private Context mContext;

        public PermissionsAdapter(Context context, ArrayList<Permission> objects) {
            super(context, -1, objects);
            mContext = context;
            mPermissions = objects;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View row = inflater.inflate(R.layout.group_permissions_item, parent, false);

            //Obtenemos los elementos visuales
            Switch permissionSwitch = (Switch) row.findViewById(R.id.permission_switch);
            TextView permissionName = (TextView) row.findViewById(R.id.permission_name);
            TextView permissionDescription = (TextView) row.findViewById(R.id.permission_description);

            //Cargamos los datos del permiso
            Permission currentPermission = mPermissions.get(position);
            permissionSwitch.setChecked(currentPermission.getValue());
            permissionName.setText(currentPermission.getName());
            permissionDescription.setText(currentPermission.getDescription());

            //Cuando el usuario presiona el botón, cambia el valor
            permissionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    mPermissions.get(position).setValue(isChecked);
                }
            });

            return row;
        }

    }
}
