package com.kashmir.keame.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.kashmir.keame.service.RequestDestinationsService;

public class AlarmReceiver extends BroadcastReceiver {

    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        RequestDestinationsService.handleDestinationRequest(context);
    }
}
