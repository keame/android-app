package com.kashmir.keame.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;

import com.kashmir.keame.model.Constants;
import com.kashmir.keame.service.FollowingService;
import com.kashmir.keame.service.RequestDestinationsService;
import com.kashmir.keame.service.ShakeEventManager;

public class BootCompletedReceiver extends BroadcastReceiver {

    private static final String TAG = BootCompletedReceiver.class.getSimpleName();

    private boolean isGPSActive;
    private boolean isNetworkActive;

    public BootCompletedReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "BroadcastReceiver disparado.");

        LocationManager locationManager =
                (LocationManager) context.getSystemService(context.LOCATION_SERVICE);

        isGPSActive = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkActive = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if(isGPSActive || isNetworkActive){

            //Buscamos los destinos para poder disparar los geofences
            RequestDestinationsService.handleDestinationRequest(context);

            //Disparamos el servicio de Tracking
            FollowingService.startTrackerService(context);

        } else {
            Log.d(TAG, "No se han podido activar los servicios de localizacion.");
        }

        if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
            Log.d(TAG, "Se inicio el celular");
            Intent shakingIntent = new Intent(context, ShakeEventManager.class);
            context.startService(shakingIntent);
        }

    }

}
