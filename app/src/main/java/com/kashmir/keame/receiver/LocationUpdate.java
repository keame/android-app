package com.kashmir.keame.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.android.volley.Request;
import com.google.android.gms.location.LocationResult;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class LocationUpdate extends BroadcastReceiver implements Data.OnQueryListener{

    private static final String TAG = LocationUpdate.class.getSimpleName();

    public LocationUpdate() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Location currentLocation;

        if(LocationResult.hasResult(intent)){
            LocationResult result = LocationResult.extractResult(intent);
            currentLocation = result.getLastLocation();
            sendLocationUpdate(context, currentLocation);
        }

    }

    private void sendLocationUpdate(Context context, Location currentLocation){
        int userId = context
                .getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.SEND_LOCATION_UPDATE, userId);

        Data.newRequest(
                context,
                this,
                uri,
                Request.Method.PUT,
                null,
                Constants.ApiRequestCode.SEND_LOCATION_UPDATE,
                createJSON(currentLocation)
        );
    }

    private String createJSON(Location currentLocation){
        JSONObject locationUpdate = new JSONObject();
        JSONObject position = new JSONObject();

        try {

            position.put("lat", currentLocation.getLatitude());
            position.put("long", currentLocation.getLongitude());

            locationUpdate.put("position", position);

            Log.d(TAG, locationUpdate.toString());

            return locationUpdate.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onResponse(int requestCode, String result) {
        Log.d(TAG, result);
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Log.d(TAG, "Error: " + statusCode);
    }

}
