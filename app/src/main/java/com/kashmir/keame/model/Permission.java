package com.kashmir.keame.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kashmir on 18/10/2015.
 */
public class Permission {

    private String mName;
    private String mDescription;
    private int mId;
    private boolean mValue;

    public Permission(String name, boolean value){
        mName = name;
        mValue = value;
    }

    public Permission(JSONObject permission){
        try {
            mName = permission.getString(Constants.Permission.JSON_TAG_PERMISSION_NAME);
            mId = permission.getInt(Constants.Permission.JSON_TAG_PERMISSION_ID);
            mValue = permission.getBoolean(Constants.Permission.JSON_TAG_PERMISSION_STATUS);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public boolean getValue() {
        return mValue;
    }

    public void setValue(boolean value) {
        this.mValue = value;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

}
