package com.kashmir.keame.model;

import java.security.PublicKey;

/**
 * Created by Lorena Soledad on 26/09/2015.
 */
public class Constants {

    public static class Preferences {
        public static final String PREFERENCES_FILE = "com.kashmir.keame.PREFERENCES";
        public static final String CURRENT_USER_ID = "current_user_id";
        public static final String CURRENT_USER_FIRST_NAME = "current_user_first_name";
        public static final String CURRENT_USER_LAST_NAME = "current_user_last_name";
        public static final String CURRENT_USER_MAIL = "current_user_mail";
        public static final String CURRENT_USER_TELEPHONE = "current_user_phone";
        public static final String IS_USER_LOGGED_IN = "is_user_logged_in";
        public static final String IS_USER_FIRST_TIME = "is_user_first_time";
        public static final String CARE_MODE_STATE = "care_mode_state";
        public static final String ROUTE_VIGILANCE_INTERVAL = "route_vigilance_interval";
        public static final String TRACKING_INTERVAL = "traking_interval";
        public static final String IS_ROUTE_SERVICE_RUNNING = "is_route_service_running";

        public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
        public static final String REGISTRATION_COMPLETE = "registrationComplete";
    }

    public static class Registration {
        public static final String CURRENT_USER_ID = "current_user_id";
        public static final String DEVICE_NAME = "device_name";
        public static final String REGISTRATION_ID = "registration_id";
        public static final String DEVICE_IMEI = "imei";
    }

    public static class RouteTraker {
        public static final String EXTRA_ROUTE = "com.kashmir.keame.RouteTrackerService.ROUTE";

        public static final int REQUEST_CHECK_SETTINGS = 9876;

        public static final int SMALLEST_DISPLACEMENT = 100; //En metros
        public static final int TIME_INTERVAL = 1000 * 60 * 5; //En milisegundos = 5 minutos
        public static final int FASTEST_TIME_INTERVAL = 1000 * 60 * 3;//En milisegundos = 3 minutos
        public static final int MAX_DISTANCE = 200; //En metros
        public static final int NOTIFICATION_ID = 192;
        public static final int NOTIFICATION_CARE_ID = 156;
    }

    public static class Traker {
        public static final int SMALLEST_DISPLACEMENT = 100; //En metros
        public static final int TIME_INTERVAL = 1000 * 60 * 20; //En milisegundos = 20 minutos
        public static final int FASTEST_TIME_INTERVAL = 1000 * 60 * 15;//En milisegundos = 15 minutos
    }

    public static class Geofence {
        public static final int RADIUS = 100; //En metros
    }

    public static class Profile {
        public static final String EMAIL = "email";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String TELEPHONE = "telephone";
        public static final String PASSWORD = "pass";
        public static final String PIN = "pin";
    }

    public static class Contacts {
        public static final String JSON_ARRAY_CONTACTS = "contacts";
        public static final String JSON_OBJ_USER_ID = "user_id";
        public static final String JSON_OBJ_FIRST_NAME = "first_name";
        public static final String JSON_OBJ_LAST_NAME = "last_name";
        public static final String JSON_OBJ_EMAIL = "email";
        public static final String JSON_OBJ_TELEPHONE_NUMBER = "telephone_number";
        public static final String JSON_OBJ_MOBILE_NUMBER = "mobile_number";
        public static final String JSON_OBJ_EMERGENCY = "emergency";

        public static final String JSON_OBJ_CONTACT_ID = "contact_id";
        //public static final String JSON_OBJ_NAME = "name";
        public static final String JSON_OBJ_STATUS = "status";
        public static final String JSON_OBJ_STATUS_UP = "up";
        public static final String JSON_OBJ_STATUS_DOWN = "down";

        public static final String CONFIRM_SEND_REQUEST_DIALOG = "send_request";
        public static final String ARG_CONTACT_ID = "contact_id";
        public static final String ARG_CONTACT_NAME = "contact_name";
        public static final String ARG_CONTACT_POSITION = "contact_position";

        public static final String STATE_CURRENT_USER_ID = "current_user_id";

        public static final String LAST_LAT = "last_lat";
        public static final String LAST_LONG = "last_long";
    }

    public static class Groups {
        public static final String JSON_ARRAY_GROUPS = "groups";
        public static final String JSON_TAG_GROUP_ID = "id";
        public static final String JSON_TAG_GROUP_NAME = "name";
        public static final String JSON_TAG_GROUPID = "group_id";
        public static final String JSON_TAG_GROUP_MEMBERS = "members";
        public static final String JSON_TAG_GROUP_PERMISSIONS = "permissions";
        public static final String JSON_TAG_GROUP_DESTINATIONS = "destinations";

        public static final String DIALOG_MODIFY_GROUP_DESTINATION = "dialog_destinations";
        public static final String DIALOG_MODIFY_GROUP_MEMBER = "dialog_members";
        public static final String DIALOG_MODIFY_GROUP_PERMISSION = "dialog_permission";
        public static final String DIALOG_MODIFY_GROUP_PERIODS = "dialog_periods";
        public static final String ARGS_GROUP_ID = "group_id";

    }

    public static class Destinations {
        public static final String JSON_ARRAY_DESTINATIONS = "destinations";
        public static final String JSON_TAG_DESTINATION_ID = "destination_id";
        public static final String JSON_TAG_DESTINATION_NAME = "name";
        public static final String JSON_TAG_DESTINATION_LATITUDE = "latitude";
        public static final String JSON_TAG_DESTINATION_LONGITUDE = "longitude";
        public static final String JSON_TAG_DESTINATION_STATUS = "status";
        public static final String JSON_OBJ_STATUS_UP = "up";
        public static final String JSON_OBJ_STATUS_DOWN = "down";
    }

    public static class Routes{
        public static final String JSON_TAG_ROUTE_COORDINATES = "coordinates";
        public static final String JSON_TAG_ROUTE_LATITUDE = "lat";
        public static final String JSON_TAG_ROUTE_LONGITUDE = "long";
        public static final String JSON_TAG_ROUTE_NAME = "name";

        public static final String EXTRA_ROUTE = "extra_route";
        public static final String DIALOG_NEW_ROUTE = "dialog_new_route";
        public static final String DIALOG_GET_DIRECTIONS = "dialog_get_destinations";
        public static final String MODE_DRIVING = "driving";
        public static final String MODE_WALKING = "walking";
        public static final String MODE_BICYCLING = "bicycling";
    }

    public static class Permission {
        public static final String JSON_ARRAY_PERMISSIONS = "permissions";
        public static final String JSON_TAG_PERMISSION_ID = "permission_id";
        public static final String JSON_TAG_PERMISSION_NAME = "name";
        public static final String JSON_TAG_PERMISSION_DESCRIPTION = "description";
        public static final String JSON_TAG_PERMISSION_STATUS = "status";
        public static final String JSON_OBJ_STATUS_UP = "up";
        public static final String JSON_OBJ_STATUS_DOWN = "down";

        //Período de los permisos
        public static final String PERIOD = "period";
        public static final String START_HOUR = "start_hour";
        public static final String START_MINUTE = "start_minute";
        public static final String END_HOUR = "end_hour";
        public static final String END_MINUTE = "end_minute";

        public static final String DAYS = "days";
        public static final String SUNDAY = "sunday";
        public static final String MONDAY = "monday";
        public static final String TUESDAY = "tuesday";
        public static final String WEDNESDAY = "wednesday";
        public static final String THURSDAY = "thursday";
        public static final String FRIDAY = "friday";
        public static final String SATURDAY = "saturday";



    }

    public static class Error {
        public static final String NETWORK_LOST =
                "Se perdio la conexion: No hay red.";
        public static final String SERVICE_DISCONNECTED =
                "Se perdio la conexion: Se desconecto el servicio.";

    }

    public static class ApiRequestCode {

        public static final int LOGIN = 100;
        public static final int REGISTER_DEVICE = 101;
        public static final int REGISTER_USER = 102;

        //Grupos
        public static final int GET_ALL_GROUPS = 1000;
        public static final int ADD_NEW_GROUP = 1001;
        public static final int EDIT_GROUP = 1002;
        public static final int DELETE_GROUP = 1003;
        public static final int UPDATE_GROUP_CONTACTS = 1004;
        public static final int UPDATE_GROUP_DESTINATION = 1005;
        public static final int GET_GROUP_PERMISSIONS = 1006;
        public static final int UPDATE_GROUP_PERMISSIONS = 1007;
        public static final int GET_GROUP_PERIODS = 1008;
        public static final int UPDATE_GROUP_PERIODS = 1009;

        //Contactos
        public static final int GET_ALL_CONTACTS = 2000;
        public static final int GET_ALL_PENDING_CONTACS = 2001;
        public static final int SEARCH_CONTACTS = 2002;
        public static final int SEND_FRIEND_REQUEST = 2003;
        public static final int ACCEPT_FRIEND = 2004;
        public static final int REJECT_FRIEND = 2005;
        public static final int DELETE_FRIEND = 2006;
        public static final int GET_CONTACTS_IN_GROUP = 2007;
        public static final int SEND_POSITION_REQUEST = 2008;
        public static final int SEND_STATE_REQUEST = 2009;
        public static final int ADD_EMERGENCY_CONTACT = 2010;
        public static final int REMOVE_EMERGENCY_CONTACT = 2011;
        public static final int GET_ALL_EMERGENCY_CONTACTS = 2012;
        public static final int ANSWER_STATE_REQUEST = 2013;
        public static final int ANSWER_POSITION_REQUEST = 2014;

        //Routes
        public static final int GET_ALL_ROUTES = 3000;
        public static final int DELETE_ROUTE = 3001;
        public static final int ADD_NEW_ROUTE = 3002;
        public static final int GET_GOOGLE_DIRECTIONS = 3003;

        //Destinos
        public static final int GET_ALL_DESTINATIONS = 4000;
        public static final int ADD_DESTINATION = 4002;
        public static final int DELETE_DESTINATION = 4001;
        public static final int GET_DESTINATIONS_IN_GROUP = 4003;

        //FUNCIONES DE SEGUIMIENTO Y YA LLEGUE
        public static final int SEND_LOCATION_UPDATE = 5000;
        public static final int SEND_GEOFENCE_UPDATE = 5001;
        public static final int REQUEST_CONTACTS_POSITION = 5002;

        //Preferencias del usuario y autenticación
        public static final int UPDATE_USER_CONFIG = 6000;
        public static final int UPDATE_INVISIBLE_MODE = 6001;
        public static final int UPDATE_PIN = 6002;
        public static final int UPDATE_TAG = 6003;
        public static final int AUTHENTICATE_PIN = 6004;
        public static final int AUTHENTICATE_TAG = 6005;
        public static final int UPDATE_PASSWORD = 6006;

        //Notificaciones
        public static final int SEND_DESTINATIONS_NOTIFICATION = 7000;
        public static final int SEND_ALERT = 7001;
    }

    public static class Uri {
        public static String REGISTER_USER =
                "https://keame.herokuapp.com/api/v1/users/new";

        public static String ADD_NEW_ROUTE =
                "https://keame.herokuapp.com/api/v1/users/%d/routes/add/";

        public static String GET_ALL_DESTINATIONS =
                "https://keame.herokuapp.com/api/v1/users/%d/destination/";

        public static String ADD_EMERGENCY_CONTACT =
                "https://keame.herokuapp.com/api/v1/users/%d/sos/add_member/";
        public static String REMOVE_EMERGENCY_CONTACT =
                "https://keame.herokuapp.com/api/v1/users/%d/sos/remove_member/?contact_id=%d";
        public static String GET_ALL_EMERGENCY_CONTACTS =
                "https://keame.herokuapp.com/api/v1/users/%d/sos";

        public static String REGISTER_DEVICE =
                "https://app.keame.com.ar/api/v1/users/%d/register_device";
        public static String UPDATE_USER_CONFIG =
                "https://keame.herokuapp.com/api/v1/users/%d/update";
        public static String UPDATE_INVISIBLE_MODE =
                "https://keame.herokuapp.com/api/v1/users/%d/update_invisible_mode/";

        //FUNCIONES DE SEGUIMIENTO Y YA LLEGUE
        public static String SEND_LOCATION_UPDATE =
                "https://keame.herokuapp.com/api/v1/users/%d/update_position";
        public static String SEND_GEOFENCE_UPDATE = "";
        public static String REQUEST_CONTACTS_POSITION = "";

        //Notificaciones
        public static String SEND_DESTINATIONS_NOTIFICATION =
                "https://keame.herokuapp.com/api/v1/users/%d/arrived/";
        public static String SEND_ALERT =
                "https://app.keame.com.ar/api/v1/users/%d/sos/send";

        //Contactos
        public static String SEND_STATE_REQUEST =
                "https://app.keame.com.ar/api/v1/users/%d/query/status";
        public static String ANSWER_STATE_REQUEST =
                "https://app.keame.com.ar/api/v1/users/%d/query/response_status";
        public static String SEND_POSITION_REQUEST =
                "https://app.keame.com.ar/api/v1/users/%d/query/position";
        public static String ANSWER_POSITION_REQUEST =
                "https://app.keame.com.ar/api/v1/users/%d/query/response_position";

        public static String UPDATE_PIN =
                "https://app.keame.com.ar/api/v1/users/%d/update_pin";
        public static String UPDATE_TAG =
                "https://app.keame.com.ar/api/v1/users/%d/update_tag";
        public static String AUTHENTICATE_PIN =
                "https://app.keame.com.ar/api/v1/users/%d/auth_pin";
        public static String AUTHENTICATE_TAG =
                "https://app.keame.com.ar/api/v1/users/%d/auth_tag";
        public static String UPDATE_PASSWORD =
                "https://keame.herokuapp.com/api/v1/users/%d/update_password";

        //TODO:BORRAR Grupos
        public static String GET_GROUP_PERIODS =
                "https://keame.herokuapp.com/api/v1/users/%d/groups/period/?group_id=%d";
        public static String UPDATE_GROUP_PERIODS =
                "";
    }
}
