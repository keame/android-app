package com.kashmir.keame.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lorena Soledad on 11/08/2015.
 */
public class Group {

    private int id;
    private String name;
    private ArrayList<Contact> mGroupContacs;


    /* CONSTRUCTORES ---------------------------------------------------------------------------- */
    public Group(){
        name = "default";
    }

    public Group(JSONObject group) throws JSONException {
        id = group.getInt(Constants.Groups.JSON_TAG_GROUP_ID);
        name = group.getString(Constants.Groups.JSON_TAG_GROUP_NAME);
        //JSONArray permissions = group.getJSONArray(Constants.Groups.JSON_TAG_GROUP_PERMISSIONS);
        //JSONArray contacts = group.getJSONArray(Constants.Groups.JSON_TAG_GROUP_CONTACS);
    }



    /* GETTERS & SETTERS ------------------------------------------------------------------------ */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
