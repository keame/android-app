package com.kashmir.keame.model;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

public class Contact {

    private int id;
    private LatLng position;
    private String name;
    private String firstName;
    private String lastName;
    private boolean statusInGroup;
    private boolean isEmergencyContact;
    private boolean hasAnEmergency;
    private String email;
    private String telephoneNumber;
    private String mobileNumber;


    /* CONSTRUCTORES ---------------------------------------------------------------------------- */

    public Contact(String name, LatLng position){
        this.name = name;
        this.position = position;
    }

    public Contact(JSONObject user){
        try {
            id = user.getInt("id");
            name = user.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Contact(JSONObject user, int requestCode){
        switch (requestCode){
            case 0:
                try {
                    id = user.getInt(Constants.Contacts.JSON_OBJ_USER_ID);
                    firstName = user.getString(Constants.Contacts.JSON_OBJ_FIRST_NAME);
                    lastName = user.getString(Constants.Contacts.JSON_OBJ_LAST_NAME);
                    email = user.getString(Constants.Contacts.JSON_OBJ_EMAIL);
                    telephoneNumber = user.getString(Constants.Contacts.JSON_OBJ_TELEPHONE_NUMBER);
                    mobileNumber = user.getString(Constants.Contacts.JSON_OBJ_MOBILE_NUMBER);
                    if(user.has(Constants.Contacts.JSON_OBJ_EMERGENCY)){
                        isEmergencyContact = user.getBoolean(Constants.Contacts.JSON_OBJ_EMERGENCY);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.ApiRequestCode.GET_CONTACTS_IN_GROUP:
                try {
                    id = user.getInt(Constants.Contacts.JSON_OBJ_CONTACT_ID);
                    firstName = user.getString(Constants.Contacts.JSON_OBJ_FIRST_NAME);
                    lastName = user.getString(Constants.Contacts.JSON_OBJ_LAST_NAME);
                    statusInGroup = user.getBoolean(Constants.Contacts.JSON_OBJ_STATUS);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.ApiRequestCode.REQUEST_CONTACTS_POSITION:
                try {
                    id = user.getInt(Constants.Contacts.JSON_OBJ_USER_ID);
                    firstName = user.getString(Constants.Contacts.JSON_OBJ_FIRST_NAME);
                    lastName = user.getString(Constants.Contacts.JSON_OBJ_LAST_NAME);
                    double latitude = user.getDouble(Constants.Contacts.LAST_LAT);
                    double longitude = user.getDouble(Constants.Contacts.LAST_LONG);
                    position = new LatLng(latitude,longitude);
                    hasAnEmergency = user.getBoolean(Constants.Contacts.JSON_OBJ_EMERGENCY);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS:
                try {
                    id = user.getInt("id");
                    firstName = user.getString(Constants.Contacts.JSON_OBJ_FIRST_NAME);
                    lastName = user.getString(Constants.Contacts.JSON_OBJ_LAST_NAME);
                    email = user.getString(Constants.Contacts.JSON_OBJ_EMAIL);
                    telephoneNumber = user.getString("telephone_number");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }



    /* GETTERS & SETTERS ------------------------------------------------------------------------ */

    public boolean getStatusInGroup() {
        return statusInGroup;
    }

    public void setStatusInGroup(boolean statusInGroup) {
        this.statusInGroup = statusInGroup;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEmergencyContact() {
        return isEmergencyContact;
    }

    public boolean hasAnEmergency() {
        return hasAnEmergency;
    }


    public void setIsEmergencyContact(boolean isEmergencyContact) {
        this.isEmergencyContact = isEmergencyContact;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }
}
