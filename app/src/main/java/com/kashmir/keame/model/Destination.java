package com.kashmir.keame.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lorena Soledad on 09/08/2015.
 */
public class Destination implements Parcelable {

    private int id;
    private String name;
    private LatLng position;
    private boolean statusInGroup;


    public static final Parcelable.Creator<Destination> CREATOR =
            new Parcelable.Creator<Destination>() {

                @Override
                public Destination createFromParcel(Parcel source) {
                    return new Destination(source);
                }

                @Override
                public Destination[] newArray(int size) {
                    return new Destination[size];
                }
            };

    public Destination() {

    }

    public Destination(JSONObject destination){
        try {
            id = destination.getInt(Constants.Destinations.JSON_TAG_DESTINATION_ID);
            name = destination.getString(Constants.Destinations.JSON_TAG_DESTINATION_NAME);
            double lat =
                    destination.getDouble(Constants.Destinations.JSON_TAG_DESTINATION_LATITUDE);
            double lon =
                    destination.getDouble(Constants.Destinations.JSON_TAG_DESTINATION_LONGITUDE);
            position = new LatLng(lat, lon);

            if(destination.has(Constants.Destinations.JSON_TAG_DESTINATION_STATUS)){
                statusInGroup = destination.getBoolean(Constants.Destinations.JSON_TAG_DESTINATION_STATUS);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Destination(String name, LatLng position){
        this.name = name;
        this.position = position;
    }

    public Destination(Parcel source){
        readFromParcel(source);
    }


    public int getId() {
        return id;
    }

    public boolean getStatusInGroup() {
        return statusInGroup;
    }

    public void setStatusInGroup(boolean statusInGroup) {
        this.statusInGroup = statusInGroup;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(position.latitude);
        dest.writeDouble(position.longitude);
    }

    public void readFromParcel(Parcel source){
        name = source.readString();
        position = new LatLng(source.readDouble(), source.readDouble());
    }
}
