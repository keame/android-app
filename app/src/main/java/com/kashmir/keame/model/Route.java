package com.kashmir.keame.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lorena Soledad on 11/08/2015.
 */
public class Route implements Parcelable {

    private int mId;
    private String mName;
    private ArrayList<LatLng> mGeopoints;

    public static final Parcelable.Creator<Route> CREATOR =
            new Parcelable.Creator<Route>() {

                @Override
                public Route createFromParcel(Parcel source) {
                    return new Route(source);
                }

                @Override
                public Route[] newArray(int size) {
                    return new Route[size];
                }
            };

    public Route(){

    }

    public Route(JSONObject route){
        try {
            mId = route.getInt("route_id");
            mName = route.getString("name");
            mGeopoints = new ArrayList<>();

            JSONArray points = route.getJSONArray("coordinates");
            JSONObject point;
            double latitude;
            double longitude;

            for (int i = 0; i < points.length(); i++ ){
                point = points.getJSONObject(i);
                latitude = point.getDouble("latitude");
                longitude = point.getDouble("longitude");
                mGeopoints.add(new LatLng(latitude, longitude));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Route(String name, ArrayList<LatLng> geopoints){
        mName = name;
        mGeopoints = geopoints;
    }

    public Route(Parcel source){
        mGeopoints = new ArrayList<LatLng>();
        readFromParcel(source);
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public ArrayList<LatLng> getGeopoints() {
        return mGeopoints;
    }

    public void setGeopoints(ArrayList<LatLng> geopoints) {
        this.mGeopoints = geopoints;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeTypedList(mGeopoints);
    }

    public void readFromParcel(Parcel source){
        mName = source.readString();
        source.readTypedList(mGeopoints, LatLng.CREATOR);
    }
}
