/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kashmir.keame.service;

        import android.app.IntentService;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.os.Build;
        import android.preference.PreferenceManager;
        import android.support.v4.content.LocalBroadcastManager;
        import android.telephony.TelephonyManager;
        import android.util.Log;

        import com.android.volley.Request;
        import com.google.android.gms.gcm.GcmPubSub;
        import com.google.android.gms.gcm.GoogleCloudMessaging;
        import com.google.android.gms.iid.InstanceID;
        import com.kashmir.keame.R;
        import com.kashmir.keame.controller.Data;
        import com.kashmir.keame.model.Constants;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.IOException;

public class RegistrationIntentService extends IntentService implements Data.OnQueryListener{

    private static final String TAG = RegistrationIntentService.class.getSimpleName();
    private static final String[] TOPICS = {"global"};

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        int currentUserId = intent.getIntExtra(Constants.Registration.CURRENT_USER_ID, -1);

        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // [START get_token]
            InstanceID instanceID = InstanceID.getInstance(this);
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.

            Log.d(TAG, "SENDER ID: " + getString(R.string.gcm_defaultSenderId));

            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            Log.i(TAG, "GCM Registration Token: " + token);

            //[START get_imei]
            final TelephonyManager tm = (TelephonyManager) getBaseContext()
                    .getSystemService(TELEPHONY_SERVICE);
            String imei = tm.getDeviceId();
            //[END get_imei]

            //[START get_device_name]
            String deviceName = Build.MODEL;
            //[END get_device_name]

            // TODO: Implement this method to send any registration to your app's servers.
            sendRegistrationToServer(currentUserId, token, imei, deviceName);

            // Subscribe to topic channels
            //subscribeTopics(token);

            // [END register_for_gcm]
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.
            getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE)
                    .edit().putBoolean(Constants.Preferences.SENT_TOKEN_TO_SERVER, false).apply();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Constants.Preferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(int userId, String token, String imei, String deviceName) {
        // Add custom implementation, as needed.
        Log.d(TAG, "Enviando al servidor: (" + token + ", " + imei + ", " + deviceName + "), para el usuario con id " + userId );

        String uri = String.format(Constants.Uri.REGISTER_DEVICE, userId);

        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.REGISTER_DEVICE,
                getJSONString(token, imei, deviceName));
    }

    private String getJSONString(String token, String imei, String deviceName) {
        try {
            JSONObject object = new JSONObject();
            object.put(Constants.Registration.DEVICE_NAME, deviceName);
            object.put(Constants.Registration.DEVICE_IMEI, imei);
            object.put(Constants.Registration.REGISTRATION_ID, token);

            Log.d(TAG, "JSON a enviar: " + object.toString());

            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]

    @Override
    public void onResponse(int requestCode, String result) {
        Log.d(TAG, "Respuesta del servidor: " + result);

        // You should store a boolean that indicates whether the generated token has been
        // sent to your server. If the boolean is false, send the token to your server,
        // otherwise your server should have already received the token.
        getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE)
                .edit().putBoolean(Constants.Preferences.SENT_TOKEN_TO_SERVER, true).apply();
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Log.d(TAG, "Error: " + statusCode);
    }

}