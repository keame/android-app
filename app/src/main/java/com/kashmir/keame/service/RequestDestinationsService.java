package com.kashmir.keame.service;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.kashmir.keame.controller.RestApiConnection;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Destination;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RequestDestinationsService extends IntentService {

    /* CONSTANTES ------------------------------------------------------------------------------- */
    private static final String TAG =
            RequestDestinationsService.class.getSimpleName();
    private static final String ACTION_GET_DESTINATIONS =
            "com.kashmir.keame.service.action.GET_DESTINATIONS";



    /* VARIABLES PRIVADAS ----------------------------------------------------------------------- */
    private GoogleApiClient mGoogleApiClient;
    private ArrayList<Geofence> mGeofenceList;
    private PendingIntent mGeofencePendingIntent;
    private ArrayList<Destination> mDestinations;





    public static void handleDestinationRequest(Context context) {
        Log.d(TAG, "Llamada al metodo");
        Intent intent = new Intent(context, RequestDestinationsService.class);
        intent.setAction(ACTION_GET_DESTINATIONS);
        context.startService(intent);
    }

    public RequestDestinationsService() {
        super("RequestDestinationsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent");
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_DESTINATIONS.equals(action)) {

                String request;

                do{
                     request = requestDestinations(getApplicationContext());
                } while (request == null);

                Log.d(TAG, request == null ? "Nada" : request);

                if( request!= null){
                    populateDestinationsList(request);
                    Log.d(TAG, "La lista de destinos tiene: " + mDestinations.size());
                } else {
                    /*Hacerlo con un Handler
                    Log.d(TAG, "Poniendo alarma");
                    Intent alarm = new Intent(getApplicationContext(), AlarmReceiver.class);
                    PendingIntent pendingIntent = PendingIntent
                            .getBroadcast(this.getApplicationContext(), 5, alarm, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                            + (500), pendingIntent);*/
                }

                if(mDestinations != null && mDestinations.size() > 0){
                    mGeofenceList = new ArrayList<Geofence>();
                    mGeofencePendingIntent = null;
                    populateGeofenceList();
                    buildGoogleApiClient(getApplication());
                    mGoogleApiClient.connect();
                }
            }
        }
    }






    /**
     * Envía el request al servidor
     */
    private String requestDestinations(Context context) {

        Log.d(TAG, "Pedido de Destinos.");

        int userId =
                getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, Context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.GET_ALL_DESTINATIONS, userId);

        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest strRequest = new StringRequest(Request.Method.GET, uri, future, future);
        RestApiConnection.getInstance(context).addToRequestQueue(strRequest);

        try {
            String response = future.get(20, TimeUnit.SECONDS);
            return transformToJsonString(response);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void populateDestinationsList(String result){

        mDestinations = new ArrayList<>();

        try {
            JSONObject response = new JSONObject(result);
            JSONArray destinations =
                    response.getJSONArray(Constants.Destinations.JSON_ARRAY_DESTINATIONS);

            for (int i = 0; i < destinations.length(); i++){
                mDestinations.add(new Destination(destinations.getJSONObject(i)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String transformToJsonString(String response){
        response = response.replaceAll("\\\\", "");
        response = response.substring(1, response.length() - 1);

        return response;
    }





    /* CONFIGURACIÓN DE GEOFENCES --------------------------------------------------------------- */

    protected synchronized void buildGoogleApiClient(final Context context) {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        LocationServices.GeofencingApi.addGeofences(
                                mGoogleApiClient,
                                getGeofencingRequest(),
                                getGeofencePendingIntent(context)
                        ).setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                Log.d(TAG, String.valueOf(status.getStatusCode()));
                            }
                        });
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {

                    }
                })
                .addApi(LocationServices.API)
                .build();
    }

    public void populateGeofenceList(){

        for( int i = 0; i < mDestinations.size(); i++ ){

            Destination destination = mDestinations.get(i);
            Log.d(TAG, "Geofenciando: " + destination.getName());

            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(String.valueOf(destination.getId()))
                    .setCircularRegion(
                            destination.getPosition().latitude,
                            destination.getPosition().longitude,
                            Constants.Geofence.RADIUS
                    )
                    .setExpirationDuration(Geofence.NEVER_EXPIRE) /*60 * 60 * 1000 * 12*/
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER
                            | Geofence.GEOFENCE_TRANSITION_DWELL)
                    .setLoiteringDelay(1000 * 5) //Cinco minutos
                    .build());
        }

    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER
                | GeofencingRequest.INITIAL_TRIGGER_DWELL);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent(Context context) {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(context, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(context, 2, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }
}
