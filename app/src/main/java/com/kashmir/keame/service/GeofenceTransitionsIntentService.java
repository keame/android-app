package com.kashmir.keame.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.android.volley.Request;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class GeofenceTransitionsIntentService extends IntentService
        implements Data.OnQueryListener {

    private static final String TAG = GeofenceTransitionsIntentService.class.getSimpleName();

    private static final int RETRY_DELAY = 60 * 1000;

    private String mRequestId;

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        /*if (intent != null) {
            final String action = intent.getAction();
            if (action.equals("ACTION_GEOFENCE")) {
                newGeofence();
            }
        }*/

        Log.d(TAG, "GeofenceTransitionIntentService.onHndleIntent()");

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            Log.e(TAG, String.valueOf(geofencingEvent.getErrorCode()));
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER /*||
                /*geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL*/) {

            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            for(int i = 0; i < triggeringGeofences.size(); i++){
                Log.d(TAG, "Encontró: " + triggeringGeofences.get(i).getRequestId());
                mRequestId = triggeringGeofences.get(i).getRequestId();

                sendNotification(mRequestId);
            }

            if( geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER){
                Log.d(TAG, "ENTER");
            } else if( geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL){
                Log.d(TAG, "DWELL");
            }



            //Log.d(TAG, "Encontré algun Geofence.");
            //SmsManager sms = SmsManager.getDefault();
            //sms.sendTextMessage("541151183543", null, "Estoy dentro de algún Geofence", null, null);

            // Get the transition details as a String.
            //String geofenceTransitionDetails = getGeofenceTransitionDetails(
              //      this,
                //    geofenceTransition,
              //      triggeringGeofences
            //);

            // Send notification and log the transition details.
            //sendNotification(geofenceTransitionDetails);
            //Log.i(TAG, geofenceTransitionDetails);
        } else {
            // Log the error.
            //Log.e(TAG, getString(R.string.geofence_transition_invalid_type,
                    //geofenceTransition));
        }

    }


    private void sendNotification(String destinationId){

        int userId = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.SEND_DESTINATIONS_NOTIFICATION, userId);

        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.SEND_DESTINATIONS_NOTIFICATION,
                getRequestBody(destinationId)
        );
    }

    private String getRequestBody(String id) {
        JSONObject destinationId = new JSONObject();
        try {
            destinationId.put(Constants.Destinations.JSON_TAG_DESTINATION_ID, id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, destinationId.toString());

        return destinationId.toString();
    }

    @Override
    public void onResponse(int requestCode, String result) {
        Log.d(TAG, result);
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Log.d(TAG, "Status Code: " + statusCode);

        switch (requestCode){
            case Constants.ApiRequestCode.SEND_DESTINATIONS_NOTIFICATION:
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                sendNotification(mRequestId);
                            }
                        }, RETRY_DELAY);
                break;
        }
    }
}
