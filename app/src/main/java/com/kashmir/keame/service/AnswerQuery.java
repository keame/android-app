package com.kashmir.keame.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.android.volley.Request;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class AnswerQuery extends IntentService implements Data.OnQueryListener {

    private static final String TAG = AnswerQuery.class.getSimpleName();

    private static final String ACTION_ANSWER_STATE_QUERY =
            "com.kashmir.keame.service.action.STATE_QUERY";
    private static final String ACTION_ANSWER_POSITION_QUERY =
            "com.kashmir.keame.service.action.POSITION";

    private static final String EXTRA_STATE = "state";
    private static final String EXTRA_SERVER_NOTIFICATION = "server_notification_id";
    private static final String EXTRA_NOTIFICATION = "notification_id";
    private static final String EXTRA_FROM = "from_id";

    private static final int RETRY_DELAY = 60 * 1000;


    private boolean isFine;
    private int serverNotificationId;
    private int notificationId;
    private int fromId;


    public AnswerQuery() {
        super("AnswerQuery");
    }

    public static Intent getStateQueryIntent
            (Context context, boolean isFine, int serverId, int id, int fromId) {
        Intent intent = new Intent(context, AnswerQuery.class);

        intent.setAction(ACTION_ANSWER_STATE_QUERY + String.valueOf(serverId));

        intent.putExtra(EXTRA_STATE, isFine);
        intent.putExtra(EXTRA_SERVER_NOTIFICATION, serverId);
        intent.putExtra(EXTRA_NOTIFICATION, id);
        intent.putExtra(EXTRA_FROM, fromId);

        return intent;
    }

    public static Intent getPositionQueryIntent(Context context, int notificationId, int fromId) {
        Intent intent = new Intent(context, AnswerQuery.class);
        intent.setAction(ACTION_ANSWER_POSITION_QUERY + String.valueOf(notificationId));

        intent.putExtra(EXTRA_SERVER_NOTIFICATION, notificationId);
        intent.putExtra(EXTRA_FROM, fromId);

        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (action.startsWith(ACTION_ANSWER_STATE_QUERY)) {

                isFine = intent.getBooleanExtra(EXTRA_STATE, false);
                serverNotificationId = intent.getIntExtra(EXTRA_SERVER_NOTIFICATION, -1);
                notificationId = intent.getIntExtra(EXTRA_NOTIFICATION, -1);
                fromId = intent.getIntExtra(EXTRA_FROM, -1);

                sendStateResponse();

            } else if (action.startsWith(ACTION_ANSWER_POSITION_QUERY)) {

                Log.d(TAG, ACTION_ANSWER_POSITION_QUERY);

                serverNotificationId = intent.getIntExtra(EXTRA_SERVER_NOTIFICATION, -1);
                fromId = intent.getIntExtra(EXTRA_FROM, -1);

                notificationId = serverNotificationId;

                sendPositionResponse();
            }

            //Si se respondió sacamos la notificación
            ((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
                    .cancel(notificationId);
        }
    }

    private void sendPositionResponse() {
        int userId =
                getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, Context.MODE_PRIVATE)
                        .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.ANSWER_POSITION_REQUEST, userId);

        Data.newRequest(
                getApplicationContext(),
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.ANSWER_POSITION_REQUEST,
                getJsonStringForPosition());
    }

    private void sendStateResponse() {

        int userId =
                getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, Context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.ANSWER_STATE_REQUEST, userId);

        Data.newRequest(
                getApplicationContext(),
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.ANSWER_STATE_REQUEST,
                getJsonStringForState());
    }

    private String getJsonStringForState(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("contact_id", fromId);
            jsonObject.put("status", isFine? "fine" : "bad");
            jsonObject.put("notification_id", serverNotificationId);

            Log.d(TAG, jsonObject.toString());

            return jsonObject.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getJsonStringForPosition(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("contact_id", fromId);
            jsonObject.put("notification_id", serverNotificationId);

            Log.d(TAG, jsonObject.toString());

            return jsonObject.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onResponse(int requestCode, String result) {
        Log.d(TAG, result);
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        Log.e(TAG, "Status code: " + statusCode);

        switch (requestCode){
            case Constants.ApiRequestCode.ANSWER_STATE_REQUEST:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "Intentando nuevamente...");
                        sendStateResponse();
                    }
                }, RETRY_DELAY);
                break;
            case Constants.ApiRequestCode.ANSWER_POSITION_REQUEST:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "Intentando nuevamente...");
                        sendPositionResponse();
                    }
                }, RETRY_DELAY);
                break;
        }
    }
}
