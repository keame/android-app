package com.kashmir.keame.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;
import com.kashmir.keame.R;
import com.kashmir.keame.activity.AccountSettingsActivity;
import com.kashmir.keame.activity.AnswerCareModeActivity;
import com.kashmir.keame.activity.MainMapActivity;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Contact;
import com.kashmir.keame.model.Route;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Lorena Soledad on 26/09/2015.
 */
public class RouteTrackerService extends IntentService implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = RouteTrackerService.class.getSimpleName();
    private static final int RETRY_DELAY = 60 * 1000;
    private static final int MAX_TIMES_OUT_OF_ROUTE = 1;

    private static boolean isServiceRunning = false;
    private int outOfRoute;



    /* VARIABLES LOCALES ------------------------------------------------------------------------ */
    private static boolean mRequestingLocationUpdates = false;
    private GoogleApiClient mGoogleApiClient = null;
    private LocationRequest mLocationRequest = null;
    private Location mCurrentLocation = null;
    private LatLng mLastPointInRoute;
    private Route mRoute = null;

    private ArrayList<Contact> mEmergencyContacts;


    /**
     * Clase que espera que la notificación sea cancelada para dar por finalizado el servicio
     */
    public static class CancelServiceReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "CancelServiceIntent.onReceive()");
            //Finalizamos el servicio. Si hay una tarea pendiente en la cola de procesos (ej: El
            //usuario decidió cambiar de ruta, se va a ejecutar este otro. De otra forma significa
            //que, el usuario canceló la ruta o bien se detectó el final del recorrido.

            mRequestingLocationUpdates = false;
        }
    }

    public RouteTrackerService(){
        super(TAG);
    }



    /* CICLO DE VIDA DEL SERVICIO --------------------------------------------------------------- */
    @Override
    public void onCreate() {

        Log.d(TAG, "onCreate()");

        super.onCreate();
        buildGoogleApiClient();
        createLocationRequest();
        //getEmergencyContacts();

        if(mGoogleApiClient != null){
            mRequestingLocationUpdates = true;
            mGoogleApiClient.connect();
        }

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent()");

        if(!isServiceRunning){
            isServiceRunning = true;
            Log.d(TAG, "Corriendo el servicio por primera vez.");
        } else {
            Log.d(TAG, "Corriendo el servicio nuevamente");
        }

        //Recuperamos el parametro que nos envia la Activity
        mRoute = intent.getParcelableExtra(Constants.RouteTraker.EXTRA_ROUTE);

        //Para controlar el lanzamiento de la alerta
        outOfRoute = 0;

        mLastPointInRoute = mRoute.getGeopoints()
                .get(mRoute.getGeopoints().size() - 1);

        startForeground();
        Toast.makeText(this, R.string.vigilance_active, Toast.LENGTH_SHORT).show();

        while (mRequestingLocationUpdates){
            synchronized (this) {
                try {
                    wait(1000);
                } catch (Exception e) {
                }
            }
        }
        stopForeground(true);

        mRequestingLocationUpdates = true;
        Log.d(TAG, "Finalizando...");

    }


    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");

        super.onDestroy();
        stopLocationUpdates();
        changeServiceStateInSettings();
        showToastMessage();
        mGoogleApiClient.disconnect();
        mGoogleApiClient = null;
    }



    /**
     * Creamos una Request con los datos que necesitamos para conectarnos a los
     * Google Play Services
     */
    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    /**
     * Crea el objeto que contiene los datos para recibir actualizaciones de posici�n
     */
    protected void createLocationRequest() {

        //Getting the option the user selected in preferences
        int trackingIntervalOpc = Integer.parseInt(
                PreferenceManager.getDefaultSharedPreferences(this)
                        .getString(Constants.Preferences.ROUTE_VIGILANCE_INTERVAL, "2"));

        int interval = 0, fastestInterval = 0;

        Log.d(TAG, "La opción es la: " + trackingIntervalOpc);

        switch (trackingIntervalOpc){
            case 1:
                interval = 1000 * 60 * 1;
                fastestInterval = 1000 * 60 * 1;
                break;
            case 2:
                interval = 1000 * 60 * 5;
                fastestInterval = 1000 * 60 * 3;
                break;
            case 3:
                interval = 1000 * 60 * 10;
                fastestInterval = 1000 * 60 * 7;
                break;
        }


        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(interval)
                .setFastestInterval(fastestInterval)
                //.setSmallestDisplacement(Constants.RouteTraker.SMALLEST_DISPLACEMENT)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    /**
     * Comienza a recibir actualizaciones de posici�n
     */
    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }


    /**
     * Deja de recibir actualizaciones de posici�n
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }




    /* CALLBACKS DE CONEXION CON GOOGLE PLAY SERVICES ------------------------------------------- */
    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected()");

        if(mRequestingLocationUpdates){
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended()");

        String message = null;

        switch (i){
            case CAUSE_NETWORK_LOST:
                message = Constants.Error.NETWORK_LOST;
                break;
            case CAUSE_SERVICE_DISCONNECTED:
                message = Constants.Error.SERVICE_DISCONNECTED;
                break;
        }

        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        Log.d(TAG, message);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, connectionResult.toString());
    }



    /* CALLBACKS DE ACTUALIZACION DE POSICION --------------------------------------------------- */
    @Override
    public void onLocationChanged(Location location) {

        Log.d(TAG, "onLocationChanged(): " + DateFormat.getTimeInstance().format(new Date()));

        mCurrentLocation = location;

        if(!isOnRoute() && outOfRoute > MAX_TIMES_OUT_OF_ROUTE){

            boolean careMode = PreferenceManager.getDefaultSharedPreferences(this)
                    .getBoolean(Constants.Preferences.CARE_MODE_STATE, false);

            if(careMode){

                Log.d(TAG, "Modo Cuidado Activado");

                //Disparar notificación para la pregunta
                showNotification();

                //Pongo la alarma
                setAlarm();

                mRequestingLocationUpdates= false;
            } else {
                //Envío alerta y finalizo la ejecución del servicio
                sendAlert();
            }


        } else if (canStopService()){
            mRequestingLocationUpdates = false;
        }

    }

    private void setAlarm() {

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent intent = SendAlertService.getCancelIntent(this);
        PendingIntent pIntent = PendingIntent.getService(this, 0, intent, 0);

        alarmManager.set(AlarmManager.RTC_WAKEUP,
                Calendar.getInstance().getTimeInMillis() + 1000 * 60 * 2,
                pIntent);
    }

    private void sendAlert() {
        SendAlertService.sendAlert(getApplicationContext());
        mRequestingLocationUpdates = false;
    }

    private void showNotification(){

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_alert)
                        .setColor(Color.argb(0, 192, 57, 43))
                        .setContentTitle(getResources().getString(R.string.notification_care_title))
                        .setContentText(getResources().getString(R.string.notification_care_content));

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, AnswerCareModeActivity.class);

        // Sets the Activity to start in a new, empty task
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        getApplicationContext(),
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setOngoing(true);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(Constants.RouteTraker.NOTIFICATION_CARE_ID, mBuilder.build());
    }

    /**
     * Determina si la posici�n actual del usuario en @mCurrentLocation se encuentra en la ruta.
     *
     * @return true si se encuentra en la ruta @mRoute, sino false
     */
    private boolean isOnRoute() {
        LatLng point = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        if (PolyUtil.isLocationOnPath(point, mRoute.getGeopoints(), false,
                Constants.RouteTraker.MAX_DISTANCE)) {
            Log.d(TAG, "Esta en la ruta");
            outOfRoute = 0;
            return true;
        } else {
            Log.d(TAG, "No esta en la ruta");
            outOfRoute++;
            return false;
        }
    }


    /**
     * Funci�n que determina si se puede finalizar o no el servicio. El criterio que usamos
     * es comparando @mCurrentLocation con @mLastPointInRoute. Si la distancia entre ambos es
     * menor a 500 metros, entonces se asume que el usuario lleg� a destino.
     *
     * @return true si podemos finalizar el servicio, sino false
     */
    private boolean canStopService(){
        float[] distance = new float[2];

        Location.distanceBetween(mCurrentLocation.getLatitude(),
                mCurrentLocation.getLongitude(), mLastPointInRoute.latitude,
                mLastPointInRoute.longitude, distance);

        if( distance[0] > Constants.RouteTraker.MAX_DISTANCE  )
            return false;
        else
            return true;
    }


    /**
     * Cambia el estado del servicio en las settings, cuando el mismo finaliza
     */
    private void changeServiceStateInSettings(){
        getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, MODE_PRIVATE)
                .edit().putBoolean(Constants.Preferences.IS_ROUTE_SERVICE_RUNNING, false)
                .commit();
    }


    /**
     * Muestra un mensaje notificando que el servicio fue detenido
     */
    private void showToastMessage(){
        Toast.makeText(this,
                getResources().getString(R.string.vigilance_deactivated),
                Toast.LENGTH_SHORT).show();
    }

    public void startForeground(){

        Intent resultIntent = new Intent(this, RouteTrackerService.CancelServiceReceiver.class);
        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getBroadcast(
                        this,
                        1,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_manual_route)
                        .setColor(Color.argb(0, 39, 174, 96))
                        .setContentTitle(getResources()
                                .getString(R.string.vigilance_notification_active))
                        .setContentText(String.format(
                                getResources()
                                        .getString(R.string.vigilance_notification_content),
                                mRoute.getName()))
                        .addAction(R.drawable.ic_back,
                                getResources().getString(R.string.vigilance_notification_action),
                                resultPendingIntent);
                        //.setStyle(new NotificationCompat.InboxStyle());

        startForeground(Constants.RouteTraker.NOTIFICATION_ID, mBuilder.build());

    }

    /*private void getEmergencyContacts(){

        int currentUserId = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE,
                MODE_PRIVATE).getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        Log.d(TAG, "User ID: " +currentUserId );

        String uri = String.format(Constants.Uri.GET_ALL_EMERGENCY_CONTACTS, currentUserId);

        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.GET,
                null,
                Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS,
                null);
    }*/


    /*@Override
    public void onResponse(int requestCode, String result) {
        try {
            JSONObject response = new JSONObject(result);
            int status = response.getInt("status");

            if(status == 200){
                switch (requestCode){
                    case Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS:
                        JSONArray contact = response.getJSONArray("contacts");

                        mEmergencyContacts = new ArrayList<>();

                        for(int i = 0; i < contact.length(); i++){
                            Log.d(TAG, "Cargando un contacto");
                            mEmergencyContacts.add(new Contact(contact.getJSONObject(i),
                                    Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS));
                        }
                        break;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/

    /*@Override
    public void onErrorResponse(int statusCode) {

        Log.d(TAG, "onErrorResponse()");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Volviendo a conectar...");
                getEmergencyContacts();
            }
        }, RETRY_DELAY);
    }*/

}
