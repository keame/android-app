package com.kashmir.keame.service;

/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

        import android.app.NotificationManager;
        import android.app.PendingIntent;
        import android.app.Service;
        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Color;
        import android.media.RingtoneManager;
        import android.net.Uri;
        import android.os.Bundle;
        import android.support.v4.app.NotificationCompat;
        import android.util.Log;

        import com.google.android.gms.gcm.GcmListenerService;
        import com.kashmir.keame.R;
        import com.kashmir.keame.activity.MainMapActivity;
        import com.kashmir.keame.model.Constants;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.Random;

public class KeameGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    private static final String TYPE_ARRIVE = "arrive";
    private static final String TYPE_CONTACT_REQUEST = "contact_request";
    private static final String TYPE_NEW_CONTACT = "new_contact";
    private static final String TYPE_QUERY_STATE = "query_state";
    private static final String TYPE_RESPOND_STATE = "respond_state";
    private static final String TYPE_QUERY_POSITION = "position";
    private static final String TYPE_RESPONSE_POSITION = "response_position";
    private static final String TYPE_ALERT = "emergency";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, data.toString());

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(data);
    }
    // [END receive_message]


    /**
     * Create and show a simple notification containing the received GCM message.
     *
     */
    private void sendNotification(Bundle data) {

        int fromId;
        int notificationId = Integer.parseInt(data.getString("notification_id"));
        String fromName = data.getString("from_name");


        Intent intent = new Intent(this, MainMapActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, notificationId, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        switch (data.getString("type")) {
            case TYPE_ARRIVE:
                String arrivedTo = data.getString("message");

                notificationBuilder.setContentTitle("Keame")
                        .setContentText(fromName + " " + arrivedTo)
                        .setColor(Color.argb(1, 22, 160, 133))
                        .setSmallIcon(R.drawable.ic_destination);
                break;
            case TYPE_CONTACT_REQUEST:
                notificationBuilder.setContentTitle("Keame")
                        .setContentText(fromName + " " + " quiere agregarte como contacto")
                        .setSmallIcon(R.drawable.ic_contact_request)
                        .setColor(Color.argb(1, 22, 160, 133));
                break;
            case TYPE_NEW_CONTACT:
                notificationBuilder.setContentTitle("Keame")
                        .setContentText(fromName + " " + " te ha aceptado")
                        .setSmallIcon(R.drawable.ic_new_contact)
                        .setColor(Color.argb(1, 22, 160, 133));
                break;
            case TYPE_ALERT:
                notificationBuilder.setContentTitle("Keame")
                        .setContentText(fromName + " necesita tu ayuda!")
                        .setSmallIcon(R.drawable.ic_alert)
                        .setColor(Color.argb(0, 192, 57, 43));
                break;
            case TYPE_QUERY_STATE:
                fromId = Integer.parseInt(data.getString("from_id"));

                PendingIntent answerFineIntent = PendingIntent.getService
                        (getApplicationContext(),
                                0,
                                AnswerQuery.getStateQueryIntent
                                        (getApplicationContext(), true, notificationId, notificationId, fromId),
                                0);

                PendingIntent answerBadIntent = PendingIntent.getService
                        (getApplicationContext(),
                                1,
                                AnswerQuery.getStateQueryIntent
                                        (getApplicationContext(), false, notificationId, notificationId, fromId),
                                0);
                notificationBuilder.setContentTitle("Keame")
                        .setContentText(fromName + " " + "quiere saber como estás")
                        .setSmallIcon(R.drawable.ic_state_response)
                        .setColor(Color.argb(1, 22, 160, 133))
                        .addAction(R.drawable.ic_bad, "Mal", answerBadIntent)
                        .addAction(R.drawable.ic_fine, "Bien", answerFineIntent);
                break;
            case TYPE_RESPOND_STATE:
                notificationBuilder.setContentTitle("Keame")
                        .setContentText(fromName + " " + "está bien")
                        .setSmallIcon(R.drawable.ic_state_response)
                        .setColor(Color.argb(1, 22, 160, 133));
                break;
            case TYPE_QUERY_POSITION:

                fromId = Integer.parseInt(data.getString("from_id"));

                notificationBuilder.setContentTitle("Keame")
                        .setContentText(fromName + " " + "quiere conocer tu ubicación")
                        .setSmallIcon(R.drawable.ic_position)
                        .setColor(Color.argb(1, 22, 160, 133));

                PendingIntent answerPosition = PendingIntent.getService
                        (getApplicationContext(),
                                0,
                                AnswerQuery.getPositionQueryIntent
                                        (getApplicationContext(), notificationId, fromId),
                                0);

                notificationBuilder.setContentIntent(answerPosition);
                break;
            case TYPE_RESPONSE_POSITION:
                double lat = Double.parseDouble(data.getString("lat"));
                double lon = Double.parseDouble(data.getString("long"));

                Log.d(TAG, "Latitud: " + lat + " Longitud: " + lon);

                notificationBuilder.setContentTitle("Keame")
                        .setContentText(fromName + " " + "te ha enviado su ubicación")
                        .setSmallIcon(R.drawable.ic_position)
                        .setColor(Color.argb(1, 22, 160, 133));

                // Creates an Intent that will load a map of San Francisco
                Uri gmmIntentUri = Uri.parse("geo:" + lat + "," + lon);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                PendingIntent pIntent = PendingIntent.getActivity(
                        this,
                        0,
                        mapIntent,
                        0);

                notificationBuilder.setContentIntent(pIntent);
                break;
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

}