package com.kashmir.keame.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.Context;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;

import com.android.volley.Request;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.model.Contact;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class SendAlertService extends IntentService implements Data.OnQueryListener {

    private static final String TAG = SendAlertService.class.getSimpleName();

    private static final String ACTION_CANCEL_NOTIF = "com.kashmir.keame.Alert.CANCEL";

    private static final int RETRY_DELAY = 60 * 1000;

    private ArrayList<Contact> mEmergencyContacts = null;

    public SendAlertService() {
        super("SendAlertService");
    }

    public static void sendAlert(Context context) {
        Intent intent = new Intent(context, SendAlertService.class);
        context.startService(intent);
    }

    public static Intent getCancelIntent(Context context){
        Intent intent = new Intent(context, SendAlertService.class);
        intent.setAction(ACTION_CANCEL_NOTIF);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if(action != null && action.equals(ACTION_CANCEL_NOTIF)){
                cancelCareNotification();
            }

            getEmergencyContacts();
        }
    }

    private void cancelCareNotification() {

        //Elimino la notificación
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(Constants.RouteTraker.NOTIFICATION_CARE_ID);
    }

    private void getEmergencyContacts(){

        int currentUserId = getSharedPreferences(Constants.Preferences.PREFERENCES_FILE,
                MODE_PRIVATE).getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        Log.d(TAG, "User ID: " + currentUserId);

        String uri = String.format(Constants.Uri.GET_ALL_EMERGENCY_CONTACTS, currentUserId);

        Data.newRequest(
                this,
                this,
                uri,
                Request.Method.GET,
                null,
                Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS,
                null);
    }


    @Override
    public void onResponse(int requestCode, String result) {

        Log.d(TAG, result);

        try {
            JSONObject response = new JSONObject(result);
            int status = response.getInt("status");

            if(status == 200){
                switch (requestCode){
                    case Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS:
                        JSONArray contact = response.getJSONArray("contacts");

                        mEmergencyContacts = new ArrayList<>();

                        for(int i = 0; i < contact.length(); i++){
                            Log.d(TAG, "Cargando un contacto");
                            mEmergencyContacts.add(new Contact(contact.getJSONObject(i),
                                    Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS));
                        }

                        sendEmergencyAlert();
                        break;

                    case Constants.ApiRequestCode.SEND_ALERT:
                        Log.d(TAG, "Alerta enviada al servidor");
                        break;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void sendEmergencyAlert() {

        //Envío de SMS
        SmsManager sms = SmsManager.getDefault();
        String message = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("emergency_message", "Ayuda!");

        if(mEmergencyContacts != null){
            for(int i = 0; i < mEmergencyContacts.size(); i++){
                String phoneNumber = mEmergencyContacts.get(i).getTelephoneNumber();
                sms.sendTextMessage("+" + phoneNumber, null, message, null, null);
                Log.d(TAG, "Enviando un mensaje a: " + "+" + phoneNumber);
            }
        }

        sendEmergencyToServer();

    }

    private void sendEmergencyToServer() {

        //Envío al servidor para notificaciones
        int userId =
                getSharedPreferences(Constants.Preferences.PREFERENCES_FILE, Context.MODE_PRIVATE)
                .getInt(Constants.Preferences.CURRENT_USER_ID, -1);

        String uri = String.format(Constants.Uri.SEND_ALERT, userId);

        Data.newRequest(
                getApplicationContext(),
                this,
                uri,
                Request.Method.POST,
                null,
                Constants.ApiRequestCode.SEND_ALERT,
                null);
    }

    @Override
    public void onErrorResponse(int statusCode, int requestCode) {
        switch (statusCode){
            case Constants.ApiRequestCode.GET_ALL_EMERGENCY_CONTACTS:
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Volviendo a conectar...");
                        getEmergencyContacts();
                    }
                }, RETRY_DELAY);
                break;

            case Constants.ApiRequestCode.SEND_ALERT:

                Log.d(TAG, "IT IS BAD");

                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendEmergencyToServer();
                    }
                }, RETRY_DELAY);
                break;
        }
    }
}
