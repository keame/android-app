package com.kashmir.keame.service;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.kashmir.keame.controller.Data;
import com.kashmir.keame.model.Constants;
import com.kashmir.keame.receiver.LocationUpdate;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;


public class FollowingService extends IntentService
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = FollowingService.class.getSimpleName();

    private static final String ACTION_TRACKER =
            "com.kashmir.keame.service.action.TRACKER";

    private final int REQUEST_UPDATE = 123;

    private Location mCurrentLocation;
    private String mLastUpdateTime;
    private boolean mRequestingLocationUpdates;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;


    public static void startTrackerService(Context context) {
        Intent intent = new Intent(context, FollowingService.class);
        intent.setAction(ACTION_TRACKER);
        context.startService(intent);
    }

    public FollowingService() {
        super("FollowingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_TRACKER.equals(action)) {
                handleTraker();
            }
        }
    }

    /*@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }*/

    private void handleTraker() {
        buildGoogleApiClient(); //Creamos el Google API Client
        createLocationRequest();

        if(mGoogleApiClient != null){
            mGoogleApiClient.connect();
        }
    }





    /* FUNCIONES PARA SEGUIMIENTO --------------------------------------------------------------- */

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {

        //Getting the option the user selected in preferences
        int trackingIntervalOpc = Integer.parseInt(
                PreferenceManager.getDefaultSharedPreferences(this)
                        .getString(Constants.Preferences.TRACKING_INTERVAL, "2"));

        int interval = 0, fastestInterval = 0;

        Log.d(TAG, "La opción es la: " + trackingIntervalOpc);

        switch (trackingIntervalOpc){
            case 1:
                interval = 1000 * 60 * 2;
                fastestInterval = 1000 * 60 * 1;
                break;
            case 2:
                interval = 1000 * 60 * 15;
                fastestInterval = 1000 * 60 * 10;
                break;
            case 3:
                interval = 1000 * 60 * 30;
                fastestInterval = 1000 * 60 * 25;
                break;
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(interval)
                .setFastestInterval(fastestInterval)
                        //.setSmallestDisplacement(SMALLEST_DISPLACEMENT)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    /**
     * Comienza a recibir actualizaciones de posicion
     */
    protected void startLocationUpdates() {

        Intent requestLocationIntent = new Intent(this, LocationUpdate.class);
        PendingIntent requestLocationPendingIntent = PendingIntent.getBroadcast(
                this, REQUEST_UPDATE, requestLocationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, requestLocationPendingIntent);
    }



    /* CONNECTION CALLBACKS --------------------------------------------------------------------- */

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //TODO
    }




    /* ON CONNECTION FAILED CALLBACK ------------------------------------------------------------ */

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //TODO
    }

}
